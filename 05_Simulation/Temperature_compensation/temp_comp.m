clear all
close all
clc

readChannelID = 845822;
% TODO - Enter the Read API Key between the '' below:
readAPIKey = '6WPTDAA1OW1XA55Q';

% TODO - Replace the [] with channel ID to write data to:
writeChannelID = 845822;
% TODO - Enter the Write API Key between the '' below:
writeAPIKey = 'N32NBEA1EHHVYSYX';

weightFieldID = 1;
tempFieldID = 2;
humFieldID = 3;
chargeFieldID = 6;
timespan_min = 30*24*60;

%% Read Data
[data,timestamps] = thingSpeakRead(readChannelID, 'Fields', [weightFieldID, tempFieldID, humFieldID, chargeFieldID], 'NumMinutes', timespan_min, 'ReadKey', readAPIKey);
%[data,timestamps] = thingSpeakRead(readChannelID, 'Fields', [weightFieldID, tempFieldID, humFieldID, chargeFieldID], 'DateRange', [datetime(2020,3,29,0,0,0),datetime(2020,4,17,0,0,0)], 'ReadKey', readAPIKey);
[data1,timestamps1] = thingSpeakRead(readChannelID, 'Fields', [weightFieldID, tempFieldID, humFieldID, chargeFieldID], 'DateRange', [datetime(2020,1,8,0,0,0),datetime(2020,2,16,0,0,0)], 'ReadKey', readAPIKey);

weight = data(:,1);
temp = data(:,2);
hum = data(:,3);
charge = data(:,4);

weight1 = data1(:,1);
temp1 = data1(:,2);
hum1 = data1(:,3);
charge1 = data1(:,4);

for i = 1:size(weight)
    %%%%%%%%%%% invalid data
    if data(i,1) < 20
        weight(i) = weight(i-1);
    end
    %%%%%%%%%%%
end

TT = timetable(timestamps, weight, temp, hum);
TTcharge = timetable(timestamps, charge);
TTcharge.Properties.VariableContinuity = {'step'};

TThour = retime(TT,'hourly','pchip');
TTmin = retime(TT,'minutely','pchip');
TTcharge_min = retime(TTcharge,'minutely');
TTcharge_min.charge(1)=0;

TT1 = timetable(timestamps1, weight1, temp1, hum1);
TTcharge1 = timetable(timestamps1, charge1);
TTcharge1.Properties.VariableContinuity = {'step'};

TTmin1 = retime(TT1,'minutely','pchip');
TTcharge_min1 = retime(TTcharge1,'minutely');
TTcharge_min1.charge1(1)=0;

weight_desired_min = 25*ones(size(TTmin,1),1);
weight_desired_min1 = 29.55*ones(size(TTmin1,1),1);
weight_desired_hour = 25*ones(size(TThour,1),1);

figure(1)
stackedplot(TTmin);
grid on

%% estimate State Space Model
iodata = iddata(weight_desired_hour-TThour.weight, TThour.temp, 3600);
[sys,x0] = n4sid(iodata, 16, 'DisturbanceModel', 'none', 'Feedthrough', true);

[comp,t] = lsim(sys,TThour.temp,[],x0);

weight_est_ss = comp+TThour.weight;

figure(2)
plot(t, TThour.weight, t, weight_est_ss)
grid on
title('State Space Model')

%% estimate Transfer Function
iodata = iddata(weight_desired_min-TTmin.weight, TTmin.temp, 60);
tf = tfest(iodata, 8, 4, 'Ts', 60,'Feedthrough',true);
[comp,t] = lsim(tf,TTmin.temp,(0:size(TTmin.weight)-1)*60);

weight_est_tf = comp + TTmin.weight;

figure(3)
plot(t, TTmin.weight, t, weight_est_tf)
grid on
title('Transfer Function Model')

%% Least Squares
S = [TTmin.weight, TTmin.temp, TTmin.temp.^2, TTmin.temp.^3];
p = ((S'*S) \ (S'*weight_desired_min));

weight_est_ls = S*p;
comp_ls = S(:,2:4)*p(2:4);
comp_desired = weight_desired_min-TTmin.weight*p(1);

% PT1 
iodata = iddata(comp_desired, comp_ls, 60);
pt1 = procest(iodata,'P1');
[comp_pt1,t] = lsim(pt1,TTmin.temp,(0:size(TTmin,1)-1)*60);

% PT1+LS
S = [TTmin.weight, comp_pt1, comp_pt1.^2, comp_pt1.^3];

weight_est_pt1ls = S*p;
comp_pt1ls = S(:,2:4)*p(2:4);

figure(4)
plot(t, TTmin.weight, t, weight_est_pt1ls);
grid on
title('PT1 + Least Squares Model')

%% Humidity
% S = [TTmin.weight, TTmin.hum, TTmin.hum.^2, TTmin.hum.^3];
% p = ((S'*S) \ (S'*weight_desired_min));
% 
% weight_est_h = S*p;
% comp_h = S(:,2:4)*p(2:4);
% 
% figure(5)
% plot(1:size(TTmin,1), TTmin.hum./10, 1:size(TTmin,1), y_est_pt1ls);
% grid on

%% Hammerstein Wiener
url = 'https://cloud.heap-engineering.at/index.php/s/WCq6enyLH8kSyqM/download';
filename = 'hw_model.mat';
outfilename = websave(filename,url);
load(outfilename);

comp = sim(nlhw8,[TTmin.temp, TTmin.hum]);
weight_est_hw = comp + TTmin.weight;

figure(6)
plot(1:size(TTmin.weight), TTmin.weight, 1:size(TTmin.weight), weight_est_hw);
grid on
title('Nichtlineares Hammerstein Wiener Model')

%% Neural Network
url = 'https://cloud.heap-engineering.at/index.php/s/5TyF6b3MgDwtKkn/download';
filename = 'nn_model.mat';
outfilename = websave(filename,url);
load(outfilename);

comp_nn = BaggedTree.predictFcn([TTmin.temp, TTmin.hum, TTcharge_min.charge]);

iodata = iddata(weight_desired_min-TTmin.weight, comp_nn, 60);
pt1_nn = procest(iodata,'P1');
[comp_pt1nn,t] = lsim(pt1_nn,comp_nn,(0:size(TTmin,1)-1)*60);

weight_est_pt1nn = comp_pt1nn + TTmin.weight;

figure(7)
plot(t, TTmin.weight, t, weight_est_pt1nn);
grid on
title('PT1 + Neural Network')

%% filter out
weight_comp = filtfilt(ones(1,600)/600,1,weight_est_pt1nn);

figure(8)
plot(1:size(weight_comp), weight_comp);
grid on
title('Best Result: PT1 + Neural Network + PT1')

out = [weight_desired_min - TTmin.weight; weight_desired_min1 - TTmin1.weight1];
in = [[TTmin.temp;TTmin1.temp1], [TTmin.hum; TTmin1.hum1], [TTcharge_min.charge; TTcharge_min1.charge1]];
train_data = [in, out];

iodata = iddata(out, in, 60);