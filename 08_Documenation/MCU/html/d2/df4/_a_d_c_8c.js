var _a_d_c_8c =
[
    [ "API_ADC_DMA_init", "db/d78/group___a_d_c.html#ga03e894b5911956fc55fc9015d851c10f", null ],
    [ "API_ADC_DMA_init", "d4/d52/group___a_d_c___private___functions.html#ga03e894b5911956fc55fc9015d851c10f", null ],
    [ "API_ADC_DMA_start_single_conv", "db/d78/group___a_d_c.html#ga465f3eeb4e57712a9cbfe0502c835587", null ],
    [ "API_ADC_DMA_start_single_conv", "d4/d52/group___a_d_c___private___functions.html#ga465f3eeb4e57712a9cbfe0502c835587", null ],
    [ "API_ADC_get_mcu_temp_gradc", "db/d78/group___a_d_c.html#gad46253ef4c9386fae8d457183919f04b", null ],
    [ "API_ADC_get_mcu_temp_gradc", "d4/d52/group___a_d_c___private___functions.html#gabb2586e5b17226a8be1fd3ea22ae706f", null ],
    [ "API_ADC_get_ue_v", "db/d78/group___a_d_c.html#ga170db7b663029dab41425f041bf7cae1", null ],
    [ "API_ADC_get_ue_v", "d4/d52/group___a_d_c___private___functions.html#ga170db7b663029dab41425f041bf7cae1", null ],
    [ "API_ADC_init", "db/d78/group___a_d_c.html#ga7f9c0f2529c2c4bc1498c10aca00e7a9", null ],
    [ "API_ADC_init", "d4/d52/group___a_d_c___private___functions.html#ga7f9c0f2529c2c4bc1498c10aca00e7a9", null ],
    [ "API_ADC1", "db/d78/group___a_d_c.html#ga62b22d9ab31843388eaa20e141437aaf", null ],
    [ "g_adc_ref_v", "db/d78/group___a_d_c.html#gaa43e29545783b29f069307138beab71a", null ]
];