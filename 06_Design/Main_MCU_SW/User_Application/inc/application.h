/**
  * @file    application.h
  * @author  Andreas Hirtenlehner
  * @brief   Header file for the application module
  */

#ifndef __APPLICATION_H
#define __APPLICATION_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <math.h>

/** @addtogroup Application
  * @{
  */

/** @addtogroup Tasks 
  * @{
  */

/* Exported types ------------------------------------------------------------*/

/** 
  * @brief  parameters to show on display
  */
	
typedef enum parameters_e
{
	WEIGHT = 0,
	BATTERY_CHARGE,
	BATTERY_VOLTAGE,
	EXT_VOLTAGE,
	AMPLIFIER_VOLTAGE,
	REFERENCE_VOLTAGE,
	AMB_TEMPERATURE,
	AMB_HUMIDITY,
	CLEAR,
} parameters_t;

typedef enum wakeup_source_e
{
	EMPTY = 0,
	BUTTON0,
	BUTTON1,
	BUTTON2,
	BUTTON3,
	RTC_ALARM
} wakeup_source_t;

typedef struct data_set_s
{
	uint32_t datetime;
	uint16_t weight;
	uint16_t amb_temperature;
	uint16_t amb_humidity;
	uint16_t battery_charge;
	uint16_t u_ext_supply;
	uint16_t status;
} data_set_t;

typedef struct sensor_control_s
{
	uint8_t type;
	uint32_t data;
} sensor_control_t;

typedef enum control_type_e
{
	ACK = 1,
	ACQUIRE_DATA,
	REQUEST_DATA,
	REQUEST_DATA_SINCE,
	REQUEST_DATA_BEFORE,
	SET_TEMPERATURE,
	SET_TIME
} control_type_e;

#define BATTERY_LOW_BITPOS     ((uint8_t) 0)
#define CHARGING_BITPOS        ((uint8_t) 1)
#define EXT_SUPPLY_UVLO_BITPOS ((uint8_t) 2)
#define SWARMING_BITPOS        ((uint8_t) 3)


/* Exported constants --------------------------------------------------------*/
/** @defgroup Tasks_Exported_Constants
  * @{
  */ 

/**
  * @brief taskclass period time definitions [s]
  * @{
  */

#define T_PERIOD1_S         ((double) 5e-3)
#define T_PERIOD2_S         ((double) 5e-2)
#define T_PERIOD_SOFT_RT_S  ((double) 1.0)
	
/**
  * @}
  */ 
	
/**
  * @brief user definitions
  * @{
  */

//#define DEBUG

#define MEASURE_CYCLES      ((uint8_t) 12)

#define TRIGGER_WEIGHT_DIFF_KG    ((double)  0.2)
#define TRIGGER_AMB_TEMP_DIFF_C   ((double)  5.0)
#define TRIGGER_AMB_HUM_DIFF_P    ((double)  10.0)
#define TRIGGER_BAT_CHARGE_DIFF_P ((uint8_t) 1)
#define TRIGGER_EXT_SUPPLY_DIFF_V ((double)  2.0)

#define EXT_SUPPLY_UVLO_V        ((double) 5.0)
#define BATTERY_UVLO_V           ((double) 3.1)

#define TIME_TO_LIFE_S	    ((double) 80.0)
#define PARAM_SHOW_TIME_S	  ((double) 80.0)
	
#define LONG_PRESS_TIME     ((double) 2.0)
	
#define WEIGHT_INT_SCALE          ((uint16_t)(0x10000 / 256))
#define BATTERY_INT_SCALE         ((uint16_t)(0x10000 / 128))
#define U_EXT_SUPPLY_INT_SCALE    ((uint16_t)(0x10000 / 64))
#define AMB_TEMP_INT_SCALE        ((uint16_t)(0x10000 / 128))
#define AMB_HUM_INT_SCALE         ((uint16_t)(0x10000 / 128))
	
#define U_EXT_SUPPLY_SCALE  ((double) 11.0) /* (R17P + R18P) / R18P */
#define U_BATTERY_SCALE     ((double) 2.0)  /* (R15P + R16P) / R16P */
	
#define FLASH_START_ADDRESS ((uint32_t) 0x08008000)
//#define FLASH_END_ADDRESS   ((uint32_t) 0x0803FFFF) /* STM32F030xC */
#define FLASH_END_ADDRESS   ((uint32_t) 0x0800FFFF) /* STM32F030x8 */

/**
  * @}
  */ 

/**
  * @brief portpin definitions
  * @{
  */

#define BUTTON0_PP 					((API_GPIO_type_t*) &PC13)
#define BUTTON1_PP 					((API_GPIO_type_t*) &PC14)
#define BUTTON2_PP 					((API_GPIO_type_t*) &PC15)
#define BUTTON3_PP 					((API_GPIO_type_t*) &PF0)

#define BATTERY_MEASURE_PP 	((API_GPIO_type_t*) &PA2)
#define N_CHARGE_ENABLE_PP	((API_GPIO_type_t*) &PB2)
#define N_CHARGE_PP					((API_GPIO_type_t*) &PB10)

#define LED_PP						  ((API_GPIO_type_t*) &PB15)

#define LED_DRIVER_EN_PP	  ((API_GPIO_type_t*) &PB7)
#define SPI_MOSI_PP 				((API_GPIO_type_t*) &PB5)
#define SPI_MISO_PP 				((API_GPIO_type_t*) &PB4)
#define SPI_CLK_PP 					((API_GPIO_type_t*) &PB3)
#define LE_PP 							((API_GPIO_type_t*) &PA15)
#define N_OE_PP 						((API_GPIO_type_t*) &PB6)

#define DIGIT1_PP 					((API_GPIO_type_t*) &PB12)
#define DIGIT2_PP 					((API_GPIO_type_t*) &PB13)
#define DIGIT3_PP 					((API_GPIO_type_t*) &PB14)

#define AMPLIFIER_EN_PP			((API_GPIO_type_t*) &PA5)
#define H_BRIDGE_HIGH_POS		((API_GPIO_type_t*) &PB1)
#define H_BRIDGE_LOW_POS		((API_GPIO_type_t*) &PB0)
#define H_BRIDGE_HIGH_NEG		((API_GPIO_type_t*) &PA7)
#define H_BRIDGE_LOW_NEG		((API_GPIO_type_t*) &PA6)

#define AMPLIFIER_ADC_PP    ((API_GPIO_type_t*) &PA0)
#define EXT_ADC_PP          ((API_GPIO_type_t*) &PA1)
#define REFERENCE_ADC_PP    ((API_GPIO_type_t*) &PA3)
#define BATTERY_ADC_PP      ((API_GPIO_type_t*) &PA4)

#define LORA_WKUP_PP				((API_GPIO_type_t*) &PA11)

#define USART_TX_PP					((API_GPIO_type_t*) &PA9)
#define USART_RX_PP					((API_GPIO_type_t*) &PA10)
#define USART_BAUDRATE 			115200

/**
  * @brief ADC Channels
  * @{
  */
	
#define AMPLIFIER_ADC_CH 		((uint8_t) 0)
#define EXT_SUPPLY_ADC_CH   ((uint8_t) 1)
#define REFERENCE_ADC_CH 		((uint8_t) 2)
#define BATTERY_ADC_CH 			((uint8_t) 3)

/**
  * @}
  */ 
	
/**
  * @brief Amplifier definitions
  * @{
  */
	
#define AAF_RES							((double)  10e3) /* R1A */
#define GAIN_RES						((double)   1e3) /* R6A,  Datasheet: R1 */
#define FEEDBACK_RES				((double) 220e3) /* R7A,  Datasheet: R2 */
#define OFFSET_RES_1				((double)  10e3) /* R9A, Datasheet: R3 */
#define OFFSET_RES_2				((double)  10e3) /* R8A, Datasheet: R4 */
#define AMPLIFIER_GAIN 			((double) (1 + (FEEDBACK_RES + OFFSET_RES_1 * OFFSET_RES_2 / (OFFSET_RES_1 + OFFSET_RES_2)) / GAIN_RES))

/**
  * @}
  */ 

/**
  * @brief Mechanical definitions
  * @{
  */

#define GAUGE_FACTOR 				((double) 2.0) /* k-factor */
#define POISSON_RATIO 			((double) 0.3) 
#define YOUNGS_MODULUS 			((double) 180e9)
#define SHAFT_DIAMETER 			((double) 0.013) /* diameter in m */

/**
  * @}
  */ 

/**
  * @brief Mechanical characteristics
  * @{
  */

#define SHAFT_AREA 					((double) (SHAFT_DIAMETER * SHAFT_DIAMETER * PI / 4.0))
#define SIGMA_REL 					((double) (2.0 * YOUNGS_MODULUS / (GAUGE_FACTOR * (1 + POISSON_RATIO)))) /* sigma / (Ud/U) */

/**
  * @}
  */ 

/**
  * @brief Correction factors
  * @{
  */

#define COMMON_CORR 				((double) 20865.88542)

/**
  * @}
  */ 

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

extern void usart1_rx_handler(void);
extern void resume_from_stop_mode(wakeup_source_t wakeup_source);

/**
  * @}
  */ 
	
/**
  * @}
  */ 

#endif // __APPLICATION_H
