/**
  * @file    application.c 
  * @author  Andreas Hirtenlehner
  * @brief   Implementation of the application code
  */

/* Includes ------------------------------------------------------------------*/
#include "application.h"
#include "arm_math.h"

/** @addtogroup Application
  * @{
  */

/** @defgroup Tasks 
  * @brief realtime tasks
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
q15_t adc_data[4];
data_set_t m_data_set;
data_set_t data_sets_min[60];
data_set_t data_sets_h[24];
data_set_t data_sets_d[31];
data_set_t data_sets_m[12];

uint8_t g_battery_charge_p = 0;
uint8_t g_active = 1;
wakeup_source_t g_wakeup_source = EMPTY;
uint8_t g_temp_received_flag = 0;
uint8_t g_datetime_received_flag = 0;
uint8_t g_lora_ack_received_flag = 0;
uint16_t g_amb_temperature = 0;
uint16_t g_amb_humidity = 0;
uint32_t g_lora_datetime_unix = 0;
uint8_t g_enter_stop_mode = 0;
uint8_t g_acquire_data_flag = 0;
sensor_control_t g_sensor_control_buffer[2];
data_set_t last_published_data_set = { 0 };
uint32_t flash_address;
uint8_t g_zeroing = 0;

double battery_charge_lt[] = 
{
	3.2,
	3.21,
	3.22,
	3.23,
	3.24,
	3.25,
	3.26,
	3.27,
	3.28,
	3.29,
	3.3,
	3.31,
	3.32,
	3.33,
	3.34,
	3.35,
	3.36,
	3.37,
	3.38,
	3.39,
	3.4,
	3.41,
	3.42,
	3.43,
	3.44,
	3.45,
	3.46,
	3.47,
	3.48,
	3.49,
	3.5,
	3.51,
	3.52,
	3.53,
	3.54,
	3.55,
	3.56,
	3.57,
	3.58,
	3.59,
	3.6,
	3.61,
	3.62,
	3.63,
	3.64,
	3.65,
	3.66,
	3.67,
	3.68,
	3.69,
	3.7,
	3.71,
	3.72,
	3.73,
	3.74,
	3.75,
	3.76,
	3.77,
	3.78,
	3.79,
	3.8,
	3.81,
	3.82,
	3.83,
	3.84,
	3.85,
	3.86,
	3.87,
	3.88,
	3.89,
	3.9,
	3.91,
	3.92,
	3.93,
	3.94,
	3.95,
	3.96,
	3.97,
	3.98,
	3.99,
	4,
	4.01,
	4.02,
	4.03,
	4.04,
	4.05,
	4.06,
	4.07,
	4.08,
	4.09,
	4.1,
	4.11,
	4.12,
	4.13,
	4.14,
	4.15,
	4.16,
	4.17,
	4.18,
	4.19,
	4.2
};

/* Private function prototypes -----------------------------------------------*/
void taskclass1(void);
void taskclass1_shifted(void);
void taskclass2(void);
void soft_rt_taskclass(void);
uint8_t get_battery_charge(double u_battery_v);
void usart1_rx_handler(void);
void resume_from_stop_mode(wakeup_source_t wakeup_source);
void set_alarm(void);

/* Private functions ---------------------------------------------------------*/
/** @defgroup Tasks_Private_Functions
  * @{
  */

/**
  * @brief  fastest taskclass with highest priority (called periodically)
  */
void taskclass1()
{	
#ifdef DEBUG
	if(g_enter_stop_mode) return;
#endif

#ifdef __USING_7S
	LIB_7S_digit_increment();
#endif // __USING_7S
	
/* states */
    enum state_t {
			START = 1,
			WAIT_FOR_AMP_POS,
      MEASURE_POS,        ///< measure state
			WAIT_FOR_AMP_NEG,
      MEASURE_NEG,        ///< measure state
			WAIT_FOR_LORA_MCU,
			CALC,
			CHECK_STATUS,
			SAVE,
			CHECK_DEVIATIONS,
			PUBLISH_FLASH,
			WAKEUP_LORA_MCU,
			PUBLISH_UART,
			WAIT_FOR_ACK,
			SLEEP,
    };
		
		typedef enum bridge_state_e
		{
			OFF,
			HIGH,
			LOW
		} bridge_state_t;

    /* current state of the state machine */
    static enum state_t state = START;

    /* previous state of the state machine */
    static enum state_t mstate = SLEEP;

    static uint16_t state_calls = 0;
		static double u_amplifier_pos_rel = 0;
		static double u_amplifier_neg_rel = 0;
		static double u_amplifier_rel = 0;
		static double u_offset_rel = 0;
		static double u_manual_offset_rel = 0;
		static double u_battery_v = 0;
		static double u_ext_supply_v = 0;
		static double weight_kg = 0;
		
		static uint8_t o_battery_low;
		static uint8_t o_charging;
		static uint8_t o_ext_supply_uvlo;
		static uint8_t o_swarming;
		
		static uint8_t battery_charge_p;
		static uint8_t request_temp_flag = 0;
		static uint8_t measure_battery = 0;
		static bridge_state_t bridge_state = OFF;

		datetime_t datetime; 
		static datetime_t last_saved_datetime; 
		static datetime_t last_measured_datetime; 
    API_RTC_get_datetime(&datetime, TM_RTC_Format_BCD);
		
		static LIB_hw_signal_t n_charging_signal = { N_CHARGE_PP };
		LIB_hw_signal_read(&n_charging_signal, T_PERIOD1_S);
		
		const uint8_t  i_charging = !n_charging_signal.o_debounced;
		const int32_t  i_amplifier = adc_data[AMPLIFIER_ADC_CH];
		const uint16_t i_battery = adc_data[BATTERY_ADC_CH];
		const uint16_t i_ext_supply = adc_data[EXT_SUPPLY_ADC_CH];
		const uint8_t  i_lora_data_ready = g_datetime_received_flag;
		const uint16_t i_amb_temperature = g_amb_temperature;
		const uint16_t i_amb_humidity = g_amb_humidity;
		const uint32_t i_lora_datetime_unix = g_lora_datetime_unix;
		const uint8_t  i_lora_ack = g_lora_ack_received_flag;
		const uint8_t  i_zeroing = g_zeroing; 
		
    /* calculate the state */
    do
    {
        /* calculate the time in the current state */
        if(state != mstate) {
            state_calls = 0;
        } else {
            state_calls++;
        }

        /* set the previous state */
        mstate = state;

        switch(state) {
            /* i_start_measure triggers the
             * reference-measurement in the WAIT-state,
             * the current led group is set to 0 */
            case START:
							if(state_calls > 0) {
								API_setActiveDO(LED_PP,             Bit_SET);
								LIB_hw_signal_reset(&n_charging_signal);
								state = WAIT_FOR_AMP_POS;
								request_temp_flag = 1;
								g_temp_received_flag = 0;
								g_datetime_received_flag = 0;
								g_lora_ack_received_flag = 0;
							}
              break;
            /* after the reference measurement, a timespan of
             * LED_ON_TIME_S is waited to switch the
             * photo-transistor on and the voltage
             * reference measurement is started
             */
            case WAIT_FOR_AMP_POS:
							if(state_calls > 0 && g_active && !request_temp_flag && bridge_state == HIGH && !i_zeroing)
							{
								bridge_state = HIGH;
								u_amplifier_rel = i_amplifier / 4095.0 - u_offset_rel;
								state = CALC;
							}
							else if(state_calls * T_PERIOD1_S > 0.3 && g_active && !request_temp_flag && !i_zeroing)
							{
								bridge_state = HIGH;
								u_amplifier_rel = i_amplifier / 4095.0 - u_offset_rel;
								state = CALC;
							}
							else if(state_calls * T_PERIOD1_S > 0.3) {
								if(datetime.minutes < last_measured_datetime.minutes) {
									bridge_state = HIGH;
									state = MEASURE_POS;
								}
								else if(g_active) {
									bridge_state = HIGH;
									state = MEASURE_POS;
								}
								else if(request_temp_flag) {
									bridge_state = HIGH;
									state = MEASURE_POS;
								}
								else if(i_zeroing) {
									bridge_state = HIGH;
									state = MEASURE_POS;
								}
								else {
									bridge_state = HIGH;
								  u_amplifier_rel = i_amplifier / 4095.0 - u_offset_rel;
									state = CALC;
								}
							}
							
							if(battery_charge_p == 0) { measure_battery = 1; }
							else if(!g_active)				{ measure_battery = 1; }
							else											{ measure_battery = 0; }
							
							break;
            /* in the MEASURE-state, the actual measurement of the
             * current led group, defined by led_count, is done.
             * if all led groups are measured, the SEND_DATA state is the next
             * state and the output-data gets calculated
             */
            case MEASURE_POS:
							if(state_calls == 0) 											 { u_amplifier_pos_rel = i_amplifier;																									}
							else if(state_calls < MEASURE_CYCLES - 1)  { u_amplifier_pos_rel += i_amplifier;																							  }
							else if(state_calls == MEASURE_CYCLES - 1) { u_amplifier_pos_rel = (u_amplifier_pos_rel + i_amplifier) / (4095.0 * MEASURE_CYCLES);	}
							// else;
							
							if(measure_battery && state_calls < 1)							           { u_battery_v = 0;					 }
							else if(measure_battery && state_calls < MEASURE_CYCLES)	     { u_battery_v += i_battery; }
							// else;
							
							//if(state_calls < 1);
							//else if(state_calls < 2) { u_ext_supply_v = i_ext_supply * U_EXT_SUPPLY_SCALE * g_adc_ref_v / 4095.0;	}
							// else;
						
							if(state_calls == MEASURE_CYCLES - 1) {	state = WAIT_FOR_AMP_NEG; }	
							// else;
							
							break;
							
						case WAIT_FOR_AMP_NEG:
							if(state_calls * T_PERIOD1_S > 0.3) 
							{	
								bridge_state = LOW;
								state = MEASURE_NEG; 
							}
							
							break;
            /* in the MEASURE-state, the actual measurement of the
             * current led group, defined by led_count, is done.
             * if all led groups are measured, the SEND_DATA state is the next
             * state and the output-data gets calculated
             */
            case MEASURE_NEG:
							if(state_calls == 0) 											 { u_amplifier_neg_rel = i_amplifier;																								      }
							else if(state_calls < MEASURE_CYCLES - 1)  { u_amplifier_neg_rel += i_amplifier;																							      }
							else if(state_calls == MEASURE_CYCLES - 1) { u_amplifier_neg_rel = (u_amplifier_neg_rel + i_amplifier) / (4095.0 * MEASURE_CYCLES);	}
							// else;
							
							
							if(measure_battery && state_calls < MEASURE_CYCLES - 1)	      { u_battery_v += i_battery;						    																												               }
							else if(measure_battery && state_calls == MEASURE_CYCLES - 1) { u_battery_v = (u_battery_v + i_battery) * U_BATTERY_SCALE * g_adc_ref_v / (4095.0 * (2 * MEASURE_CYCLES - 1));	
																																				      battery_charge_p = get_battery_charge(u_battery_v);                                                            }
							// else;
																													 
							if(state_calls == MEASURE_CYCLES - 1) 
							{ 
								u_amplifier_rel = (u_amplifier_pos_rel - u_amplifier_neg_rel) / 2.0; 
								u_offset_rel = (u_amplifier_pos_rel + u_amplifier_neg_rel) / 2.0; 
							}
							
							if(i_zeroing && state_calls == MEASURE_CYCLES - 1) 
							{ 
								u_manual_offset_rel = u_amplifier_rel;
							}
						
							if(state_calls == MEASURE_CYCLES - 1 && request_temp_flag) { state = WAIT_FOR_LORA_MCU; }
							else if(state_calls == MEASURE_CYCLES - 1)                 { state = CALC;              }
							// else;
							
							break;
							
						case WAIT_FOR_LORA_MCU:
							if(state_calls > 0 && i_lora_data_ready)
							{
								API_RTC_get_datetime_from_unix(&datetime, i_lora_datetime_unix);
								API_RTC_set_datetime(&datetime, TM_RTC_Format_BCD);
								state = CALC;
								g_temp_received_flag = 0;
								g_datetime_received_flag = 0;
								request_temp_flag = 0;
							}
							else if((double)state_calls * T_PERIOD1_S > 0.4)
							{
 								state = START;
							}

							break;
							
						case CALC:
							if(state_calls > 0)
							{
								//weight_kg = (COMMON_CORR * SHAFT_AREA * SIGMA_REL / AMPLIFIER_GAIN / 1000.0) * u_amplifier_rel;
								weight_kg = (u_amplifier_rel - u_manual_offset_rel) / AMPLIFIER_GAIN * COMMON_CORR * g_adc_ref_v;
								last_measured_datetime = datetime;
								state = CHECK_STATUS;
							}
							break;
						
						case CHECK_STATUS:
							o_charging = i_charging;
						
							if(battery_charge_p < 10) { o_battery_low = 1; }
							else									    { o_battery_low = 0; }
							
							if(u_ext_supply_v < EXT_SUPPLY_UVLO_V) { o_ext_supply_uvlo = 1; }
							else 	  															 { o_ext_supply_uvlo = 0; }
							
							o_swarming = 0;
							
							state = SAVE;
							break;
						
						case SAVE:
							m_data_set.datetime        = datetime.unix;
							m_data_set.weight          = weight_kg * WEIGHT_INT_SCALE;
							m_data_set.amb_temperature = i_amb_temperature;
							m_data_set.amb_humidity    = i_amb_humidity;
							m_data_set.battery_charge  = battery_charge_p * BATTERY_INT_SCALE;
							m_data_set.u_ext_supply    = u_ext_supply_v * U_EXT_SUPPLY_INT_SCALE;
							m_data_set.status          = (o_charging        << CHARGING_BITPOS)        |
																	         (o_battery_low     << BATTERY_LOW_BITPOS)     |
																           (o_ext_supply_uvlo << EXT_SUPPLY_UVLO_BITPOS) |
																	         (o_swarming        << SWARMING_BITPOS);
						
						  if(datetime.minutes > last_saved_datetime.minutes || 
						     (datetime.minutes == 0 && last_saved_datetime.minutes == 59)) {
						    data_sets_min[datetime.minutes] = m_data_set;
					    }
						
							if(datetime.minutes < last_saved_datetime.minutes) { 
								data_sets_h[datetime.hours] = m_data_set;
							}
							
							if(datetime.hours < last_saved_datetime.hours) { 
								data_sets_d[datetime.day] = m_data_set;
							}
							
							if(datetime.date < last_saved_datetime.date) { 
								data_sets_m[datetime.month] = m_data_set;
							}
							
							last_saved_datetime = datetime;
							state = CHECK_DEVIATIONS;
				
							break;
							
						case CHECK_DEVIATIONS:														
						       if(m_data_set.datetime        - last_published_data_set.datetime        < 60 && g_acquire_data_flag)                  			    { state = PUBLISH_UART; 					}
							else if(m_data_set.datetime        - last_published_data_set.datetime        < 60 && g_active)                             			    { state = WAIT_FOR_AMP_POS; 			}
							else if(m_data_set.datetime        - last_published_data_set.datetime        < 60)                                         			    { state = SLEEP;            			}
							else if(m_data_set.datetime        - last_published_data_set.datetime        >= 60*60)                  							  					  { state = PUBLISH_FLASH; 					}
							else if(m_data_set.weight          - last_published_data_set.weight          > TRIGGER_WEIGHT_DIFF_KG * WEIGHT_INT_SCALE)    				{ state = PUBLISH_FLASH; 					}
							else if(m_data_set.weight          - last_published_data_set.weight          < -TRIGGER_WEIGHT_DIFF_KG * WEIGHT_INT_SCALE)    			{ state = PUBLISH_FLASH; 					}
							else if(m_data_set.amb_temperature - last_published_data_set.amb_temperature > TRIGGER_AMB_TEMP_DIFF_C * AMB_TEMP_INT_SCALE)  			{	state = PUBLISH_FLASH; 					}
							else if(m_data_set.amb_temperature - last_published_data_set.amb_temperature < -TRIGGER_AMB_TEMP_DIFF_C * AMB_TEMP_INT_SCALE) 			{ state = PUBLISH_FLASH; 					}
							else if(m_data_set.amb_humidity    - last_published_data_set.amb_humidity    > TRIGGER_AMB_HUM_DIFF_P * AMB_HUM_INT_SCALE)					{ state = PUBLISH_FLASH; 					}
							else if(m_data_set.amb_humidity    - last_published_data_set.amb_humidity    < -TRIGGER_AMB_HUM_DIFF_P * AMB_HUM_INT_SCALE)					{ state = PUBLISH_FLASH; 					}
							else if(m_data_set.battery_charge  - last_published_data_set.battery_charge  < -TRIGGER_BAT_CHARGE_DIFF_P) 													{ state = PUBLISH_FLASH; 					}
							else if(m_data_set.u_ext_supply    - last_published_data_set.u_ext_supply    > TRIGGER_EXT_SUPPLY_DIFF_V * U_EXT_SUPPLY_INT_SCALE) 	{ state = PUBLISH_FLASH; 					}
							else if(m_data_set.u_ext_supply    - last_published_data_set.u_ext_supply    < -TRIGGER_EXT_SUPPLY_DIFF_V * U_EXT_SUPPLY_INT_SCALE) { state = PUBLISH_FLASH; 					}
							else if(m_data_set.status != last_published_data_set.status)                                              													{ state = PUBLISH_FLASH; 		    	}
							else if(request_temp_flag)				                                                                              										{ state = PUBLISH_UART; 					}
							else if(g_acquire_data_flag)																																																				{ state = PUBLISH_UART;           }
							else if(g_active)																																											  														{ state = WAIT_FOR_AMP_POS;       }
							else 																																																																{ state = SLEEP;        		      }
							
							break;
						
						case PUBLISH_FLASH:
							last_published_data_set = m_data_set;
							
							if(state_calls > 0) 
							{ 
								flash_address += sizeof(data_set_t);
								g_temp_received_flag = 0;
								g_datetime_received_flag = 0;
								state = WAKEUP_LORA_MCU; 
							}

							break;
							
						case WAKEUP_LORA_MCU:
							if(state_calls > 0 && i_lora_data_ready) 
							{	
								state = PUBLISH_UART;            
								g_temp_received_flag = 0;
								g_datetime_received_flag = 0;
							}
							if (state_calls * T_PERIOD1_S > 0.2) 
							{ 
								state = PUBLISH_UART; 
							}
							break;
							
						case PUBLISH_UART:
							last_published_data_set = m_data_set;
							if(state_calls > 0) { state = WAIT_FOR_ACK; }
							break;
							
						case WAIT_FOR_ACK:
							if(state_calls > 0 && i_lora_ack && g_active) 
							{	
								state = WAIT_FOR_AMP_POS; 
								g_lora_ack_received_flag = 0; 
								g_acquire_data_flag = 0;
							}
							else if(state_calls > 0 && i_lora_ack) 
							{	
								state = SLEEP;            
								g_lora_ack_received_flag = 0; 
								g_acquire_data_flag = 0;
							}
							else if(state_calls * T_PERIOD1_S > 0.2) 
							{ 
								API_USART_DMA_receive(&API_USART1, 2 * sizeof(sensor_control_t));
								g_temp_received_flag = 0;
								g_datetime_received_flag = 0;
								state = WAKEUP_LORA_MCU; 
							}
						  break;
								
						case SLEEP:
							if(state_calls > 1 && !g_enter_stop_mode) 
							{
								state = START;
							}
							else if(state_calls > 0) 
							{ 
								bridge_state = OFF;
							  g_enter_stop_mode = 1;
							}
							// else;
							
							break;
        }
    } while (state != mstate);
      
    /* power on Amplifier and LoRa */
    if(state == START) {
      API_setActiveDO(AMPLIFIER_EN_PP, Bit_RESET);
			API_setActiveDO(LORA_WKUP_PP, Bit_RESET);
			API_USART_DMA_receive(&API_USART1, 2 * sizeof(sensor_control_t));
    }
		else if(state == WAIT_FOR_AMP_POS)
		{
			API_setActiveDO(H_BRIDGE_LOW_POS,  Bit_RESET);
			API_setActiveDO(H_BRIDGE_HIGH_POS, Bit_RESET);
			API_setActiveDO(H_BRIDGE_HIGH_NEG, Bit_SET);
			API_setActiveDO(H_BRIDGE_LOW_NEG,  Bit_SET);
		}
		else if(state == MEASURE_POS) {
      API_setActiveDO(BATTERY_MEASURE_PP, Bit_RESET);
    }
		else if(state == WAIT_FOR_AMP_NEG)
		{
			API_setActiveDO(LORA_WKUP_PP,       Bit_SET);
			API_setActiveDO(H_BRIDGE_HIGH_POS,  Bit_SET);
			API_setActiveDO(H_BRIDGE_LOW_POS,   Bit_SET);
			API_setActiveDO(H_BRIDGE_LOW_NEG,   Bit_RESET);
			API_setActiveDO(H_BRIDGE_HIGH_NEG,  Bit_RESET);
		}
		else if(state == WAIT_FOR_LORA_MCU)
		{
			API_setActiveDO(BATTERY_MEASURE_PP, Bit_SET);
		}
		else if(state == CALC)
		{
			API_setActiveDO(BATTERY_MEASURE_PP, Bit_SET);
		}
    else if (state == SLEEP) 
		{
      //API_setActiveDO(AMPLIFIER_EN_PP,    Bit_SET); /* BUG: Main MCU resets after wake-up from sleep
		  API_setActiveDO(H_BRIDGE_HIGH_POS,  Bit_SET);
			API_setActiveDO(H_BRIDGE_HIGH_NEG,  Bit_SET);
			API_setActiveDO(H_BRIDGE_LOW_POS,   Bit_RESET);
			API_setActiveDO(H_BRIDGE_LOW_NEG,   Bit_RESET);
			API_setActiveDO(LED_PP,             Bit_RESET);
    }
    else if(state == PUBLISH_FLASH)
    {
			API_FLASH_write_halfwords(flash_address, flash_address + sizeof(data_set_t), (uint16_t*)&last_published_data_set);
			API_USART_DMA_receive(&API_USART1, 2 * sizeof(sensor_control_t));
    }
		else if(state == WAKEUP_LORA_MCU)
		{
			API_setActiveDO(LORA_WKUP_PP, Bit_SET);
		}
		/* send over USART */
		else if(state == PUBLISH_UART)
    {
			API_setActiveDO(LORA_WKUP_PP, Bit_RESET);
			API_USART_DMA_receive(&API_USART1, sizeof(sensor_control_t));
			API_USART_DMA_send(&API_USART1, sizeof(data_set_t));
    }
		
		g_battery_charge_p = battery_charge_p;

#ifdef __USING_WWDG
  WWDG_SetCounter(0x7F);
#endif // __USING_WWDG
}

/**
  * @brief  fastest taskclass with highest priority (called periodically after taskclass1)
  */
void taskclass1_shifted()
{	
#ifdef __USING_ADC
	API_ADC_DMA_start_single_conv(&API_ADC1);
#endif // __USING_ADC
}

/**
  * @brief  taskclass (called periodically)
  */
void taskclass2()
{
  static uint8_t iniOK = 0;

	static double t_show_s;
	static double time_to_live_s;
	static double weight_filter;
	static double weight_flicker_surpress;
	static parameters_t param;
	
	uint8_t low_bat;

  static LIB_hw_signal_t hw_signal_button0 = { BUTTON0_PP };
  static LIB_hw_signal_t hw_signal_button1 = { BUTTON1_PP };
  static LIB_hw_signal_t hw_signal_button2 = { BUTTON2_PP };
  static LIB_hw_signal_t hw_signal_button3 = { BUTTON3_PP };

#ifdef __USING_IWDG
	if(!iniOK)
	{
		API_IWDG_init(); //updated in taskclass2
	}
#endif // __USING_IWDG
	
	/* read buttons */
	if(!iniOK)
	{
		LIB_hw_signal_reset(&hw_signal_button0);
		LIB_hw_signal_reset(&hw_signal_button1);
		LIB_hw_signal_reset(&hw_signal_button2);
		LIB_hw_signal_reset(&hw_signal_button3);
	}
	else
	{
		LIB_hw_signal_read(&hw_signal_button0, T_PERIOD2_S);
		LIB_hw_signal_read(&hw_signal_button1, T_PERIOD2_S);
		LIB_hw_signal_read(&hw_signal_button2, T_PERIOD2_S);
		LIB_hw_signal_read(&hw_signal_button3, T_PERIOD2_S);
	}
	
	/* detect low battery */
	if(!iniOK)                                                   { low_bat = 0; }
	else if(m_data_set.battery_charge == 0)                      { low_bat = 0; }
	else if(m_data_set.battery_charge / BATTERY_INT_SCALE <= 10) { low_bat = 1; }
	// else;
		
	/* control user LED */
	if(!iniOK);
	else if(m_data_set.status & (1 << BATTERY_LOW_BITPOS)) { API_setActiveDO(LED_PP, Bit_SET);   }
	//else 																							     { API_setActiveDO(LED_PP, Bit_RESET); }
	
	if(!iniOK) 															              { g_zeroing = 0; }
	else if(hw_signal_button0.t_off_s >= LONG_PRESS_TIME) { g_zeroing = 1; }
	else																		              { g_zeroing = 0; }
	
	if(!iniOK) 															                                                                            { g_acquire_data_flag = 0; }
	else if(hw_signal_button1.t_off_s >= LONG_PRESS_TIME && hw_signal_button1.t_off_s < LONG_PRESS_TIME + T_PERIOD2_S ) { g_acquire_data_flag = 1; }
	//else;
	
	/* parameter to show */
	if(!iniOK && g_wakeup_source == EMPTY) { param = WEIGHT;           }
	else if(g_wakeup_source == BUTTON0)		 { param = WEIGHT;  }
	else if(hw_signal_button0.o_neg_edge)  { param = WEIGHT;  }
	else if(g_wakeup_source == BUTTON1)		 { param = AMB_TEMPERATURE;  }
	else if(hw_signal_button1.o_neg_edge)  { param = AMB_TEMPERATURE;  }
	else if(g_wakeup_source == BUTTON2)		 { param = AMB_HUMIDITY;  }
	else if(hw_signal_button2.o_neg_edge)  { param = AMB_HUMIDITY;  }
	else if(g_wakeup_source == BUTTON3)		 { param = BATTERY_CHARGE;  }
	else if(hw_signal_button3.o_neg_edge)  { param = BATTERY_CHARGE;  }
	else if(t_show_s > PARAM_SHOW_TIME_S)  { param = WEIGHT;        }
	// else;
	
	if(!iniOK);
	else { g_wakeup_source = EMPTY; }
	// else

	/* time to show a parameter */
	if(!iniOK) 		                          { t_show_s = 0; 										 }
	else if(!hw_signal_button0.o_debounced) { t_show_s = 0; 										 }
	else if(!hw_signal_button1.o_debounced) { t_show_s = 0; 										 }
	else if(!hw_signal_button2.o_debounced) { t_show_s = 0; 										 }
	else if(!hw_signal_button3.o_debounced) { t_show_s = 0; 										 }
	else if(param != WEIGHT)                { t_show_s = t_show_s + T_PERIOD2_S; }
	else 					                          { t_show_s = 0; 										 }

	/* time to live */
	if(!iniOK) 		                       	 	{ time_to_live_s = TIME_TO_LIFE_S; }
	else if(!hw_signal_button0.o_debounced) { time_to_live_s = TIME_TO_LIFE_S; }
	else if(!hw_signal_button1.o_debounced) { time_to_live_s = TIME_TO_LIFE_S; }
	else if(!hw_signal_button2.o_debounced) { time_to_live_s = TIME_TO_LIFE_S; }
	else if(!hw_signal_button3.o_debounced) { time_to_live_s = TIME_TO_LIFE_S; }
	else                                    { time_to_live_s -= T_PERIOD2_S;   }
	
	if(!iniOK)  { weight_filter = (double)m_data_set.weight / WEIGHT_INT_SCALE; }
	else { weight_filter = /*0.9 * weight_filter + 0.1 **/ (double)m_data_set.weight / WEIGHT_INT_SCALE; }
	
	if(!iniOK) { weight_flicker_surpress = weight_filter; }
	else if((uint16_t)(weight_filter*10) >= (uint16_t)(weight_flicker_surpress*10) + 1) { weight_flicker_surpress = weight_filter-0.05; }
	else if((uint16_t)(weight_filter*10) <= (uint16_t)(weight_flicker_surpress*10) - 1) { weight_flicker_surpress = weight_filter+0.05; }
	// else;

	/* seven segment output */
	if(param == WEIGHT)                 { LIB_7S_set_format(1, 1, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number(weight_flicker_surpress);      }
	else if(param == AMB_TEMPERATURE)   { LIB_7S_set_format(1, 1, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number((double)m_data_set.amb_temperature / AMB_TEMP_INT_SCALE);      }
	else if(param == AMB_HUMIDITY)      { LIB_7S_set_format(1, 0, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number((double)m_data_set.amb_humidity / AMB_HUM_INT_SCALE);      }
	else if(param == BATTERY_CHARGE) 	  {	LIB_7S_set_format(2, 0, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number(g_battery_charge_p);                            }
	else if(param == EXT_VOLTAGE)       { LIB_7S_set_format(1, 2, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number((double)m_data_set.u_ext_supply / U_EXT_SUPPLY_INT_SCALE); }
	else if(param == REFERENCE_VOLTAGE) { LIB_7S_set_format(1, 2, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number((double)adc_data[REFERENCE_ADC_CH] / 4095.0);   }
	else if(param == AMPLIFIER_VOLTAGE) { LIB_7S_set_format(1, 2, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number((double)adc_data[AMPLIFIER_ADC_CH] / 4095.0);   }
	else 															  { LIB_7S_SPI_clear();                             																	                                }

	/* power LED driver */
	if(!iniOK)									 { API_setActiveDO(LED_DRIVER_EN_PP, Bit_RESET); }
	else if(time_to_live_s <= 0) { API_setActiveDO(LED_DRIVER_EN_PP, Bit_SET);   }
	// else
	
	/* shutdown */
	if(time_to_live_s <= 0 || low_bat)
	{
		API_conf_EXTI_external(BUTTON0_PP, 4);
		API_conf_EXTI_external(BUTTON1_PP, 4);
		API_conf_EXTI_external(BUTTON2_PP, 4);
		API_conf_EXTI_external(BUTTON3_PP, 4);
		
		TIM_Cmd(TIM14, DISABLE);
		
		iniOK = 0;
		g_active = 0;
	}
	else
	{
		g_active = 1;
		iniOK = 1;
	}

#ifdef __USING_IWDG
  IWDG_ReloadCounter();
#endif // __USING_IWDG
}

/**
  * @brief  slowest taskclass (soft-realtime; called periodically in taskclass2)
  */
void soft_rt_taskclass()
{
	static uint8_t led = 1;
	//if(led) API_setDO(LED_PP, Bit_SET);
	//else                                          API_setDO(LED_PP, Bit_RESET);
	//if(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_10)) API_setDO(LED_PP, Bit_SET);
	//else                                          API_setDO(LED_PP, Bit_RESET);
	
	led = !led;
}

void resume_from_stop_mode(wakeup_source_t wakeup_source)
{
	API_EXTI_deinit();
	set_alarm();
	g_wakeup_source = wakeup_source;
	g_active = 1;
	
	#ifdef DEBUG
		g_enter_stop_mode = 0;
	#endif
	
	if(wakeup_source != RTC_ALARM)
	{
		TIM_Cmd(TIM14, ENABLE);
	}
}

void usart1_rx_handler()
{
	uint32_t* p_data;
	
	if(g_sensor_control_buffer[0].type == ACK)
	{
		g_lora_ack_received_flag = 1;
	}
	if(g_sensor_control_buffer[0].type == ACQUIRE_DATA)
	{
		g_acquire_data_flag = 1;
	}
	if(g_sensor_control_buffer[0].type == REQUEST_DATA)
	{
		p_data = (uint32_t*)flash_address;
		API_USART_DMA_send_buffer(&API_USART1, p_data, sizeof(data_set_t));
	}
	if(g_sensor_control_buffer[0].type == REQUEST_DATA_SINCE)
	{
		p_data = (uint32_t*)flash_address;
		API_USART_DMA_send_buffer(&API_USART1, p_data, (uint32_t)p_data - FLASH_START_ADDRESS);
	}
	if(g_sensor_control_buffer[0].type == REQUEST_DATA_BEFORE)
	{
		p_data = (uint32_t*)FLASH_START_ADDRESS;
		API_USART_DMA_send_buffer(&API_USART1, p_data, flash_address - FLASH_START_ADDRESS);
	}
	if(g_sensor_control_buffer[0].type == SET_TEMPERATURE)
	{
		g_amb_temperature    = (uint16_t)(g_sensor_control_buffer[0].data >> 16);
		g_amb_humidity       = (uint16_t)(g_sensor_control_buffer[0].data >> 0);
		g_temp_received_flag = 1;
	}
	if(g_sensor_control_buffer[1].type == SET_TIME)
	{
		g_lora_datetime_unix     = g_sensor_control_buffer[1].data;
		g_datetime_received_flag = 1;
	}
	
	API_USART_DMA_receive(&API_USART1, sizeof(sensor_control_t));
}

uint8_t get_battery_charge(double u_battery_v)
{
	uint8_t i;
	
	for(i = 0; i < sizeof(battery_charge_lt) / 8; i++)
	{
		if(u_battery_v <= battery_charge_lt[i]) { return i * 100 / (sizeof(battery_charge_lt) / 8); }
	}
	
	return 100;
}

void API_RTC_request_handler()
{
	API_TIM_start(&API_TIM16, TIM_IT_CC1, T_PERIOD1_S, 0);
}

void set_alarm()
{
	EXTI_InitTypeDef EXTI_InitStructure;
  RTC_AlarmTypeDef RTC_AlarmStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  RTC_AlarmStructInit(&RTC_AlarmStructure);
	
	/* Allow access to RTC */
  PWR_BackupAccessCmd(ENABLE);
	
	/* Disable the alarmA */
  RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
    
  /* EXTI configuration */
  EXTI_ClearITPendingBit(EXTI_Line17);
  EXTI_InitStructure.EXTI_Line = EXTI_Line17;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
  
  /* Enable the RTC Alarm Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 3;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
 
  /* Set the alarmA Masks */
  RTC_AlarmStructure.RTC_AlarmMask = RTC_AlarmMask_Minutes | RTC_AlarmMask_Hours | RTC_AlarmMask_DateWeekDay;
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = 0;
  RTC_SetAlarm(RTC_Format_BCD, RTC_Alarm_A, &RTC_AlarmStructure);
  
  /* Enable AlarmA interrupt */
  RTC_ITConfig(RTC_IT_ALRA, ENABLE);
	
	/* Enable the alarmA */
  RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
}

/**
  * @brief startup - function, called in the main module
*/
void startup_sequence(void)
{
	RCC_HSEConfig(RCC_HSE_OFF); // disable HSE to enable GPIO mode on PF0 and PF1
  SystemCoreClockUpdate();

	/* disable wheatstone bridge */
	API_setDO(H_BRIDGE_LOW_NEG, Bit_RESET);
	API_setDO(H_BRIDGE_LOW_POS, Bit_RESET);
	API_setDO(H_BRIDGE_HIGH_NEG, Bit_SET);
	API_setDO(H_BRIDGE_HIGH_POS, Bit_SET);

	/* enable amplifier */
	API_setDO(AMPLIFIER_EN_PP, Bit_RESET);
	
	/* disable battery measurement */
	API_setDO(BATTERY_MEASURE_PP, Bit_SET);
	
	/* disable LED driver */
	API_setDO(LED_DRIVER_EN_PP, Bit_SET);
	
	/* init LED */
	API_setDO(LED_PP, Bit_RESET);
	
	/* init 7-segment digits */
	API_setDO(DIGIT1_PP, Bit_SET);
	API_setDO(DIGIT2_PP, Bit_SET);
	API_setDO(DIGIT3_PP, Bit_SET);
	
	/* init LoRa wakeup pin */
	API_setDO(LORA_WKUP_PP, Bit_RESET);
	
	/* enable battery charger */
	API_setDO(N_CHARGE_ENABLE_PP, Bit_SET);
	
	/* configure inputs */
	API_getDI(N_CHARGE_PP);
	
	flash_address = API_FLASH_get_next_free_address(FLASH_START_ADDRESS, FLASH_END_ADDRESS);
	
	/* UART for printf */
#ifdef __USING_USART
	API_USART1.DMA_Channel_x_Tx = DMA1_Channel4;
	API_USART1.DMA_Channel_x_Rx = DMA1_Channel5;
	API_USART1.DMA_IRQn = DMA1_Channel4_5_IRQn;
  API_USART_DMA_init(&API_USART1, (uint8_t*)&last_published_data_set, (uint8_t*)g_sensor_control_buffer, 4, 4);
	API_USART_init(&API_USART1, USART_TX_PP, USART_RX_PP, USART_BAUDRATE, 6);
	API_USART_DMA_receive(&API_USART1, 2 * sizeof(sensor_control_t));
#endif // __USING_USART

#ifdef __USING_ADC
  g_adc_ref_v = 3.0;
	API_ADC_DMA_init(	&API_ADC1, 
										(uint16_t*)adc_data, 
										ADC_SampleTime_239_5Cycles, 
										4, 
										AMPLIFIER_ADC_PP,
										EXT_ADC_PP, 
										REFERENCE_ADC_PP,
										BATTERY_ADC_PP );
#endif // __USING_ADC

  API_RTC_init(TM_RTC_ClockSource_Internal);
	set_alarm();

#ifdef __USING_TIMER
	#ifdef __USING_7S
		#ifdef __USING_SPI
			LIB_7S_set_display_assembly(3, COMMON_ANODE);
			LIB_7S_set_segment_bit_position(3, 4, 6, 7, 8, 2, 1, 5);
			LIB_7S_set_digit_portpin(3, DIGIT1_PP, DIGIT2_PP, DIGIT3_PP);
			LIB_7S_set_format(1, 1, RIGHT, 0);
		
			LIB_7S_SPI_init(&API_SPI1, 
											SPI_MOSI_PP, 
											SPI_CLK_PP, 
											LE_PP, 
											N_OE_PP, 
											SPI_BaudRatePrescaler_128, 
											SPI_MODE_0);
			LIB_7S_SPI_DMA_TIM_start_continious_tx(&API_TIM16, T_PERIOD1_S);
		#endif // __USING_SPI
	#endif // __USING_7S

  API_TIM_start(&API_TIM16, TIM_IT_CC1, T_PERIOD1_S, 0);
  API_PWM_start(&API_TIM16, CH1, N_OE_PP, T_PERIOD1_S, 0.5, 0xFF, TIM_OCPolarity_High);
  API_TIM_start(&API_TIM14, TIM_IT_Update, T_PERIOD2_S, 5);
	
#endif // __USING_TIMER
	
#ifdef __USING_WWDG  
	API_WWDG_init();
#endif // __USING_WWDG
}

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 
