/**
  ******************************************************************************
  * @file    stm32f0xx_it.c 
  * @author  MCD Application Team
  * @version V1.5.0
  * @date    05-December-2014
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "./../inc/stm32f0xx_it.h"

/** @addtogroup System_Core
  * @{
  */

/** @defgroup IRQ_Handler 
  * @brief interrupt handler
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t soft_rt_taskclass_signal;

/* Private function prototypes -----------------------------------------------*/
extern void taskclass1(void);
extern void taskclass1_shifted(void);
extern void taskclass2(void);
extern void soft_rt_taskclass(void);

/* Private functions ---------------------------------------------------------*/

/** @defgroup IRQ_Handler_Private_Functions
  * @{
  */
	
/** @defgroup Processor_Exceptions_Handlers
  * @{
  */
	
/******************************************************************************/
/*            Cortex-M0 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  */
void HardFault_Handler(void)
{
#ifdef __USING_DEBUG
  LIB_DEBUG_hardfault_handler_wrapper();
#endif // __USING_DEBUG

  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  */
void SysTick_Handler(void)
{
}

/**
  * @}
  */

/******************************************************************************/
/*                 STM32F0xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f0xx.s).                                            */
/******************************************************************************/

void DMA1_Channel4_5_IRQHandler(void)
{
	if(DMA_GetITStatus(DMA1_IT_TC4))
	{
		DMA_ClearITPendingBit(DMA1_IT_TC4);
	}
	
	if(DMA_GetITStatus(DMA1_IT_TC5))
	{
		usart1_rx_handler();
		DMA_ClearITPendingBit(DMA1_IT_TC5);
	}
}

#ifdef __USING_USART
void USART1_IRQHandler(void)
{
	if(USART_GetFlagStatus(USART1, USART_IT_TC))
	{
		USART_ClearITPendingBit(USART1,  USART_IT_TC);
	}
	else if(USART_GetFlagStatus(USART1, USART_IT_RXNE))
	{
		usart1_rx_handler();
		USART_ClearITPendingBit(USART1,  USART_IT_RXNE);
	}
}
#endif
/**
  * @brief  This function handles PPP interrupt request.
  */
/*void PPP_IRQHandler(void)
{
}*/

#ifdef __USING_TIMER
/** @defgroup Timer_IRQ_Handler
  * @{
  */
	
/**
  * @brief  This function handles TIM3_IRQn
  */
void TIM3_IRQHandler(void){

  TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
}

/**
  * @brief  This function handles TIM16_IRQn
  */
void TIM16_IRQHandler(void){
	if(TIM_GetFlagStatus(TIM16, TIM_IT_Update))
	{
		taskclass1();
		
		TIM_ClearITPendingBit(TIM16, TIM_IT_Update);
	}
	
	if(TIM_GetFlagStatus(TIM16, TIM_IT_CC1))
	{
		taskclass1_shifted();
		
		TIM_ClearITPendingBit(TIM16, TIM_IT_CC1);
	}
}

/**
  * @brief  This function handles TIM14_IRQn
  */
void TIM14_IRQHandler(void){
	/* timing for soft_rt_taskclass */
	static double  t = 0;

	if(t + T_PERIOD2_S < T_PERIOD_SOFT_RT_S)
	{
			t = t + T_PERIOD2_S;
	}
	else
	{
			t = t + T_PERIOD2_S - T_PERIOD_SOFT_RT_S;
			soft_rt_taskclass_signal = 1;
	}

	taskclass2();

  TIM_ClearITPendingBit(TIM14, TIM_IT_Update);
}

/**
  * @}
  */

#endif // __USING_TIMER

#ifdef __USING_EXTI

void EXTI0_1_IRQHandler(void){
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line0) != RESET) {
        resume_from_stop_mode(BUTTON3);
			
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line0);
    }

		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line1) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line1);
    }
}
		
void EXTI12_3_IRQHandler(void) {
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line2) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line2);
    }

		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line3) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line3);
    }
}

void EXTI4_15_IRQHandler(void){
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line4) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line4);
    }	 
		
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line5) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line5);
    }
		
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line6) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line6);
    }
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line7) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line7);
    }
		
    if (EXTI_GetITStatus(EXTI_Line8) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line8);
    }
		
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line9) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line9);
    }
		
	 /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line10) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line10);
    }
		
	  /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line11) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line11);
    }
		
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line12) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line12);
    }
		
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line13) != RESET) {
        resume_from_stop_mode(BUTTON0);
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line13);
    }
		
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line14) != RESET) {
        resume_from_stop_mode(BUTTON1);
			
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line14);
    }
		
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line15) != RESET) {
        resume_from_stop_mode(BUTTON2);
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line15);
    }
}
#endif // __USING_EXTI

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
