/**
 * @file    ./API/src/SPI.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer
 * @brief   API for SPI
 */
  
#include "../inc/PerUsings.h"
#ifdef __USING_SPI

/* Includes ------------------------------------------------------------------*/
#include "SPI.h"

/** @addtogroup STM32F0xx_API
  * @{
  */

/** @defgroup SPI 
  * @brief SPI API functions
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
API_SPI_type_t API_SPI1 = { 0, SPI1, RCC_APB2Periph_SPI1, GPIO_AF_0, &PA7,  &PA6,  &PA5,  &PA4, DMA1, DMA1_Channel3, DMA1_Channel2, DMA1_Channel2_3_IRQn, RCC_AHBPeriph_DMA1, 0, 0 };
API_SPI_type_t API_SPI2 = { 0, SPI2, RCC_APB1Periph_SPI2, GPIO_AF_0, &PB15, &PB14, &PB13, &PB12, DMA1, DMA1_Channel5, DMA1_Channel4, DMA1_Channel4_5_IRQn, RCC_AHBPeriph_DMA1, 0, 0 };

/* Private function prototypes -----------------------------------------------*/
void API_SPI_init(API_SPI_type_t* API_SPIx, API_GPIO_type_t* mosi_portpin, API_GPIO_type_t* miso_portpin, API_GPIO_type_t* sck_portpin, API_GPIO_type_t* nss_portpin, uint16_t baudrate_prescaler, uint8_t mode);
void API_SPI_DMA_init(API_SPI_type_t* API_SPIx, uint32_t dma_mode, uint8_t* p_tx_data, uint8_t* p_rx_data);
void API_SPI_DMA_TIM_start_continious_tx(API_SPI_type_t* API_SPIx, API_TIM_type_t* API_TIMx, double t_period_s, uint16_t tim_dma_source, uint8_t* p_tx_data, uint16_t tx_length);
void API_SPI_DMA_TIM_stop_continious_tx(API_SPI_type_t* API_SPIx, API_TIM_type_t* API_TIMx);
void API_SPI_DMA_send(API_SPI_type_t* API_SPIx, uint16_t tx_length);
void API_SPI_DMA_send_and_receive(API_SPI_type_t* API_SPIx, uint16_t length);
void API_SPI_send_byte(API_SPI_type_t* API_SPIx, uint8_t data);
uint8_t API_SPI_receive_byte(API_SPI_type_t* API_SPIx);
error_t API_SPI_send_then_receive_statemachine(API_SPI_type_t* API_SPIx, double t_period_s, uint8_t *p_tx_data, uint8_t tx_length, uint8_t *p_rx_data, uint8_t rx_length);
error_t API_SPI_DMA_send_then_receive_statemachine(API_SPI_type_t* API_SPIx, double t_period_s, uint8_t* p_tx_data, uint8_t  tx_length, uint8_t* p_rx_data, uint8_t  rx_length, uint8_t* p_tx_data_while_receiving);  

/* Private functions ---------------------------------------------------------*/
/** @defgroup SPI_Private_Functions
  * @{
  */
	
/**
  * @brief  init SPI
  * @param  API_SPIx:     where x can be (1, 2) to select the SPI
  * @param  mosi_portpin: MOSI portpin definition
  * @param  miso_portpin: MISO portpin definition
  * @param  sck_portpin:  clock portpin definition
  * @param  nss_portpin:  chip select portpin definition
  * @param  baudrate_prescaler: Prescaler for Baudrate:
  *           SPI_BaudRatePrescaler_2:   SCK = 21 MHz
  *           SPI_BaudRatePrescaler_4:   SCK = 10.5 MHz
  *           SPI_BaudRatePrescaler_8:   SCK = 5.25 MHz
  *           SPI_BaudRatePrescaler_16:  SCK = 2.625 MHz
  *           SPI_BaudRatePrescaler_32:  SCK = 1.3125 MHz
  *           SPI_BaudRatePrescaler_64:  SCK = 656.2 kHz
  *           SPI_BaudRatePrescaler_128: SCK = 328.1 kHz
  *           SPI_BaudRatePrescaler_256: SCK = 164.0 kHz
  * @param mode: SPI modes:
  *           SPI_MODE_0: CPOL=0, CPHA=0
  *           SPI_MODE_1: CPOL=0, CPHA=1
  *           SPI_MODE_2: CPOL=1, CPHA=0
  *           SPI_MODE_3: CPOL=1, CPHA=1
  */
void API_SPI_init(API_SPI_type_t* API_SPIx, API_GPIO_type_t* mosi_portpin, API_GPIO_type_t* miso_portpin, API_GPIO_type_t* sck_portpin, API_GPIO_type_t* nss_portpin, uint16_t baudrate_prescaler, uint8_t mode)
{
  SPI_InitTypeDef  SPI_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  uint16_t MOSI_PinSource = 0;
  uint16_t MISO_PinSource = 0;
  uint16_t SCK_PinSource  = 0;
  uint16_t NSS_PinSource  = 0;

  API_SPIx->is_init = 0;
  SPI_I2S_DeInit(API_SPIx->SPIx);
  
  API_SPIx->mosi_portpin = mosi_portpin;
  API_SPIx->miso_portpin = miso_portpin;
  API_SPIx->sck_portpin  = sck_portpin;
  API_SPIx->nss_portpin  = nss_portpin;
  
  if(API_SPIx->SPIx == SPI1)      { RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE); }
  else if(API_SPIx->SPIx == SPI2) { RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE); }

  RCC_AHBPeriphClockCmd(mosi_portpin->RCC_AHB1Periph_GPIOx, ENABLE);
  RCC_AHBPeriphClockCmd(miso_portpin->RCC_AHB1Periph_GPIOx, ENABLE);
  RCC_AHBPeriphClockCmd(sck_portpin->RCC_AHB1Periph_GPIOx,  ENABLE);
  RCC_AHBPeriphClockCmd(nss_portpin->RCC_AHB1Periph_GPIOx,  ENABLE);

  while((mosi_portpin->GPIO_Pin_x >> MOSI_PinSource) > 0x01) MOSI_PinSource++;
  while((miso_portpin->GPIO_Pin_x >> MISO_PinSource) > 0x01) MISO_PinSource++;
  while((sck_portpin->GPIO_Pin_x  >> SCK_PinSource)  > 0x01) SCK_PinSource++;
  while((nss_portpin->GPIO_Pin_x  >> NSS_PinSource)  > 0x01) NSS_PinSource++;

  GPIO_PinAFConfig(mosi_portpin->GPIOx, MOSI_PinSource, API_SPIx->GPIO_AF_SPIx);
  GPIO_PinAFConfig(miso_portpin->GPIOx, MISO_PinSource, API_SPIx->GPIO_AF_SPIx);
  GPIO_PinAFConfig(sck_portpin->GPIOx,  SCK_PinSource,  API_SPIx->GPIO_AF_SPIx);
  GPIO_PinAFConfig(nss_portpin->GPIOx,  NSS_PinSource,  API_SPIx->GPIO_AF_SPIx);

  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;

  GPIO_InitStructure.GPIO_Pin = mosi_portpin->GPIO_Pin_x;
  GPIO_Init(mosi_portpin->GPIOx, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = miso_portpin->GPIO_Pin_x;
  GPIO_Init(miso_portpin->GPIOx, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = sck_portpin->GPIO_Pin_x;
  GPIO_Init(sck_portpin->GPIOx, &GPIO_InitStructure);
  
  GPIO_InitStructure.GPIO_Pin = nss_portpin->GPIO_Pin_x;
  GPIO_Init(nss_portpin->GPIOx, &GPIO_InitStructure);

  if(mode==SPI_MODE_0)      { SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge; }
  else if(mode==SPI_MODE_1) { SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge; }
  else if(mode==SPI_MODE_2) { SPI_InitStructure.SPI_CPOL = SPI_CPOL_High; SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge; }
  else                      { SPI_InitStructure.SPI_CPOL = SPI_CPOL_High; SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge; }
	
	if(nss_portpin != 0) { SPI_InitStructure.SPI_NSS = SPI_NSS_Hard; }
	else								 { SPI_InitStructure.SPI_NSS = SPI_NSS_Soft; }
	
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  
  SPI_InitStructure.SPI_BaudRatePrescaler = baudrate_prescaler;
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(API_SPIx->SPIx, &SPI_InitStructure);
	
	SPI_SSOutputCmd(API_SPIx->SPIx, ENABLE);
	SPI_NSSPulseModeCmd(API_SPIx->SPIx, ENABLE);

  SPI_Cmd(API_SPIx->SPIx, ENABLE);

  API_SPIx->is_init = 1;
}

/**
  * @brief  init DMA for SPI transmission
  * @param  API_SPIx: x can be (1, 2)
  * @param  dma_mode: circular or normal mode; can be DMA_Mode_Normal or DMA_Mode_Circular
  * @param  p_tx_data: pointer to the tx buffer
  * @param  p_rx_data: pointer to the rx buffer
  * @note   if p_xx_data = 0, the old buffer remains
	* @note   call API_SPI_init before  
  */
void API_SPI_DMA_init(API_SPI_type_t* API_SPIx, uint32_t dma_mode, uint8_t* p_tx_data, uint8_t* p_rx_data)
{
  DMA_InitTypeDef    DMA_InitStructure;
  NVIC_InitTypeDef   NVIC_InitStructure;

  if(p_tx_data != 0) { API_SPIx->p_tx_data = p_tx_data; }
  //else;
  
  if(p_rx_data != 0) { API_SPIx->p_rx_data = p_rx_data; }
  //else;
  
  RCC_AHBPeriphClockCmd(API_SPIx->RCC_AHB1Periph_DMAx, ENABLE);

  DMA_DeInit(API_SPIx->DMA_Channel_x_Tx);
  DMA_DeInit(API_SPIx->DMA_Channel_x_Rx);

  /* Initialize the FIFO threshold */
  SPI_RxFIFOThresholdConfig(API_SPIx->SPIx, SPI_RxFIFOThreshold_QF);

  DMA_StructInit(&DMA_InitStructure);

  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_Mode = dma_mode;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_InitStructure.DMA_BufferSize = (uint16_t)0;
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(API_SPIx->SPIx->DR));
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  
  // Config DMA TX
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)p_tx_data;
  DMA_Init(API_SPIx->DMA_Channel_x_Tx, &DMA_InitStructure);

  // Config DMA RX
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)p_rx_data;
  DMA_Init(API_SPIx->DMA_Channel_x_Rx, &DMA_InitStructure);

  SPI_I2S_DMACmd(API_SPIx->SPIx, SPI_I2S_DMAReq_Tx, ENABLE);
  SPI_I2S_DMACmd(API_SPIx->SPIx, SPI_I2S_DMAReq_Rx, ENABLE);
  
  /* configure NVIC */
  NVIC_InitStructure.NVIC_IRQChannel = API_SPIx->DMA_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
  DMA_ITConfig(API_SPIx->DMA_Channel_x_Tx, DMA_IT_TC, ENABLE);
  DMA_ITConfig(API_SPIx->DMA_Channel_x_Rx, DMA_IT_TC, ENABLE);
}
/**
  * @brief  start continious SPI transmission, triggered a timer
	* @param  API_SPIx: x can be (1, 2)
	* @param  API_TIMx: where x can be 1, 2, 3, 6, 7, 15, 16 or 17 to select the TIM peripheral.
  * @param  t_period_s: Time for one period
	* @param  tim_dma_source: specifies the DMA Request sources.
	*          This parameter can be any combination of the following values :
	*            @arg TIM_DMA_Update : TIM update Interrupt source
	*            @arg TIM_DMA_CC1 : TIM Capture Compare 1 DMA source
	*            @arg TIM_DMA_CC2 : TIM Capture Compare 2 DMA source
	*            @arg TIM_DMA_CC3 : TIM Capture Compare 3 DMA source
	*            @arg TIM_DMA_CC4 : TIM Capture Compare 4 DMA source
	*            @arg TIM_DMA_COM : TIM Commutation DMA source
	*            @arg TIM_DMA_Trigger : TIM Trigger DMA source
  * @param  p_tx_data: pointer to the tx buffer
  * @param  tx_length: byte count to send
	* @note   TIM7 is applicable only for STM32F072 devices
	* @note   TIM6 is not applicable for STM32F031 devices.
	* @note   TIM2 is not applicable for STM32F030 devices.
  */
void API_SPI_DMA_TIM_start_continious_tx(API_SPI_type_t* API_SPIx, API_TIM_type_t* API_TIMx, double t_period_s, uint16_t tim_dma_source, uint8_t* p_tx_data, uint16_t tx_length)
{
  DMA_InitTypeDef    DMA_InitStructure;

  DMA_Cmd(API_SPIx->DMA_Channel_x_Tx, DISABLE);
	DMA_DeInit(API_SPIx->DMA_Channel_x_Tx);

  if(p_tx_data != 0) { API_SPIx->p_tx_data = p_tx_data; }
  //else;

  RCC_AHBPeriphClockCmd(API_SPIx->RCC_AHB1Periph_DMAx, ENABLE);
	
	API_TIM_start(API_TIMx, TIM_IT_Update, t_period_s, 3);
	
	/* TIM Capture Compare DMA Request enable */
  TIM_DMACmd(API_TIMx->TIMx, tim_dma_source, ENABLE);

  DMA_StructInit(&DMA_InitStructure);

  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_InitStructure.DMA_BufferSize = (uint16_t)tx_length;
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(API_SPIx->SPIx->DR));
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;

  // Config DMA TX
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)p_tx_data;
  DMA_Init(API_SPIx->DMA_Channel_x_Tx, &DMA_InitStructure);

  //DMA_SetCurrDataCounter(API_SPIx->DMA_Channel_x_Tx, tx_length);
  DMA_Cmd(API_SPIx->DMA_Channel_x_Tx, ENABLE);
}

/**
  * @brief  stop continious SPI transmission
	* @param  API_SPIx: x can be (1, 2)
  * @param  API_TIMx: can be API_TIM1, API_TIM3, API_TIM14 .. API_TIM17
  */
void API_SPI_DMA_TIM_stop_continious_tx(API_SPI_type_t* API_SPIx, API_TIM_type_t* API_TIMx)
{
  DMA_Cmd(API_SPIx->DMA_Channel_x_Tx, DISABLE);
  API_TIM_stop(API_TIMx, 0);
}

/**
  * @brief  send data in SPI mode using DMA
  * @param  API_SPIx:  x can be (1, 2)
  * @param  tx_length: number of bytes to send/receive
  * @note   API_SPI_init() and API_SPI_DMA_init() must be called before
  * @note   NSS pin must be set in application code
  */
void API_SPI_DMA_send(API_SPI_type_t* API_SPIx, uint16_t tx_length)
{
  while(DMA_GetCurrDataCounter(API_SPIx->DMA_Channel_x_Tx) > 0);
  
  DMA_Cmd(API_SPIx->DMA_Channel_x_Tx, DISABLE);
  DMA_SetCurrDataCounter(API_SPIx->DMA_Channel_x_Tx, tx_length);
  DMA_Cmd(API_SPIx->DMA_Channel_x_Tx, ENABLE);
}

/**
  * @brief  send and receive data simultaneously in SPI mode using DMA
  * @param  API_SPIx: x can be (1, 2)
  * @param  length: number of bytes to send/receive
  * @note   API_SPI_init() and API_SPI_DMA_init() must be called before
  * @note   NSS pin must be set in application code
  */
void API_SPI_DMA_send_and_receive(API_SPI_type_t* API_SPIx, uint16_t length)
{
  while(DMA_GetCurrDataCounter(API_SPIx->DMA_Channel_x_Tx) > 0);
  while(DMA_GetCurrDataCounter(API_SPIx->DMA_Channel_x_Rx) > 0);
  
  DMA_Cmd(API_SPIx->DMA_Channel_x_Tx, DISABLE);
  DMA_Cmd(API_SPIx->DMA_Channel_x_Rx, DISABLE);
  
  DMA_SetCurrDataCounter(API_SPIx->DMA_Channel_x_Tx, length);
  DMA_SetCurrDataCounter(API_SPIx->DMA_Channel_x_Rx, length);
  
  DMA_Cmd(API_SPIx->DMA_Channel_x_Rx, ENABLE);
  DMA_Cmd(API_SPIx->DMA_Channel_x_Tx, ENABLE);
}

/**
  * @brief  send a single byte in SPI mode
  * @param  API_SPIx: x can be (1, 2)
  * @param  data:     byte to send
  * @note   API_SPI_init() must be called before
  * @note   NSS pin must be set in application code
  */
void API_SPI_send_byte(API_SPI_type_t* API_SPIx, uint8_t data)
{
  while (SPI_I2S_GetFlagStatus(API_SPIx->SPIx, SPI_I2S_FLAG_TXE) == RESET);
  SPI_SendData8(API_SPIx->SPIx, data);
}

/**
  * @brief  receive a single byte in SPI mode
  * @param  API_SPIx: x can be (1, 2)
  * @retval received byte
  * @note   API_SPI_init() must be called before
  * @note   don't use this function for a robust, low energy or high performance application
  * @note   NSS pin must be set in application code
  */
uint8_t API_SPI_receive_byte(API_SPI_type_t* API_SPIx)
{
  while(SPI_I2S_GetFlagStatus(API_SPIx->SPIx, SPI_I2S_FLAG_TXE) == RESET);
  while(SPI_I2S_GetFlagStatus(API_SPIx->SPIx, SPI_I2S_FLAG_RXNE) == SET) SPI_I2S_ReceiveData16(API_SPIx->SPIx);
  
  SPI_SendData8(API_SPIx->SPIx, 0x00);
  
  while(SPI_I2S_GetFlagStatus(API_SPIx->SPIx, SPI_I2S_FLAG_RXNE) == RESET);
  
  return SPI_ReceiveData8(API_SPIx->SPIx);
}


/**
  * @brief  send and receive data successively in SPI mode
  * @param  API_SPIx:     x can be (1, 2)
  * @param  t_period_s:   period time in seconds
	* @param  p_tx_data:    Data to send
	* @param  tx_length:    number of bytes to send
	* @param  p_rx_data:    received Data
	* @param  rx_length:    number of bytes to receive
  * @note   API_SPI_init() must be called before
  * @retval error number: EBUSY..sending or receiving data, EIO..unknown error, 0..transmission complete
  */
error_t API_SPI_send_then_receive_statemachine(API_SPI_type_t* API_SPIx, double t_period_s, uint8_t *p_tx_data, uint8_t tx_length, uint8_t *p_rx_data, uint8_t rx_length)
{
  static uint8_t Z                = 0;
  static uint8_t mZ               = 0;
  static uint8_t SEND             = 0x01;
  static uint8_t READ             = 0x02;

  static uint8_t  iniOK           = 0;
  static double   t               = 0;
  static uint8_t  bytes_sent      = 0;
  static uint8_t  bytes_received  = 0;
	
	const FlagStatus bsy_flag = SPI_I2S_GetFlagStatus(API_SPIx->SPIx, SPI_I2S_FLAG_BSY);

  do
  {
    if(!iniOK)       t = 0;
    else if(Z != mZ) t = 0;
    else             t = t + t_period_s;

    mZ = Z;

    if(!iniOK)
    {
      Z               = SEND;
      mZ              = Z;
      bytes_sent      = 0;
      bytes_received  = 0;
      API_setDO(API_SPIx->nss_portpin, Bit_RESET);
    }
    else if(Z == SEND && t > 0 && (bsy_flag == RESET) && bytes_sent < tx_length)
    {
      SPI_SendData8(API_SPIx->SPIx, *(p_tx_data + bytes_sent));
      bytes_sent++;
    }
    else if(Z == SEND && t > 0 && (bsy_flag == RESET) && rx_length <= 0)
    {
      API_setDO(API_SPIx->nss_portpin, Bit_SET);
      iniOK = 0;
      return 0;
    }
    else if(Z == SEND && t > 0 && (bsy_flag == RESET))
    {
      
      while(SPI_I2S_GetFlagStatus(API_SPIx->SPIx, SPI_I2S_FLAG_RXNE) == SET) SPI_I2S_ReceiveData16(API_SPIx->SPIx);
      Z = READ;
    }
    else if(Z == READ && t > 0 && (SPI_I2S_GetFlagStatus(API_SPIx->SPIx, SPI_I2S_FLAG_RXNE) == SET) && bytes_received < rx_length)
    {
      *(p_rx_data + bytes_received) = SPI_ReceiveData8(API_SPIx->SPIx);
      bytes_received++;
    }
    else if(Z == READ && t > 0 && (bsy_flag == RESET) && bytes_received < rx_length)
    {
      SPI_SendData8(API_SPIx->SPIx, 0x00);
    }
    else if(Z == READ && t > 0 && bytes_received >= rx_length)
    {
      API_setDO(API_SPIx->nss_portpin, Bit_SET);
      iniOK = 0;
      return 0;
    }
    else if(t > 0.5)
    {
      API_setDO(API_SPIx->nss_portpin, Bit_SET);
      iniOK = 0;
      return EIO;
    }
  }
  while(Z != mZ);

  iniOK = 1;

  return EBUSY;
}

/**
  * @brief  send and receive data successively in SPI mode using DMA
  * @param  API_SPIx:     x can be (1, 2, 3)
  * @param  t_period_s:   period time in seconds
  * @param  p_tx_data:    Data to send
	* @param  tx_length:    number of bytes to send
  * @param  p_rx_data:    received Data
	* @param  rx_length:    number of bytes to receive
  * @param p_tx_data_while_receiving: pointer to tx data while receiving
  * @note   API_SPI_init() and API_SPI_DMA_init() must be called before
  * @note   if *p_tx_data_while_receiving = 0: tx data are zero bytes
  * @retval error number: EBUSY..sending or receiving data, EIO..unknown error, 0..transmission complete
  */
error_t API_SPI_DMA_send_then_receive_statemachine(API_SPI_type_t* API_SPIx, 
  double t_period_s, 
  uint8_t* p_tx_data, 
  uint8_t  tx_length, 
  uint8_t* p_rx_data, 
  uint8_t  rx_length, 
  uint8_t* p_tx_data_while_receiving)
{
  static uint8_t Z                = 0;
  static uint8_t mZ               = 0;
  static uint8_t SEND             = 0x01;
  static uint8_t WAIT             = 0x02;
  static uint8_t READ             = 0x03;

  static uint8_t  iniOK           = 0;
  static double   t               = 0;
  static uint8_t* p_tx_data_while_sending;
  uint16_t i = 0;
  
  const FlagStatus bsy_flag = SPI_I2S_GetFlagStatus(API_SPIx->SPIx, SPI_I2S_FLAG_BSY);

  do
  {
    if(!iniOK)       t = 0;
    else if(Z != mZ) t = 0;
    else             t = t + t_period_s;

    mZ = Z;

    if(!iniOK)
    {
      Z   = SEND;
      mZ  = Z;
      API_SPI_DMA_init(API_SPIx, DMA_Mode_Normal, p_tx_data, p_rx_data);
      for(i = 0; i < rx_length; i++) API_SPIx->p_rx_data[i] = 0x00;
      API_setDO(API_SPIx->nss_portpin, Bit_RESET);
    }
    else if(Z == SEND && t > 0 && (bsy_flag == RESET))
    {
      p_tx_data_while_sending = API_SPIx->p_tx_data;
      API_SPI_DMA_send(API_SPIx, tx_length);
      Z = WAIT;
    }
    else if(Z == SEND && t > 0 && (bsy_flag == RESET) && rx_length <= 0)
    {
      API_setDO(API_SPIx->nss_portpin, Bit_SET);
      iniOK = 0;
      return 0;
    }
    else if(Z == WAIT && t > 0 && (bsy_flag == RESET) && p_tx_data_while_receiving != 0)
    {
      API_SPI_DMA_init(API_SPIx, DMA_Mode_Normal, p_tx_data_while_receiving, API_SPIx->p_rx_data);
      API_SPI_DMA_send_and_receive(API_SPIx, rx_length);
      Z = READ;
    }
    else if(Z == WAIT && t > 0 && (bsy_flag == RESET))
    {
      API_SPI_DMA_init(API_SPIx, DMA_Mode_Normal, API_SPIx->p_rx_data, API_SPIx->p_rx_data);
      API_SPI_DMA_send_and_receive(API_SPIx, rx_length);
      Z = READ;
    }
    else if(Z == READ && t > 0 && DMA_GetCurrDataCounter(API_SPIx->DMA_Channel_x_Rx) == 0)
    {
      API_SPI_DMA_init(API_SPIx, DMA_Mode_Normal, p_tx_data_while_sending, API_SPIx->p_rx_data);
      API_setDO(API_SPIx->nss_portpin, Bit_SET);
      iniOK = 0;
      return 0;           
    }
    else if(t > 0.5)
    {
      API_SPI_DMA_init(API_SPIx, DMA_Mode_Normal, p_tx_data_while_sending, API_SPIx->p_rx_data);
      API_setDO(API_SPIx->nss_portpin, Bit_SET);
      iniOK = 0;
      return EIO;
    }
  }
  while(Z != mZ);

  iniOK = 1;

  return EBUSY;
}

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 

#endif // __USING_SPI
