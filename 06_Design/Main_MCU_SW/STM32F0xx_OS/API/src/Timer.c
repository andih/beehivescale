/**
 * @file    ./API/src/Timer.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer
 * @brief   API for Timer
 */
  
#include "../inc/PerUsings.h"
#ifdef __USING_TIMER

/* Includes ------------------------------------------------------------------*/
#include "Timer.h"

/** @addtogroup STM32F0xx_API
  * @{
  */

/** @defgroup Timer 
  * @brief Timer API functions
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
#ifdef STM32F030
API_TIM_type_t API_TIM1 =  { TIM1,  RCC_APB2Periph_TIM1, GPIO_AF_2, TIM1_CC_IRQn, 0, DMA1_Channel2 };
API_TIM_type_t API_TIM3 =  { TIM3,  RCC_APB1Periph_TIM3, GPIO_AF_1, TIM3_IRQn, 0,    DMA1_Channel3 };
API_TIM_type_t API_TIM14 = { TIM14, RCC_APB1Periph_TIM14, 0, TIM14_IRQn, 0,  0 };
API_TIM_type_t API_TIM15 = { TIM15, RCC_APB2Periph_TIM15, GPIO_AF_1, TIM15_IRQn, 0,  DMA1_Channel5 };
API_TIM_type_t API_TIM16 = { TIM16, RCC_APB2Periph_TIM16, GPIO_AF_2, TIM16_IRQn, 0,  DMA1_Channel3 };
API_TIM_type_t API_TIM17 = { TIM17, RCC_APB2Periph_TIM17, GPIO_AF_2, TIM17_IRQn, 0,  DMA1_Channel1 };
#endif // STM32F030

/* Private function prototypes -----------------------------------------------*/
void API_TIM_start(API_TIM_type_t* API_TIMx, uint16_t tim_it, double t_period_s, uint8_t priority);
void API_PWM_start(API_TIM_type_t* API_TIMx, uint32_t CHx, API_GPIO_type_t* portpin, double t_period_s, double duty_cycle, uint8_t deadtime, uint16_t TIM_OCPolarity);
void API_PWM_set_dutycycle(API_TIM_type_t* API_TIMx, uint8_t CHx, double dutycycle);
static void timer_clock_enable(API_TIM_type_t* API_TIMx);
static uint32_t timer_get_clock_frequency(API_TIM_type_t* API_TIMx);

/* Private functions ---------------------------------------------------------*/
/** @defgroup Timer_Private_Functions
  * @{
  */

/**
  * @brief  Start a new Timer
  * @param  API_TIMx: can be API_TIM1 .. API_TIM17
  * @param  tim_it: specifies the TIM interrupts sources.
  *          This parameter can be any combination of the following values:
  *            @arg 0: disable interrupt
  *            @arg TIM_IT_Update: TIM update Interrupt source
  *            @arg TIM_IT_CC1: TIM Capture Compare 1 Interrupt source
  *            @arg TIM_IT_CC2: TIM Capture Compare 2 Interrupt source
  *            @arg TIM_IT_CC3: TIM Capture Compare 3 Interrupt source
  *            @arg TIM_IT_CC4: TIM Capture Compare 4 Interrupt source
  *            @arg TIM_IT_COM: TIM Commutation Interrupt source
  *            @arg TIM_IT_Trigger: TIM Trigger Interrupt source
  *            @arg TIM_IT_Break: TIM Break Interrupt source
  * @param  t_period_s: Time for one period
  * @param  priority: Interrupt priority (0..15)
  */
void API_TIM_start(API_TIM_type_t* API_TIMx, uint16_t tim_it, double t_period_s, uint8_t priority)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  NVIC_InitTypeDef NVIC_InitStructure;
	
  uint32_t period_timer_ticks = 0;
  uint32_t prescaler = 0;
  uint32_t timer_clock = 0;
	
	/* TIMx counter disable */
  TIM_Cmd(API_TIMx->TIMx, DISABLE);

  /* TIMx clock enable */
  timer_clock_enable(API_TIMx);

  timer_clock = timer_get_clock_frequency(API_TIMx);
  
  /* Prescaler berechnen */
  prescaler = (uint16_t)((timer_clock) * t_period_s / 0x10000);

  period_timer_ticks = (timer_clock * t_period_s / ((prescaler + 1))); 

  /* Time Base configuration */
  TIM_TimeBaseStructure.TIM_Prescaler = prescaler;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = period_timer_ticks;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;

  TIM_TimeBaseInit(API_TIMx->TIMx, &TIM_TimeBaseStructure);

  /* TIMx counter enable */
  TIM_Cmd(API_TIMx->TIMx, ENABLE);

  if(tim_it != 0)
  {
    /* Interrupt konfigurieren */
    NVIC_InitStructure.NVIC_IRQChannel = API_TIMx->IRQnx;                // Interrupt Source ausw�hlen
    NVIC_InitStructure.NVIC_IRQChannelPriority = priority; // Priorit�t ausw�hlen
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;                  // enablen
    NVIC_Init(&NVIC_InitStructure);                                  // Parameter �bernehmen

    TIM_ITConfig(API_TIMx->TIMx, tim_it, ENABLE);
  }
  // else;
}

/**
* @brief  Stop a running Timer
* @param  API_TIMx: can be API_TIM1 .. API_TIM17
* @param  tim_it: specifies the TIM interrupts sources.
*          This parameter can be any combination of the following values:
*            @arg 0: disable interrupt
*            @arg TIM_IT_Update: TIM update Interrupt source
*            @arg TIM_IT_CC1: TIM Capture Compare 1 Interrupt source
*            @arg TIM_IT_CC2: TIM Capture Compare 2 Interrupt source
*            @arg TIM_IT_CC3: TIM Capture Compare 3 Interrupt source
*            @arg TIM_IT_CC4: TIM Capture Compare 4 Interrupt source
*            @arg TIM_IT_COM: TIM Commutation Interrupt source
*            @arg TIM_IT_Trigger: TIM Trigger Interrupt source
*            @arg TIM_IT_Break: TIM Break Interrupt source
*/
void API_TIM_stop(API_TIM_type_t* API_TIMx, uint16_t tim_it)
{
  if(tim_it != 0)
  {
    TIM_ITConfig(API_TIMx->TIMx, tim_it, DISABLE);
  }
  TIM_Cmd(API_TIMx->TIMx, DISABLE);
}

/**
  * @brief  Start a new PWM Signal on a GPIO
  * @param  API_TIMx: can be API_TIM1 .. API_TIM17
  * @param  CHx: Timer-channel (can be CH1, CH2, CH3 or CH4)
  * @param  portpin: Output Pin for the PWM, can be PA0..PA15, ..., PI0..PI15 (depends on Timer and Timer-channel)
  * @param  t_period_s: Time for one period
  * @param  dutycycle: 0..1
  * @param  deadtime: see Reference Manual RM0360 page 447 for time calculation
  * @param  TIM_OCPolarity: can be TIM_OCPolarity_High or TIM_OCPolarity_Low
  */
void API_PWM_start(API_TIM_type_t* API_TIMx, uint32_t CHx, API_GPIO_type_t* portpin, double t_period_s, double duty_cycle, uint8_t deadtime, uint16_t TIM_OCPolarity)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_BDTRInitTypeDef TIM_BDTRInitStructure;
  TIM_OCInitTypeDef TIM_OCInitStructure;
  GPIO_InitTypeDef  GPIO_InitStructure;

  uint32_t pulse_timer_ticks  = 0;
  uint32_t period_timer_ticks = 0;
  uint16_t prescaler          = 0;
  uint16_t pin_source         = 0;
  uint32_t timer_clock        = 0;
	
	/* TIMx counter disable */
  TIM_Cmd(API_TIMx->TIMx, DISABLE);

  while((portpin->GPIO_Pin_x >> pin_source) > 0x01) pin_source++;

  /* TIMx clock enable */
  timer_clock_enable(API_TIMx);
  
  timer_clock = timer_get_clock_frequency(API_TIMx);

  /* Prescaler berechnen */
  prescaler = (uint16_t)((timer_clock) * t_period_s / 0x10000);

  period_timer_ticks = (timer_clock * t_period_s / ((prescaler + 1))); 
  pulse_timer_ticks  = period_timer_ticks * duty_cycle;

  /* API_GPIOx Periph clock enable */
  RCC_AHBPeriphClockCmd(portpin->RCC_AHB1Periph_GPIOx, ENABLE);

  /* Set AF Pin */
  GPIO_PinAFConfig(portpin->GPIOx, pin_source, API_TIMx->GPIO_AF_TIMx);

  GPIO_InitStructure.GPIO_Pin = portpin->GPIO_Pin_x;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(portpin->GPIOx, &GPIO_InitStructure);
  
  /* Time Base configuration */
  TIM_TimeBaseStructure.TIM_Prescaler = prescaler;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = period_timer_ticks;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;

  TIM_TimeBaseInit(API_TIMx->TIMx, &TIM_TimeBaseStructure);
  
  API_TIMx->period_timer_ticks = period_timer_ticks;

  /* PWM Mode configuration */
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
  TIM_OCInitStructure.TIM_Pulse = pulse_timer_ticks;
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
  TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Reset;
	
	if(TIM_OCPolarity == TIM_OCPolarity_Low) { TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;  TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High; }
	else 																		 { TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;  }
	
  if(CHx == 0x01)      { TIM_OC1Init(API_TIMx->TIMx, &TIM_OCInitStructure); TIM_OC1PreloadConfig(API_TIMx->TIMx, TIM_OCPreload_Enable); }
  else if(CHx == 0x02) { TIM_OC2Init(API_TIMx->TIMx, &TIM_OCInitStructure); TIM_OC2PreloadConfig(API_TIMx->TIMx, TIM_OCPreload_Enable); }
  else if(CHx == 0x04) { TIM_OC3Init(API_TIMx->TIMx, &TIM_OCInitStructure); TIM_OC3PreloadConfig(API_TIMx->TIMx, TIM_OCPreload_Enable); }
  else if(CHx == 0x08) { TIM_OC4Init(API_TIMx->TIMx, &TIM_OCInitStructure); TIM_OC4PreloadConfig(API_TIMx->TIMx, TIM_OCPreload_Enable); }
  //else;
	
	/* dead time configuration*/
	TIM_BDTRStructInit(&TIM_BDTRInitStructure);
  TIM_BDTRInitStructure.TIM_DeadTime = deadtime;
  TIM_BDTRConfig(API_TIMx->TIMx, &TIM_BDTRInitStructure);
	
	/* TIMx counter enable */
  TIM_Cmd(API_TIMx->TIMx, ENABLE);
	
	/* TIMx Main Output Enable */
  TIM_CtrlPWMOutputs(API_TIMx->TIMx, ENABLE);
}

/**
  * @brief  update value for dutycycle
  * @param  API_TIMx: can be API_TIM1 .. API_TIM17
  * @param  CHx: Timer-channel (can be CH1, CH2, CH3 or CH4)
  * @param  dutycycle: 0..1
  */
void API_PWM_set_dutycycle(API_TIM_type_t* API_TIMx, uint8_t CHx, double dutycycle)
{
  uint16_t TimerPeriod   = API_TIMx->period_timer_ticks;
  uint16_t ChannelPulse  = (uint16_t)(TimerPeriod * dutycycle);                        

  /*** Timer Configuration ***/

  /* Channel x Configuration in PWM mode */
  if(CHx == CH1) TIM_SetCompare1(API_TIMx->TIMx, ChannelPulse);
  else if(CHx == CH2) TIM_SetCompare2(API_TIMx->TIMx, ChannelPulse);
  else if(CHx == CH3) TIM_SetCompare3(API_TIMx->TIMx, ChannelPulse);
  else if(CHx == CH4) TIM_SetCompare4(API_TIMx->TIMx, ChannelPulse);
}

/**
  * @brief  enable clock of TIMx
  * @param  API_TIMx: can be API_TIM1 .. API_TIM17
  */
void timer_clock_enable(API_TIM_type_t* API_TIMx)
{
  if(API_TIMx->TIMx == API_TIM3.TIMx)       RCC_APB1PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else if(API_TIMx->TIMx == API_TIM14.TIMx) RCC_APB1PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else                                      RCC_APB2PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
}

/**
  * @brief  read clock frequencie of TIMx
  * @param  API_TIMx: can be API_TIM1 .. API_TIM17
  * @retval clock frequency
  */
uint32_t timer_get_clock_frequency(API_TIM_type_t* API_TIMx)
{
  RCC_ClocksTypeDef RCC_Clocks;
  
  RCC_GetClocksFreq(&RCC_Clocks);
 
#ifdef STM32F030
  if(API_TIMx->TIMx == API_TIM1.TIMx)       return RCC_Clocks.PCLK_Frequency;
  else if(API_TIMx->TIMx == API_TIM3.TIMx)  return RCC_Clocks.PCLK_Frequency;
  else if(API_TIMx->TIMx == API_TIM14.TIMx) return RCC_Clocks.PCLK_Frequency;  
  else if(API_TIMx->TIMx == API_TIM15.TIMx) return RCC_Clocks.PCLK_Frequency;
  else if(API_TIMx->TIMx == API_TIM16.TIMx) return RCC_Clocks.PCLK_Frequency;
  else if(API_TIMx->TIMx == API_TIM17.TIMx) return RCC_Clocks.PCLK_Frequency;
  else                                      return 0;
#endif // STM32F030
}

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 

#endif // __USING_TIMER
