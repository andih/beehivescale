/**
 * @file    ./API/src/DAC.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer
 * @brief   API for DAC
 */
  
#include "PerUsings.h"
#ifdef __USING_DAC

/* Includes ------------------------------------------------------------------*/
#include "DAC.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
API_DAC_type_t API_DAC1 = { DAC_Channel_1, &PA4, &API_TIM6, DMA_Channel_7, DMA1_Stream5, DAC_DHR12R1_ADDRESS, DAC_Trigger_T6_TRGO};
API_DAC_type_t API_DAC2 = { DAC_Channel_2, &PA5, &API_TIM7, DMA_Channel_7, DMA1_Stream6, DAC_DHR12R2_ADDRESS, DAC_Trigger_T7_TRGO};

double g_dac_ref_v;

/* Private function prototypes -----------------------------------------------*/
void API_DAC_set_ua_v(API_DAC_type_t* API_DACx, double u_v);
void API_DAC_DMA_set_ua_v(API_DAC_type_t* API_DACx, uint32_t* U_digit, double t_period_s, uint32_t length);

/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Set a analog output voltage on a portpin.
  * @param  API_DACx: where x can be (1,2) to select the DAC (PA4,PA5)
  * @param  U: outputvoltage, can be 0..Uref_V
  * @retval none
  */
void API_DAC_set_ua_v(API_DAC_type_t* API_DACx, double u_v){
  DAC_InitTypeDef   DAC_InitStructure;
  GPIO_InitTypeDef  GPIO_InitStructure;
  
  /* DAC Periph clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
  
  /* DAC channel configuration */
  GPIO_InitStructure.GPIO_Pin = API_DACx->portpin->GPIO_Pin_x;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(API_DACx->portpin->GPIOx, &GPIO_InitStructure);
    
  /* DAC channel Configuration */
  DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;
  DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
  DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
  DAC_Init(API_DACx->DAC_Channel_x, &DAC_InitStructure);

  /* Enable DAC channel */
  DAC_Cmd(API_DACx->DAC_Channel_x, ENABLE);
    
  /* Set DAC channel DHR12RD register */
  if (API_DACx->DAC_Channel_x == API_DAC1.DAC_Channel_x)     DAC_SetChannel1Data(DAC_Align_12b_R, (4095.0 / g_dac_ref_v) * u_v);
  else if(API_DACx->DAC_Channel_x == API_DAC2.DAC_Channel_x) DAC_SetChannel2Data(DAC_Align_12b_R, (4095.0 / g_dac_ref_v) * u_v);
  //else;			
}

/**
  * @brief  Set a analog output voltage on a portpin.
  * @note   API_TIM6 and DAM1_Stream5, DMA_Channel_7 is used to trigger API_DAC1
  * @note   API_TIM7 and DAM1_Stream6, DMA_Channel_7 is used to trigger API_DAC2
  * @param  API_DACx: where x can be (1,2) to select the DAC (PA4,PA5)
  * @param  u_digit: pointer to the output voltage in digits
  * @param  t_period_s: Time of one period [s].
  * @param  length: length of the variable U (points in one period).
  * @retval none
  */
void API_DAC_DMA_set_ua_v(API_DAC_type_t* API_DACx, uint32_t *u_digit, double t_period_s, uint32_t length){
  DAC_InitTypeDef   DAC_InitStructure;
  GPIO_InitTypeDef  GPIO_InitStructure;
  DMA_InitTypeDef   DMA_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	
	uint32_t TimerPeriod = 0; 
  uint32_t i = 0;
  uint32_t Prescaler = 0;
  
  /* DAC Periph clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
  
  /* DMA Periph clock enable */
  RCC_APB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
  
  /* GPIO Periph clock enable */
  RCC_AHB1PeriphClockCmd(API_DACx->portpin->RCC_AHB1Periph_GPIOx, ENABLE);
  
  /* TIMx Periph clock enable */
  if(API_DACx->API_TIMx->TIMx == API_TIM1.TIMx)       RCC_APB2PeriphClockCmd(API_DACx->API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else if(API_DACx->API_TIMx->TIMx == API_TIM8.TIMx)  RCC_APB2PeriphClockCmd(API_DACx->API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else if(API_DACx->API_TIMx->TIMx == API_TIM9.TIMx)  RCC_APB2PeriphClockCmd(API_DACx->API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else if(API_DACx->API_TIMx->TIMx == API_TIM1.TIMx)  RCC_APB2PeriphClockCmd(API_DACx->API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else if(API_DACx->API_TIMx->TIMx == API_TIM11.TIMx) RCC_APB2PeriphClockCmd(API_DACx->API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else                                                RCC_APB1PeriphClockCmd(API_DACx->API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  
  /* DAC channel configuration */
  GPIO_InitStructure.GPIO_Pin = API_DACx->portpin->GPIO_Pin_x;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(API_DACx->portpin->GPIOx, &GPIO_InitStructure);

  /* Prescaler berechnen */
  if(API_DACx->API_TIMx->TIMx == API_TIM2.TIMx){
    for(i = 0; ((SystemCoreClock * (t_period_s / length) / ((Prescaler + 1.0) * 2.0)) - 1.0) > 0xFFFFFFFF; i++) Prescaler++;
  }
  else if(API_DACx->API_TIMx->TIMx == API_TIM5.TIMx){
    for(i = 0; ((SystemCoreClock * (t_period_s / length) / ((Prescaler + 1.0) * 2.0)) - 1.0) > 0xFFFFFFFF; i++) Prescaler++;
  }
  else{
    for(i = 0; ((SystemCoreClock * (t_period_s / length) / ((Prescaler + 1.0) * 2.0)) - 1.0) > 0xFFFF; i++) Prescaler++;
  }

  TimerPeriod = (SystemCoreClock * (t_period_s / length) / ((Prescaler + 1.0) * 2.0)) - 1.0;
   
  /* Time Base configuration */
  TIM_TimeBaseStructure.TIM_Prescaler = Prescaler;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = TimerPeriod;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;

  TIM_TimeBaseInit(API_DACx->API_TIMx->TIMx, &TIM_TimeBaseStructure);

  TIM_SelectOutputTrigger(API_DACx->API_TIMx->TIMx, TIM_TRGOSource_Update);
    
  /* DAC channel Configuration */
  DAC_InitStructure.DAC_Trigger = API_DACx->TriggerSource;
  DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
  DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
  DAC_Init(API_DACx->DAC_Channel_x, &DAC_InitStructure);

  /* DMA1_Stream channel configuration */
  DMA_DeInit(API_DACx->DMA_Stream_x);
  DMA_InitStructure.DMA_Channel = API_DACx->DMA_Channel_x;  
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)API_DACx->DAC_Address;
  DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)u_digit;
  DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
  DMA_InitStructure.DMA_BufferSize = length;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;         
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_Init(API_DACx->DMA_Stream_x, &DMA_InitStructure);
  
  /* TIMx counter enable */
  TIM_Cmd(API_DACx->API_TIMx->TIMx, ENABLE);

  /* Enable DMA1_Stream */
  DMA_Cmd(API_DACx->DMA_Stream_x, ENABLE);

  /* Enable DAC channel */
  DAC_Cmd(API_DACx->DAC_Channel_x, ENABLE);

  /* Enable DMA for DAC channel */
  DAC_DMACmd(API_DACx->DAC_Channel_x, ENABLE);
}

#endif // __USING_DAC
