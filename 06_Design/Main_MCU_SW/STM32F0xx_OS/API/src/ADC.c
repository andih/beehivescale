/**
 * @file    ./API/src/ADC.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer
 * @brief   API for ADC
 */

#include "../inc/PerUsings.h"
#ifdef __USING_ADC

/* Includes ------------------------------------------------------------------*/
#include "ADC.h"

/** @addtogroup STM32F0xx_API
  * @{
  */

/** @defgroup ADC 
  * @brief ADC API functions
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
API_ADC_type_t API_ADC1 = { ADC1, RCC_APB2Periph_ADC1, 0 };

double g_adc_ref_v = 2.5;

/* Private function prototypes -----------------------------------------------*/
void API_ADC_init(API_ADC_type_t* API_ADCx);
void API_ADC_DMA_init(API_ADC_type_t* API_ADCx, uint16_t *p_adc_data, uint32_t sample_time, uint8_t channel_count, ...);
double API_ADC_get_ue_v(API_ADC_type_t* API_ADCx, API_GPIO_type_t* portpin, uint32_t sample_time);
double API_ADC_get_mcu_temp_gradc(void);
void API_ADC_DMA_start_single_conv(API_ADC_type_t* API_ADCx);
static uint32_t API_ADC_portpin_to_adc_channel(API_GPIO_type_t* portpin);

/* Private functions ---------------------------------------------------------*/
/** @defgroup ADC_Private_Functions
  * @{
  */

/**
  * @brief  Init ADC
  * @param  API_ADCx: where x can be (1) to select the ADC
  * @retval none
*/
void API_ADC_init(API_ADC_type_t* API_ADCx)
{
  ADC_InitTypeDef       ADC_InitStructure;

  /* Clock enable */
  RCC_APB2PeriphClockCmd(API_ADCx->RCC_APB2Periph_ADCx, ENABLE);

  /* ADC Init ****************************************************************/
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
  ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
  ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
  ADC_InitStructure.ADC_ExternalTrigConv = 0;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_ScanDirection = ADC_ScanDirection_Upward;
  ADC_Init(API_ADCx->ADCx, &ADC_InitStructure);

  ADC_GetCalibrationFactor(API_ADCx->ADCx);

  /* Enable ADC */
  ADC_Cmd(API_ADCx->ADCx, ENABLE);
}

/**
  * @brief  ADC init to convert multiple channels using DMA
  * @param  API_ADCx: where x can be (1) to select the ADC
  * @param  p_adc_data: Pointer to the ADC data array
  * @param  sample_time: used Sample Time (ADC_SampleTime_x_xCycles)
  * @param  channel_count: Count of the used ADC channels (Portpins)
  * @param  pp: pointers from type API_GPIO_type_t to all used Portpins
  * @note   if p_adc_data = 0, the old buffer remains
  * @retval none
*/
void API_ADC_DMA_init(API_ADC_type_t* API_ADCx, uint16_t *p_adc_data, uint32_t sample_time, uint8_t channel_count, ...)
{
  DMA_InitTypeDef   DMA_InitStructure;
  GPIO_InitTypeDef  GPIO_InitStructure;
  uint8_t           i = 0;
  uint32_t          adc_channel;
	va_list arguments;

  if(p_adc_data != 0) { API_ADCx->p_adc_data = p_adc_data; }
  //else;
	
	API_ADC_init(&API_ADC1);

  /* Channel Init */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

  API_ADCx->ADCx->CHSELR = 0; // clear selected channels
	
	va_start(arguments, channel_count);

  for(i = 0; i < channel_count; i++)
  {
		API_GPIO_type_t *portpin = va_arg(arguments, API_GPIO_type_t*);
    RCC_AHBPeriphClockCmd(portpin->RCC_AHB1Periph_GPIOx, ENABLE);

    GPIO_InitStructure.GPIO_Pin = portpin->GPIO_Pin_x;
    GPIO_Init(portpin->GPIOx, &GPIO_InitStructure);

    /* ADC channel configuration */
    adc_channel = API_ADC_portpin_to_adc_channel(portpin);
    ADC_ChannelConfig(API_ADCx->ADCx, adc_channel, sample_time);
  }
	
	va_end(arguments);

  /* Enable ADC_DMA */
  ADC_DMACmd(ADC1, ENABLE);

  /* DMA1 clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

  /* DMA1 Channel1 Config */
  DMA_DeInit(DMA1_Channel1);
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(API_ADCx->ADCx->DR));
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)(API_ADCx->p_adc_data);
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
  DMA_InitStructure.DMA_BufferSize = channel_count;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(DMA1_Channel1, &DMA_InitStructure);
  /* DMA1 Channel1 enable */
  DMA_Cmd(DMA1_Channel1, ENABLE);
}


/**
  * @brief  Get the analog input voltage on a Portpin.
  * @param  API_ADCx: where x can be (1) to select the ADC
  * @param  portpin: used Portpin for ADC conversation
  * @param  sample_time: used Sample Time (ADC_SampleTime_x_xCycles)
  * @retval measured input Voltage (double)
  */
double API_ADC_get_ue_v(API_ADC_type_t* API_ADCx, API_GPIO_type_t* portpin, uint32_t sample_time)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  __IO uint16_t    converted_value = 0;
  uint32_t         adc_channel;

  /* clock enable */
  RCC_AHBPeriphClockCmd(portpin->RCC_AHB1Periph_GPIOx, ENABLE);

  /* GPIO Init */
  GPIO_InitStructure.GPIO_Pin = portpin->GPIO_Pin_x;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(portpin->GPIOx, &GPIO_InitStructure);

  /* ADC channel configuration */
  API_ADCx->ADCx->CHSELR = 0; // clear selected channels
  adc_channel = API_ADC_portpin_to_adc_channel(portpin);
  ADC_ChannelConfig(API_ADCx->ADCx, adc_channel, sample_time);

  while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_ADRDY));

  /* Start Conversion */
  ADC_StartOfConversion(API_ADCx->ADCx);
  while(ADC_GetFlagStatus(API_ADCx->ADCx, ADC_FLAG_EOC) == RESET);
  converted_value = ADC_GetConversionValue(API_ADCx->ADCx);

  return (g_adc_ref_v *  converted_value / 4096.0);
}

/**
  * @brief  Get analog input voltages on Portpins using DMA.
  * @param  API_ADCx: where x can be (1) to select the ADC
  * @retval measured input Voltage (double)
*/
void API_ADC_DMA_start_single_conv(API_ADC_type_t* API_ADCx)
{
  while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_ADRDY));

  /* Start Conversion */
  ADC_StartOfConversion(API_ADCx->ADCx);
}

/**
  * @brief  Get processor temperature
  * @retval temperature in °C
  */
double API_ADC_get_mcu_temp_gradc()
{
  __IO uint16_t converted_value = 0;
  double MCUtemp = 0.0;

  // Enable internal temperature sensor
  ADC_TempSensorCmd(ENABLE);

  API_ADC_init(&API_ADC1);

  // ADC1 Configuration, ADC_Channel_TempSensor is actual channel 16
  ADC_ChannelConfig(ADC1, ADC_Channel_TempSensor, ADC_SampleTime_55_5Cycles);

  while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_ADRDY));

  /* Start Conversion */
  ADC_StartOfConversion(ADC1);
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
  converted_value = ADC_GetConversionValue(ADC1);

  MCUtemp = g_adc_ref_v * converted_value / 4096.0;
  MCUtemp -= 0.760;
  MCUtemp /= 0.0025;
  MCUtemp += 25.0;

  return MCUtemp;
}

/**
  * @brief  get ADC channel from given portpin
  * @param  portpin: can be PA0..PA15, ..., PI0..PI15
  * @retval ADC_Channel_x
  */
static uint32_t API_ADC_portpin_to_adc_channel(API_GPIO_type_t* portpin)
{
  /* ADC channel configuration *************************************/
  if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_0) return ADC_Channel_0;
  else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_1) return ADC_Channel_1;
  else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_2) return ADC_Channel_2;
  else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_3) return ADC_Channel_3;
  else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_4) return ADC_Channel_4;
  else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_5) return ADC_Channel_5;
  else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_6) return ADC_Channel_6;
  else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_7) return ADC_Channel_7;
  else if(portpin->GPIOx == GPIOB && portpin->GPIO_Pin_x == GPIO_Pin_0) return ADC_Channel_8;
  else if(portpin->GPIOx == GPIOB && portpin->GPIO_Pin_x == GPIO_Pin_1) return ADC_Channel_9;
  else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_0) return ADC_Channel_10;
  else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_1) return ADC_Channel_11;
  else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_2) return ADC_Channel_12;
  else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_3) return ADC_Channel_13;
  else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_4) return ADC_Channel_14;
  else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_5) return ADC_Channel_15;
  else
#ifdef USE_EXCEPTION
    Exception("ADC.c: portpin ist keinem ADC Channel zugewiesen.")
#endif
    ;

	return 0;
}

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 

#endif // __USING_ADC
