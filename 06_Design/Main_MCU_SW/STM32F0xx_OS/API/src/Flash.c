/**
 * @file    ./API/src/Flash.c
 * @author  Andreas Hirtenlehner
 * @brief   API for Flash
 */
  
#include "PerUsings.h"
#ifdef __USING_FLASH

/* Includes ------------------------------------------------------------------*/
#include "Flash.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
FLASH_Status API_FLASH_write_halfwords(uint32_t startAddr, uint32_t endAddr, uint16_t *pData);
FLASH_Status API_FLASH_read(uint32_t startAddr, uint32_t endAddr, uint32_t *pData);
FLASH_Status API_FLASH_clear(uint32_t startAddr, uint32_t endAddr, uint16_t data);
uint32_t API_FLASH_check(uint32_t startAddr, uint32_t endAddr, uint16_t *pData);
FLASH_Status API_FLASH_erase_sector(uint16_t startSector, uint16_t endSector);
uint32_t API_FLASH_get_sector(uint32_t Address);
uint32_t API_FLASH_get_next_free_address(uint32_t startAddr, uint32_t endAddr);

/* Private functions ---------------------------------------------------------*/
FLASH_Status API_FLASH_write_halfwords(uint32_t startAddr, uint32_t endAddr, uint16_t *pData)
{
  FLASH_Status state = FLASH_BUSY;
  uint32_t addr;
  uint32_t i = 0;

  FLASH_Unlock();

  for(addr = startAddr; addr < endAddr; addr += 2)
  {
    state = FLASH_ProgramHalfWord(addr, pData[i]);
    i++;
    if(state != FLASH_COMPLETE) break;
  }

  FLASH_Lock();

  return state;
}

FLASH_Status API_FLASH_read(uint32_t startAddr, uint32_t endAddr, uint32_t *pData)
{
	uint32_t i;
	
	for(i = 0; i < endAddr - startAddr; i += 4)
	{
		*(pData + i) = *(__IO uint32_t*)startAddr;
	}
	
  return FLASH_COMPLETE;
}

FLASH_Status API_FLASH_clear(uint32_t startAddr, uint32_t endAddr, uint16_t data)
{
  FLASH_Status state = FLASH_BUSY;
  uint32_t addr;
  uint32_t i = 0;

  FLASH_Unlock();

  for(addr = startAddr; addr <= endAddr; addr += 2)
  {
    state = FLASH_ProgramHalfWord(addr, data);
    i++;
    if(state != FLASH_COMPLETE) break;
  }

  FLASH_Lock();

  return state;
}

uint32_t API_FLASH_check(uint32_t startAddr, uint32_t endAddr, uint16_t *pData)
{
  __IO uint32_t MemoryProgramStatus = 0;
  __IO uint32_t data = 0;
  uint32_t addr = startAddr;
  uint32_t i = 0;

  FLASH_Unlock();

  while (addr <= endAddr)
  {
    data = *(__IO uint8_t*)addr;

    if (data != pData[i]) {
        MemoryProgramStatus++;
        FLASH_ProgramHalfWord(addr, 0x00);
    }

    addr += 2;
    i++;
  }

  FLASH_Lock();

  return MemoryProgramStatus;
}

FLASH_Status API_FLASH_erase_pages(uint32_t startPage, uint32_t endPage)
{
  uint8_t i = 0;
  FLASH_Status status;
	uint16_t page_size_bytes = 2048; 

  FLASH_Unlock();
  FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_WRPERR | FLASH_FLAG_PGERR);

  for (i = startPage; i < endPage; i += page_size_bytes)
  {
    status = FLASH_ErasePage(i);
    if (status != FLASH_COMPLETE)    { return status; }
  }

  FLASH_Lock();

  return FLASH_COMPLETE;
}

uint32_t API_FLASH_get_next_free_address(uint32_t startAddr, uint32_t endAddr)
{
  uint32_t addr;

  for(addr = startAddr; addr < endAddr - 4; addr = addr + 4)
  {
    uint32_t data = *(__IO uint32_t*)addr;

    if(data == 0xFFFFFFFF)      return addr;
  }

  return 0;
}

#endif // __USING_FLASH
