/**
 * @file    ./API/src/USART.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer
 * @brief   API for USART
 */
 
#include "PerUsings.h"
#ifdef __USING_USART

/* Includes ------------------------------------------------------------------*/
#include "USART.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
API_USART_type_t API_USART1 = { USART1, RCC_APB2Periph_USART1, GPIO_AF_1, USART1_IRQn, DMA1, DMA1_Channel2, DMA1_Channel3, DMA1_Channel2_3_IRQn, 0, 0 };
API_USART_type_t API_USART2 = { USART2, RCC_APB1Periph_USART2, GPIO_AF_1, USART2_IRQn, DMA1, DMA1_Channel4, DMA1_Channel5, DMA1_Channel4_5_IRQn, 0, 0 };

/* Private function prototypes -----------------------------------------------*/
void API_USART_init(API_USART_type_t* USART, API_GPIO_type_t* Portpin_Tx, API_GPIO_type_t* Portpin_Rx, uint32_t BaudRate, uint8_t priority);
void API_USART_DMA_init(API_USART_type_t* USART, uint8_t* p_tx_data, uint8_t* p_rx_data, uint8_t tx_priority, uint8_t rx_priority);
void API_USART_send_byte(API_USART_type_t* USART, uint8_t SendByte);
void API_USART_send_data(API_USART_type_t* USART, const char *ptr, long len);
void API_USART_send_string(API_USART_type_t* USART, const char* FormatString, ...);
void API_USART_DMA_send(API_USART_type_t* USART, uint16_t tx_length);
void API_USART_DMA_send_buffer(API_USART_type_t* USART, uint32_t* memory_base_address, uint16_t tx_length);
void API_USART_DMA_receive(API_USART_type_t* USART, uint16_t rx_length);
uint8_t API_USART_get_byte_from_buffer(API_USART_type_t* API_USARTx);

/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Send a single Byte using USART_x
  * @param  USART: can be USART_1..USART_6
  * @param  Portpin: Tx pin for USART, can be PA0..PA15, ..., PI0..PI15 (depends on selected USART)
  * @param  BaudRate: Baudrate for the selected USART
  * @param  priority: Preemption Priority for selected USART
  * @note   for STM32F407: USART_4 and USART_5 are UARTs
  * @retval none
  */
void API_USART_init(API_USART_type_t* USART, API_GPIO_type_t* Portpin_Tx, API_GPIO_type_t* Portpin_Rx, uint32_t BaudRate, uint8_t priority)
{
    USART_InitTypeDef  USART_InitStructure;
    GPIO_InitTypeDef   GPIO_InitStructure;
    //NVIC_InitTypeDef   NVIC_InitStructure;

    uint16_t PinSource_Tx = 0;
    uint16_t PinSource_Rx = 0;

    while((Portpin_Tx->GPIO_Pin_x >> PinSource_Tx) > 0x01) PinSource_Tx++;
    while((Portpin_Rx->GPIO_Pin_x >> PinSource_Rx) > 0x01) PinSource_Rx++;

    /* USART_x Periph clock enable */
    if(USART->USARTx == API_USART1.USARTx)  RCC_APB2PeriphClockCmd(USART->RCC_APBxPeriph_USARTx, ENABLE);
    else                                    RCC_APB1PeriphClockCmd(USART->RCC_APBxPeriph_USARTx, ENABLE);

    /* GPIOx Periph clock enable */
    RCC_AHBPeriphClockCmd(Portpin_Tx->RCC_AHB1Periph_GPIOx, ENABLE);
    RCC_AHBPeriphClockCmd(Portpin_Rx->RCC_AHB1Periph_GPIOx, ENABLE);

    /* Set AF Pin */
    GPIO_PinAFConfig(Portpin_Tx->GPIOx, PinSource_Tx, USART->GPIO_AF_USARTx);
    GPIO_PinAFConfig(Portpin_Rx->GPIOx, PinSource_Rx, USART->GPIO_AF_USARTx);

    GPIO_InitStructure.GPIO_Pin = Portpin_Tx->GPIO_Pin_x;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(Portpin_Tx->GPIOx, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = Portpin_Rx->GPIO_Pin_x;
    GPIO_Init(Portpin_Rx->GPIOx, &GPIO_InitStructure);

    /* configure USART */
    USART_InitStructure.USART_BaudRate = BaudRate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_Init(USART->USARTx, &USART_InitStructure);

    /* configure NVIC */
    /*NVIC_InitStructure.NVIC_IRQChannel = USART->IRQnx;
    NVIC_InitStructure.NVIC_IRQChannelPriority = priority;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);*/

    /* Interrupt enable */
    //USART_ITConfig(USART->USARTx, USART_IT_RXNE, ENABLE);
    //USART_ITConfig(USART->USARTx, USART_IT_TC, ENABLE);

    /* USART enable */
    USART_Cmd(USART->USARTx,ENABLE);

    USART->isInit = 1;
}

void API_USART_DMA_init(API_USART_type_t* USART, uint8_t* p_tx_data, uint8_t* p_rx_data, uint8_t tx_priority, uint8_t rx_priority)
{
    NVIC_InitTypeDef   NVIC_InitStructure;
    DMA_InitTypeDef    DMA_InitStructure;

    if(USART->DMAx == DMA1) { RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE); }
    else                    { RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA2, ENABLE); }
		
		/* USART_x Periph clock enable */
    if(USART->USARTx == API_USART1.USARTx)  RCC_APB2PeriphClockCmd(USART->RCC_APBxPeriph_USARTx, ENABLE);
    else                                    RCC_APB1PeriphClockCmd(USART->RCC_APBxPeriph_USARTx, ENABLE);
		
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
		
		uint8_t priority;
		
		if(tx_priority < rx_priority) { priority = tx_priority; }
		else													{ priority = rx_priority; }

    /* configure NVIC */
    NVIC_InitStructure.NVIC_IRQChannel = USART->DMA_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPriority = priority;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* Interrupt enable */
    //USART_ITConfig(USART->USARTx, USART_IT_RXNE, ENABLE);
    //USART_ITConfig(USART->USARTx, USART_IT_TC, ENABLE);
		DMA_StructInit(&DMA_InitStructure);

		DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
		DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
		DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
		DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
		DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
		DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
		DMA_InitStructure.DMA_BufferSize = (uint16_t)0;
		DMA_InitStructure.DMA_Priority = DMA_Priority_High;
		
		// Config DMA TX
		if(p_tx_data != 0)
		{
			DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(USART->USARTx->TDR));
			DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
			DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)p_tx_data;
			DMA_Init(USART->DMA_Channel_x_Tx, &DMA_InitStructure);
			
			if(USART->USARTx == USART1 && USART->DMA_Channel_x_Tx == DMA1_Channel4) { SYSCFG->CFGR1 |= SYSCFG_CFGR1_USART1TX_DMA_RMP; }
		}

		// Config DMA RX
		if(p_rx_data != 0)
		{
			DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(USART->USARTx->RDR));
			DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
			DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)p_rx_data;
			DMA_Init(USART->DMA_Channel_x_Rx, &DMA_InitStructure);
			
			if(USART->USARTx == USART1 && USART->DMA_Channel_x_Rx == DMA1_Channel5) { SYSCFG->CFGR1 |= SYSCFG_CFGR1_USART1RX_DMA_RMP; }
		}

    // enable DMA request
    if(p_rx_data != 0) USART_DMACmd(USART->USARTx, USART_DMAReq_Rx, ENABLE);
    if(p_tx_data != 0) USART_DMACmd(USART->USARTx, USART_DMAReq_Tx, ENABLE);

    // DMA Transfer-Complete Interrupt enable
    if(p_rx_data != 0) DMA_ITConfig(USART->DMA_Channel_x_Rx, DMA_IT_TC, ENABLE);
    if(p_tx_data != 0) DMA_ITConfig(USART->DMA_Channel_x_Tx, DMA_IT_TC, ENABLE);

    // DMA enable
    if(p_rx_data != 0) DMA_Cmd(USART->DMA_Channel_x_Rx,ENABLE);
		if(p_tx_data != 0) DMA_Cmd(USART->DMA_Channel_x_Tx,ENABLE);
}

/**
  * @brief  Send a single Byte using USART_x
  * @param  USART: can be USART_1..USART_6
  * @param  SendByte: Data to send
  * @note   for STM32F407: USART_4 and USART_5 are UARTs
  * @retval none
  */
void API_USART_send_byte(API_USART_type_t* USART, uint8_t SendByte)
{
  USART->busy = 1;
	while(USART_GetFlagStatus(USART->USARTx, USART_FLAG_TC) == RESET){}
  USART_SendData(USART->USARTx, SendByte);
  USART->busy = 0;
}

/**
  * @brief  Send a String using USART_x
  * @param  USART: can be USART_1..USART_6
  * @param  All other parameters like printf()
  * @note   for STM32F407: USART_4 and USART_5 are UARTs
  * @note   max string length: 100 byte
  * @retval none
  */
void API_USART_send_string(API_USART_type_t* USART, const char* FormatString, ...)
{
    va_list  args;
    int32_t  nSize = 0;
    uint8_t  i = 0;
    char     SendString[100];

    /* format string */
    va_start(args, FormatString);
    nSize = vsnprintf(SendString, 100, FormatString, args);
    va_end(args);

    USART->busy = 1;

    /* send string */
    for(i = 0; i < nSize; i++)
    {
      USART_SendData(USART->USARTx, SendString[i]);
      while(USART_GetFlagStatus(USART->USARTx, USART_FLAG_TC) == RESET);
    }

    USART->busy = 0;
}

/**
  * @brief  Send a data using USART_x
  * @param  USART: can be USART_1..USART_6
  * @param  *ptr: Pointer to Data to send
  * @param  len:  Number of bytes to send
  * @note
  * @retval none
  */
void API_USART_send_data(API_USART_type_t* USART, const char *ptr, long len)
{
    long i;

    USART->busy = 1;

    for(i=0; i<len; i++)
    {
        API_USART_send_byte(USART, ptr[i]);
    }

    USART->busy = 0;
}

void API_USART_DMA_send(API_USART_type_t* USART, uint16_t tx_length)
{
    while (DMA_GetCurrDataCounter(USART->DMA_Channel_x_Tx) > 0);

    USART->busy = 1;

    DMA_Cmd(USART->DMA_Channel_x_Tx, DISABLE);
    DMA_SetCurrDataCounter(USART->DMA_Channel_x_Tx, tx_length);
    DMA_Cmd(USART->DMA_Channel_x_Tx, ENABLE);
}

void API_USART_DMA_send_buffer(API_USART_type_t* USART, uint32_t* memory_base_address, uint16_t tx_length)
{
    while (DMA_GetCurrDataCounter(USART->DMA_Channel_x_Tx) > 0);

    USART->busy = 1;

    DMA_Cmd(USART->DMA_Channel_x_Tx, DISABLE);
    DMA_SetCurrDataCounter(USART->DMA_Channel_x_Tx, tx_length);
	  USART->DMA_Channel_x_Tx->CMAR = (uint32_t)memory_base_address;
    DMA_Cmd(USART->DMA_Channel_x_Tx, ENABLE);
}

void API_USART_DMA_receive(API_USART_type_t* USART, uint16_t rx_length)
{
  DMA_Cmd(USART->DMA_Channel_x_Rx, DISABLE);
  DMA_SetCurrDataCounter(USART->DMA_Channel_x_Rx, rx_length);
  DMA_Cmd(USART->DMA_Channel_x_Rx, ENABLE);
}

/**
  * @brief  Get a single Byte from USART_x buffer
  * @param  USART: can be USART_1..USART_6
  * @note   for STM32F407: USART_4 and USART_5 are UARTs
  * @note   call function USARTStartReceiving() before reading data from the buffer
  * @retval ReceivedByte: most recent received byte by the USARTx peripheral
  */
uint8_t API_USART_get_byte_from_buffer(API_USART_type_t* USART)
{
    uint8_t ReceivedByte = 0;

    ReceivedByte = (uint8_t)USART_ReceiveData(USART->USARTx);

    return ReceivedByte;
}

#endif // __USING_USART
