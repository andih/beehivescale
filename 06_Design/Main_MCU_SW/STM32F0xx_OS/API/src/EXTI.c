/**
  ******************************************************************************
  * @file    ./API/src/EXTI.c 
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   API fr External Interrupts
  */
	
	#include "PerUsings.h"
#ifdef __USING_EXTI

/* Includes ------------------------------------------------------------------*/
#include "EXTI.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void API_conf_EXTI_external(API_GPIO_type_t* portpin, uint8_t priority);
void API_conf_EXTI_internal(uint32_t EXTI_Line, uint8_t NVIC_IRQChannel, uint8_t priority);
void API_EXTI_deinit(void);
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Configures external EXTI Line
  * @param  portpin:   select portpin
  * @param  priority:   interrupt priority (0..15)
  * @retval none
  * @note   
  */
void API_conf_EXTI_external(API_GPIO_type_t* portpin, uint8_t priority) {
		GPIO_InitTypeDef  GPIO_InitStructure;
		EXTI_InitTypeDef	EXTI_InitStructure;
		NVIC_InitTypeDef  NVIC_InitStructure;
		uint8_t EXTI_PortSourceGPIO;
		uint16_t EXTI_PinSource;
		uint8_t NVIC_IRQChannel; 
	
	/* Enable Clock for GPIO */
	/* GPIOx Periph clock enable */
	RCC_AHBPeriphClockCmd(portpin->RCC_AHB1Periph_GPIOx, ENABLE);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

//	(#) Configure the I/O in input mode using GPIO_Init().
		
	/* configure Portpin to input with pull up */
		GPIO_InitStructure.GPIO_Pin   = portpin->GPIO_Pin_x;
		GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
		GPIO_Init(portpin->GPIOx, &GPIO_InitStructure);
	
//	(#) Select the input source pin for the EXTI line using
//		 SYSCFG_EXTILineConfig().
	if(portpin->GPIOx == GPIOA)				EXTI_PortSourceGPIO = EXTI_PortSourceGPIOA;
	else if(portpin->GPIOx == GPIOB) 	EXTI_PortSourceGPIO = EXTI_PortSourceGPIOB;
	else if(portpin->GPIOx == GPIOC)	EXTI_PortSourceGPIO = EXTI_PortSourceGPIOC;
	else if(portpin->GPIOx == GPIOD)	EXTI_PortSourceGPIO = EXTI_PortSourceGPIOD;
	else if(portpin->GPIOx == GPIOE)	EXTI_PortSourceGPIO = EXTI_PortSourceGPIOE;
	else if(portpin->GPIOx == GPIOF)	EXTI_PortSourceGPIO = EXTI_PortSourceGPIOF;
	//else;
	
	while((portpin->GPIO_Pin_x >> EXTI_PinSource) > 0x01) EXTI_PinSource++;
	
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIO, EXTI_PinSource);
//	(#) Select the mode(interrupt, event) and configure the trigger 
//		 selection (Rising, falling or both) using EXTI_Init(). For the 
//		 internal interrupt, the trigger selection is not needed 
//		 (the active edge is always the rising one).
	EXTI_InitStructure.EXTI_Line = portpin->GPIO_Pin_x;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt; 
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);
	
	if(EXTI_PinSource < 2)      { NVIC_IRQChannel = EXTI0_1_IRQn;  }
	else if(EXTI_PinSource < 4) { NVIC_IRQChannel = EXTI2_3_IRQn;  }
	else												{ NVIC_IRQChannel = EXTI4_15_IRQn; }

//	(#) Configure NVIC IRQ channel mapped to the EXTI line using NVIC_Init().
	NVIC_InitStructure.NVIC_IRQChannel = NVIC_IRQChannel;
	NVIC_InitStructure.NVIC_IRQChannelPriority = priority;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
//	(#) Optionally, you can generate a software interrupt using the function 
//		 EXTI_GenerateSWInterrupt().
}

/**
  * @brief  Configures internal EXTI Line
  * @param  EXTI_Line: select external interrupt line (EXTI_Line0 ... EXTI_Line35)
	* @param  NVIC_IRQChannel: select ISR accordingly (@ref IRQn_Type)
  * @param  priority:   interrupt priority (0..15)
  * @retval none
  * @note   
  */
void API_conf_EXTI_internal(uint32_t EXTI_Line, uint8_t NVIC_IRQChannel, uint8_t priority) {
		EXTI_InitTypeDef	EXTI_InitStructure;
		NVIC_InitTypeDef  NVIC_InitStructure;
		uint8_t EXTI_PortSourceGPIO;
		uint16_t EXTI_PinSource;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIO, EXTI_PinSource);
//	(#) Select the mode(interrupt, event) and configure the trigger 
//		 selection (Rising, falling or both) using EXTI_Init(). For the 
//		 internal interrupt, the trigger selection is not needed 
//		 (the active edge is always the rising one).
	EXTI_InitStructure.EXTI_Line = EXTI_Line;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt; 
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

//	(#) Configure NVIC IRQ channel mapped to the EXTI line using NVIC_Init().
	NVIC_InitStructure.NVIC_IRQChannel = NVIC_IRQChannel;
	NVIC_InitStructure.NVIC_IRQChannelPriority = priority;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
//	(#) Optionally, you can generate a software interrupt using the function 
//		 EXTI_GenerateSWInterrupt().
}

void API_EXTI_deinit()
{
	EXTI_DeInit();
	
	NVIC_DisableIRQ(EXTI0_1_IRQn);
	NVIC_DisableIRQ(EXTI2_3_IRQn);
	NVIC_DisableIRQ(EXTI4_15_IRQn);
}

#endif // __USING_EXTI
