/**
  ******************************************************************************
  * @file    ./API/inc/Flash.h 
  * @author  Andreas Hirtenlehner
  * @brief   Header for Flash module
  */

#ifndef __FLASH_H
#define __FLASH_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define FLASH_TIMESTRING_START_ADDR   ADDR_FLASH_SECTOR_8
#define FLASH_TIMESTRING_END_ADDR     ADDR_FLASH_SECTOR_11

#define FLASH_LOG_START_ADDR          ADDR_FLASH_SECTOR_11
#define FLASH_LOG_END_ADDR           (ADDR_FLASH_SECTOR_11 + 0x0000FFFF)

/* Base address of the Flash sectors */
#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes   */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes   */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes   */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes   */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes   */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes  */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes  */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes  */
#define ADDR_FLASH_SECTOR_8     ((uint32_t)0x08080000) /* Base @ of Sector 8, 128 Kbytes  */
#define ADDR_FLASH_SECTOR_9     ((uint32_t)0x080A0000) /* Base @ of Sector 9, 128 Kbytes  */
#define ADDR_FLASH_SECTOR_10    ((uint32_t)0x080C0000) /* Base @ of Sector 10, 128 Kbytes */
#define ADDR_FLASH_SECTOR_11    ((uint32_t)0x080E0000) /* Base @ of Sector 11, 128 Kbytes */

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern FLASH_Status API_FLASH_write_halfwords(uint32_t startAddr, uint32_t endAddr, uint16_t *pData);
extern FLASH_Status API_FLASH_read(uint32_t startAddr, uint32_t endAddr, uint32_t *pData);
extern FLASH_Status API_FLASH_clear(uint32_t startAddr, uint32_t endAddr, uint16_t data);
extern uint32_t API_FLASH_check(uint32_t startAddr, uint32_t endAddr, uint16_t *pData);
extern FLASH_Status API_FLASH_erase_sector(uint16_t startSector, uint16_t endSector);
extern uint32_t API_FLASH_get_sector(uint32_t Address);
extern uint32_t API_FLASH_get_next_free_address(uint32_t startAddr, uint32_t endAddr);

#endif // __FLASH_H
