/**
  * @file    ./API/inc/ADC.h 
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   Header for ADC.c module
  */

#ifndef __ADC_H
#define __ADC_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"
#include <stdarg.h>

/** @addtogroup STM32F0xx_API
  * @{
  */

/** @addtogroup ADC
  * @{
  */

/* Exported types ------------------------------------------------------------*/

/** 
  * @brief  ADC Init structure definition
  */
	
typedef struct{
	ADC_TypeDef*  ADCx;
	__IO uint32_t RCC_APB2Periph_ADCx;
  uint16_t*     p_adc_data;
} API_ADC_type_t;

extern API_ADC_type_t API_ADC1;

extern double g_adc_ref_v;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_ADC_init(API_ADC_type_t* API_ADCx);
extern void API_ADC_DMA_init(API_ADC_type_t* API_ADCx, uint16_t *p_adc_data, uint32_t sample_time, uint8_t channel_count, ...);
extern double API_ADC_get_ue_v(API_ADC_type_t* API_ADCx, API_GPIO_type_t* portpin, uint32_t sample_time);
extern double API_ADC_get_mcu_temp_gradc(void);
extern void API_ADC_DMA_start_single_conv(API_ADC_type_t* API_ADCx);

/**
  * @}
  */ 
	
/**
  * @}
  */ 
	
#endif // __USING_ADC
