/**
  ******************************************************************************
  * @file    ./API/inc/PerUsings.h
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   peripherial usage definitions
  */

#ifndef __PERUSINGS_H
#define __PERUSINGS_H

/* Includes ------------------------------------------------------------------*/

/** @addtogroup STM32F0xx_API
  * @{
  */

/** @addtogroup Peripherial_Usings
  * @{
  */
	
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/** @defgroup PerUsings_Exported_Constants
  * @{
  */ 
	
#define __USING_GPIO
#define __USING_TIMER
#define __USING_ADC
//#define __USING_DAC
#define __USING_USART
#define __USING_SPI
//#define __USING_WWDG
//#define __USING_IWDG
#define __USING_RTC
#define __USING_FLASH
#define __USING_EXTI

/**
  * @}
  */ 

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/**
  * @}
  */ 
	
/**
  * @}
  */ 

#endif // __PERUSINGS_H
