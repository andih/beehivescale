/**
  * @file    ./API/inc/USART.h
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   Header for USART module
  */

#ifndef __USART_H
#define __USART_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"
#include "stdarg.h"
#include "stdio.h"

/* Exported types ------------------------------------------------------------*/
typedef struct{
  USART_TypeDef* USARTx;
  __IO uint32_t  RCC_APBxPeriph_USARTx;
  __IO uint8_t   GPIO_AF_USARTx;
  IRQn_Type      IRQnx;
  DMA_TypeDef*   DMAx;
  DMA_Channel_TypeDef* DMA_Channel_x_Tx;
  DMA_Channel_TypeDef* DMA_Channel_x_Rx;
  IRQn_Type      DMA_IRQn;
  uint8_t        isInit;
  uint8_t        busy;
} API_USART_type_t;

/* Exported constants --------------------------------------------------------*/
extern API_USART_type_t API_USART1;
extern API_USART_type_t API_USART2;
extern API_USART_type_t API_USART3;
extern API_USART_type_t API_USART4;
extern API_USART_type_t API_USART5;
extern API_USART_type_t API_USART6;

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_USART_init(API_USART_type_t* USART, API_GPIO_type_t* Portpin_Tx, API_GPIO_type_t* Portpin_Rx, uint32_t BaudRate, uint8_t priority);
extern void API_USART_DMA_init(API_USART_type_t* USART, uint8_t* p_tx_data, uint8_t* p_rx_data, uint8_t tx_priority, uint8_t rx_priority);
extern void API_USART_send_byte(API_USART_type_t* USART, uint8_t SendByte);
extern void API_USART_send_data(API_USART_type_t* USART, const char *ptr, long len);
extern void API_USART_send_string(API_USART_type_t* USART, const char* FormatString, ...);
extern void API_USART_DMA_send(API_USART_type_t* USART, uint16_t tx_length);
extern void API_USART_DMA_send_buffer(API_USART_type_t* USART, uint32_t* memory_base_address, uint16_t tx_length);
extern void API_USART_DMA_receive(API_USART_type_t* USART, uint16_t rx_length);
extern uint8_t API_USART_get_byte_from_buffer(API_USART_type_t* API_USARTx);

#endif // __USART_H
