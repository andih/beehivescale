/**
  * @file    ./SystemCore/src/main.c 
  * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
  * @brief   main module
  */
  
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/** @addtogroup System_Core
  * @{
  */

/** @defgroup main 
  * @brief main module
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern uint8_t soft_rt_taskclass_signal;
extern uint8_t g_enter_stop_mode;

/* Private function prototypes -----------------------------------------------*/
extern void startup_sequence(void);
extern void soft_rt_taskclass(void);

/* Private functions ---------------------------------------------------------*/
/** @defgroup Main_Private_Functions
  * @{
  */
	
/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
  startup_sequence();
	
	while(1)
  {
		#ifndef DEBUG
			if(g_enter_stop_mode)
			{
				__disable_irq();
				PWR_EnterSTOPMode(PWR_Regulator_ON, PWR_STOPEntry_WFI);
				SystemInit();
				g_enter_stop_mode = 0;
				__enable_irq();
			}
		#endif
		
    if(soft_rt_taskclass_signal)
    {
      soft_rt_taskclass();
      soft_rt_taskclass_signal = 0;
    }
		
#ifndef DEBUG
		PWR_EnterSleepMode(PWR_SLEEPEntry_WFI);
#endif
  }
}

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 
