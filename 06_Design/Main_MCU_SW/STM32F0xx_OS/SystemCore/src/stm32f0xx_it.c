/**
  ******************************************************************************
  * @file    stm32f0xx_it.c 
  * @author  MCD Application Team
  * @version V1.5.0
  * @date    05-December-2014
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_it.h"
#include "../../API/inc/PerUsings.h"

/** @addtogroup System_Core
  * @{
  */

/** @defgroup IRQ_Handler 
  * @brief interrupt handler
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t soft_rt_taskclass_signal;

/* Private function prototypes -----------------------------------------------*/
extern void taskclass1(void);
extern void taskclass2(void);
extern void taskclass3(void);
extern void soft_rt_taskclass(void);

/* Private functions ---------------------------------------------------------*/

/** @defgroup IRQ_Handler_Private_Functions
  * @{
  */
	
/** @defgroup Processor_Exceptions_Handlers
  * @{
  */

/******************************************************************************/
/*            Cortex-M0 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
#ifdef __USING_DEBUG
  LIB_DEBUG_hardfault_handler_wrapper();
#endif // __USING_DEBUG

  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
}

/**
  * @}
  */

/******************************************************************************/
/*                 STM32F0xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f0xx.s).                                            */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

#ifdef __USING_TIMER

/** @defgroup Timer_IRQ_Handler
  * @{
  */
	
/**
  * @brief  This function handles TIM1_IRQn
  * @param  None
  * @retval None
  */
void TIM1_BRK_UP_TRG_COM_IRQHandler(void){

  TIM_ClearITPendingBit(TIM1, TIM_IT_Update);
}

/**
  * @brief  This function handles TIM3_IRQn
  * @param  None
  * @retval None
  */
void TIM3_IRQHandler(void){

  TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
}

/**
  * @brief  This function handles TIM14_IRQn
  * @param  None
  * @retval None
  */
void TIM14_IRQHandler(void){
  taskclass1();

  TIM_ClearITPendingBit(TIM14, TIM_IT_Update);
}

/**
  * @brief  This function handles TIM16_IRQn
  * @param  None
  * @retval None
  */
void TIM16_IRQHandler(void){
	taskclass2();
		
  TIM_ClearITPendingBit(TIM16, TIM_IT_Update);
}

/**
  * @brief  This function handles TIM17_IRQn
  * @param  None
  * @retval None
  */
void TIM17_IRQHandler(void){
    /* timing for soft_rt_taskclass */
    static double  t = 0;

    if(t + T_PERIOD3_S < T_PERIOD_SOFT_RT_S)
    {
        t = t + T_PERIOD3_S;
    }
    else
    {
        t = t + T_PERIOD3_S - T_PERIOD_SOFT_RT_S;
        soft_rt_taskclass_signal = 1;
    }

    taskclass3();

    TIM_ClearITPendingBit(TIM17, TIM_IT_Update);
}

/**
  * @}
  */

#endif // __USING_TIMER

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
