/**
  * @file    ./SystemCore/inc/main.h
  * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
  * @brief   Header for main.c module
  */

#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "API.h"
#include "Library.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif // __MAIN_H
