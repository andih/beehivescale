/**
* @file    ./Library/src/seven_segment.c
* @author  Andreas Hirtenlehner
* @brief   library to control seven segment displays
*/

#include "LibUsings.h"
#ifdef __USING_7S

/* Includes ------------------------------------------------------------------*/
#include "seven_segment.h"

/** @addtogroup MCU_Library
  * @{
  */
	
/** @defgroup seven_segment 
  * @brief 7-segment Library functions
  * @{
  */
	
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

uint8_t data_buffer[10] = { 0 };
LIB_7S_display_assembly_t display_assembly = { 1, COMMON_ANODE };
LIB_7S_segment_portpin_t seg_pp;
LIB_7S_SPI_communication_t spi_com;
API_GPIO_type_t* digit_pp[10] = { 0 };
LIB_7S_format_t format = { 1, 0, RIGHT, 0 };

LIB_7S_segment_bit_position_t seg_pos = {
  1, /*a*/
  2, /*b*/
  3, /*c*/
  4, /*d*/
  5, /*e*/
  6, /*f*/
  7, /*g*/
  8  /*dp*/
};

LIB_7S_character_t characters = {
{
  0x3F, /*0*/
  0x06, /*1*/
  0x5B, /*2*/
  0x4F, /*3*/
  0x66, /*4*/
  0x6D, /*5*/
  0x7D, /*6*/
  0x07, /*7*/
  0x7F, /*8*/
  0x6F  /*9*/
},
  0x77, /*A*/
  0x7C, /*b*/
  0x39, /*C*/
  0x58, /*c*/
  0x5E, /*d*/
  0x79, /*E*/
  0x71, /*F*/
  0x7D, /*G*/
  0x76, /*H*/
  0x74, /*h*/
  0x06, /*I*/
  0x19, /*i*/
  0x0E, /*J*/
  0x0D, /*j*/
  0x38, /*L*/
  0x54, /*n*/
  0x5C, /*o*/
  0x50, /*r*/
  0x78, /*t*/
  0x3E, /*U*/
  0x1C, /*u*/
  0x40, /*-*/
  0x08, /*_*/
  0x80, /*.*/
};

/* Private function prototypes -----------------------------------------------*/

static uint32_t LIB_7S_pow10(uint8_t n);
void LIB_7S_set_display_assembly(uint8_t digit_count, LIB_7S_LED_configuration_t led_config);
void LIB_7S_set_segment_bit_position(uint8_t a, uint8_t b, uint8_t c, uint8_t d, uint8_t e, uint8_t f, uint8_t g, uint8_t dp);
void LIB_7S_set_segment_portpin(API_GPIO_type_t* a, API_GPIO_type_t* b, API_GPIO_type_t* c, API_GPIO_type_t* d, API_GPIO_type_t* e, API_GPIO_type_t* f, API_GPIO_type_t* g, API_GPIO_type_t* dp);
void LIB_7S_set_digit_portpin(uint8_t count, ... );
void LIB_7S_set_format(uint8_t leading_zeros, uint8_t decimal_places, LIB_alignment_t alignment, uint8_t indention);
void LIB_7S_set_brightness(API_TIM_type_t* API_TIMx, uint8_t CHx, double duty_cycle);
void LIB_7S_digit_increment(void);
void LIB_7S_GPIO_show_char(uint8_t character);
void LIB_7S_GPIO_clear(void);
void LIB_7S_SPI_init(API_SPI_type_t* API_SPIx, API_GPIO_type_t* mosi_portpin, API_GPIO_type_t* sck_portpin, API_GPIO_type_t* le_portpin, API_GPIO_type_t* n_oe_portpin, uint16_t baudrate_prescaler, uint8_t mode);
void LIB_7S_SPI_DMA_TIM_start_continious_tx(API_TIM_type_t* API_TIMx, double t_period_s);
void LIB_7S_SPI_DMA_TIM_stop_continious_tx(void);
void LIB_7S_SPI_DMA_TIM_show_number(double number);
void LIB_7S_SPI_DMA_TIM_show_string(uint8_t *p_characters);
void LIB_7S_SPI_clear(void);

/* Private functions ---------------------------------------------------------*/
/** @defgroup seven_segment_Private_Functions
  * @{
  */
	
/**
  * @brief  lookup table for 10^n calculation
  * @param  n: calculation parameter 0..9
  * @retval 10^n
  */
static uint32_t LIB_7S_pow10(uint8_t n)
{
  static uint32_t pow10[10] = {
    1, 10, 100, 1000, 10000,
    100000, 1000000, 10000000, 100000000, 1000000000
  };

  return pow10[n];
}

/**
  * @brief  config hardware parameters of the seven segment display
  * @param  digit_count: count of seven segment digits 1..10
  * @param  led_config: can be COMMON_ANODE or COMMON_CATHODE
  */
void LIB_7S_set_display_assembly(uint8_t digit_count, LIB_7S_LED_configuration_t led_config)
{
  display_assembly.digit_count = digit_count;
  display_assembly.led_config = led_config;
}

/**
  * @brief  config bit poitions of the segments a..g and dp
  * @param  a..g, dp: bit position 1..8
  * @note segment layout:
  *        _______
  *       |   A   |
  *      F|       |B
  *       |_______|
  *       |   G   |
  *      E|       |C
  *       |_______|  * DP
  *           D   
  *
  */
void LIB_7S_set_segment_bit_position(uint8_t a, uint8_t b, uint8_t c, uint8_t d, uint8_t e, uint8_t f, uint8_t g, uint8_t dp)
{
  uint8_t character_count;
  uint8_t old_character;
  uint8_t new_character = 0;

  for(character_count = 0; character_count < sizeof(LIB_7S_character_t); character_count++)
  {
    old_character = *(uint8_t*)((uint32_t)&characters + character_count);
		
		new_character = 0;

    if(old_character & (1 << (seg_pos.a-1))) new_character |= 1 << (a-1);
    if(old_character & (1 << (seg_pos.b-1))) new_character |= 1 << (b-1);
    if(old_character & (1 << (seg_pos.c-1))) new_character |= 1 << (c-1);
    if(old_character & (1 << (seg_pos.d-1))) new_character |= 1 << (d-1);
    if(old_character & (1 << (seg_pos.e-1))) new_character |= 1 << (e-1);
    if(old_character & (1 << (seg_pos.f-1))) new_character |= 1 << (f-1);
    if(old_character & (1 << (seg_pos.g-1))) new_character |= 1 << (g-1);
    if(old_character & (1 << (seg_pos.dp-1))) new_character |= 1 << (dp-1);

    *(uint8_t*)((uint32_t)&characters + character_count) = new_character;
  }

  seg_pos.a = a;
  seg_pos.b = b;
  seg_pos.c = c;
  seg_pos.d = d;
  seg_pos.e = e;
  seg_pos.f = f;
  seg_pos.g = g;
  seg_pos.dp = dp;
}

/**
  * @brief  config GPIOs of segments for parallel excitation
  * @param  a..g, dp: pointer to portpin (e.g. &PA0)
  */
void LIB_7S_set_segment_portpin(API_GPIO_type_t* a, API_GPIO_type_t* b, API_GPIO_type_t* c, API_GPIO_type_t* d, API_GPIO_type_t* e, API_GPIO_type_t* f, API_GPIO_type_t* g, API_GPIO_type_t* dp)
{
  seg_pp.a = a;
  seg_pp.b = b;
  seg_pp.c = c;
  seg_pp.d = d;
  seg_pp.e = e;
  seg_pp.f = f;
  seg_pp.g = g;
  seg_pp.dp = dp;
}

/**
  * @brief  config GPIOs to activate seven segment digits
  * @param  count: number of digits configured in LIB_7S_set_display_assembly()
  * @param  GPIOs: pointer to portpins (e.g. &PA0)
  */
void LIB_7S_set_digit_portpin(uint8_t count, ... )
{
  int i;
	va_list arguments;
	
	va_start(arguments, count);

  for(i = 0; i < count; i++)
  {
    digit_pp[i] = va_arg(arguments, API_GPIO_type_t*);
  }
	
	va_end(arguments);
}

/**
  * @brief  config the output format
  * @param  leading_zeros: + number of digits before the comma
	*												 + leading digits are filled with zeros
  *                        + bigger numbers are shown anyway
  * @param  alignment: can be RIGHT, CENTER or LEFT
  * @param  indention: shifting the value n digits
  */
void LIB_7S_set_format(uint8_t leading_zeros, uint8_t decimal_places, LIB_alignment_t alignment, uint8_t indention)
{
  format.leading_zeros = leading_zeros;
  format.decimal_places = decimal_places;
  format.alignment = alignment;
  format.indention = indention;
}

/**
  * @brief  config the brightness of the seven segment display
  * @param  API_TIMx: can be API_TIM1, API_TIM3, API_TIM14 .. API_TIM17
  * @param  CHx: Timer-channel (can be CH1, CH2, CH3 or CH4)
  * @param  duty_cycle: 0..1
  */
void LIB_7S_set_brightness(API_TIM_type_t* API_TIMx, uint8_t CHx, double duty_cycle)
{
  API_PWM_set_dutycycle(API_TIMx, CHx, duty_cycle);
}

/**
  * @brief  switch digits in ascending order
  * @note   portpins are configured in LIB_7S_set_digit_portpin()
  * @note   digit count and common connection are configured in LIB_7S_set_display_assembly()
  */
void LIB_7S_digit_increment(void)
{
  static uint8_t iniOK = 0;
  static uint8_t active_digit = 0;
  uint8_t i = 0;
	
	if(!iniOK && display_assembly.led_config == COMMON_ANODE)
	{
    for(i = 0; i < display_assembly.digit_count; i++)
    {
      API_setActiveDO(digit_pp[i], Bit_SET);
    }
	}
	else if(!iniOK)
  {
    for(i = 0; i < display_assembly.digit_count; i++)
    {
      API_setActiveDO(digit_pp[i], Bit_RESET);
    }
  }
  // else;
	
	if(display_assembly.led_config == COMMON_ANODE)
  {
    API_setActiveDO(digit_pp[active_digit], Bit_SET);
  }
  else
  {
    API_setActiveDO(digit_pp[active_digit], Bit_RESET);
  }
	
	if(!iniOK) { active_digit = display_assembly.digit_count; }
  else if(active_digit >= display_assembly.digit_count-1) { active_digit = 0; }
  else { active_digit++; }
	
  if(display_assembly.led_config == COMMON_ANODE)
  {
    API_setActiveDO(digit_pp[active_digit], Bit_RESET);
  }
  else
  {
    API_setActiveDO(digit_pp[active_digit], Bit_SET);
  }

  iniOK = 1;
}

/**
  * @brief  show character using parallel excitation
  * @param  character: character from the exported struct 'characters'
  */
void LIB_7S_GPIO_show_char(uint8_t character)
{
	if(display_assembly.led_config == COMMON_CATHODE)
	{
		API_setDO(seg_pp.a, (BitAction)(character & seg_pos.a));
		API_setDO(seg_pp.b, (BitAction)(character & seg_pos.b));
		API_setDO(seg_pp.c, (BitAction)(character & seg_pos.c));
		API_setDO(seg_pp.d, (BitAction)(character & seg_pos.d));
		API_setDO(seg_pp.e, (BitAction)(character & seg_pos.e));
		API_setDO(seg_pp.f, (BitAction)(character & seg_pos.f));
		API_setDO(seg_pp.g, (BitAction)(character & seg_pos.g));
		API_setDO(seg_pp.dp, (BitAction)(character & seg_pos.dp));
	}
	else 
	{
		API_setDO(seg_pp.a, (BitAction)!(character & seg_pos.a));
		API_setDO(seg_pp.b, (BitAction)!(character & seg_pos.b));
		API_setDO(seg_pp.c, (BitAction)!(character & seg_pos.c));
		API_setDO(seg_pp.d, (BitAction)!(character & seg_pos.d));
		API_setDO(seg_pp.e, (BitAction)!(character & seg_pos.e));
		API_setDO(seg_pp.f, (BitAction)!(character & seg_pos.f));
		API_setDO(seg_pp.g, (BitAction)!(character & seg_pos.g));
		API_setDO(seg_pp.dp, (BitAction)!(character & seg_pos.dp));
	}
}

/**
  * @brief  clear display when using parallel excitation
  */
void LIB_7S_GPIO_clear(void)
{
	if(display_assembly.led_config == COMMON_CATHODE)
	{
		API_setDO(seg_pp.a, Bit_RESET);
		API_setDO(seg_pp.b, Bit_RESET);
		API_setDO(seg_pp.c, Bit_RESET);
		API_setDO(seg_pp.d, Bit_RESET);
		API_setDO(seg_pp.e, Bit_RESET);
		API_setDO(seg_pp.f, Bit_RESET);
		API_setDO(seg_pp.g, Bit_RESET);
		API_setDO(seg_pp.dp, Bit_RESET);
	}
	else
	{
		API_setDO(seg_pp.a, Bit_SET);
		API_setDO(seg_pp.b, Bit_SET);
		API_setDO(seg_pp.c, Bit_SET);
		API_setDO(seg_pp.d, Bit_SET);
		API_setDO(seg_pp.e, Bit_SET);
		API_setDO(seg_pp.f, Bit_SET);
		API_setDO(seg_pp.g, Bit_SET);
		API_setDO(seg_pp.dp, Bit_SET);
	}

}

#ifdef __USING_SPI

/**
  * @brief  initilize SPI to control a LED driver
  * @param  API_SPIx:     where x can be (1, 2) to select the SPI
  * @param  mosi_portpin: MOSI portpin definition
  * @param  sck_portpin:  clock portpin definition
  * @param  le_portpin:   letch enable portpin definition (chip select hardware is used)
  * @param  n_oe_portpin: inverted output enable portpin definition (unused for now, the PWM output can be used to control this output)
  */
void LIB_7S_SPI_init(API_SPI_type_t* API_SPIx, API_GPIO_type_t* mosi_portpin, API_GPIO_type_t* sck_portpin, API_GPIO_type_t* le_portpin, API_GPIO_type_t* n_oe_portpin, uint16_t baudrate_prescaler, uint8_t mode)
{
	spi_com.API_SPIx = API_SPIx;
  spi_com.data_out = mosi_portpin;
  spi_com.clock = sck_portpin;
  spi_com.le = le_portpin;
  spi_com.n_oe = n_oe_portpin;

  API_SPI_init(API_SPIx, mosi_portpin, 0, sck_portpin, le_portpin, baudrate_prescaler, mode);
}

/**
  * @brief  start continious SPI transmission of the characters, triggered by the update event of a timer
  * @param  API_TIMx: can be API_TIM1, API_TIM3, API_TIM14 .. API_TIM17
  * @param  t_period_s: Time for one tx interval (one byte)
  */
void LIB_7S_SPI_DMA_TIM_start_continious_tx(API_TIM_type_t* API_TIMx, double t_period_s)
{
	spi_com.API_TIMx = API_TIMx;
  API_SPI_DMA_TIM_start_continious_tx(spi_com.API_SPIx, API_TIMx, t_period_s, TIM_DMA_Update, data_buffer, display_assembly.digit_count);
}

/**
  * @brief  stop continious SPI transmission
  */
void LIB_7S_SPI_DMA_TIM_stop_continious_tx(void)
{
  API_SPI_DMA_TIM_stop_continious_tx(spi_com.API_SPIx, spi_com.API_TIMx);
}

/**
  * @brief  show a value on the seven segment display
  * @param  number: double value to show
  */
void LIB_7S_SPI_DMA_TIM_show_number(double number)
{
  uint8_t int_count = 1; // count of integer digits
  uint8_t frac_count = 0; // count of fractional digits
  uint32_t ten_p_int_count = 10; // 10^int_count
	uint8_t digit_number;
  double digit_magnitude = 1;
  double number_residual = number;
  uint8_t i;

  if(number > 0xFFFFFFFF) return;

  while((uint32_t)(number / ten_p_int_count)) { ten_p_int_count = ten_p_int_count * 10; int_count++; }

  if(int_count > display_assembly.digit_count) {
    digit_magnitude = (double)LIB_7S_pow10(display_assembly.digit_count);
    int_count = display_assembly.digit_count;
  }
  else if(int_count < format.leading_zeros)
  {
    digit_magnitude = (double)LIB_7S_pow10(format.leading_zeros-1);
    int_count = format.leading_zeros;
  }
  else { digit_magnitude = ten_p_int_count / 10; }

  if(format.decimal_places + format.indention + int_count <= display_assembly.digit_count)
  {
    frac_count = format.decimal_places;
  }
  else
  {
    frac_count = display_assembly.digit_count - format.indention - int_count;
  }

  switch(format.alignment)
  {
    case LEFT:
      for(i = 0; i < 10; i++)
      {
        if(i < format.indention)
        {
          data_buffer[i] = 0;
        }
        else if(i < format.indention + int_count + frac_count && digit_magnitude == 0.1)
        {
          digit_number = (uint8_t)((double)(number_residual / digit_magnitude));
          number_residual -= digit_number * digit_magnitude;
          digit_magnitude /= 10;
          data_buffer[i] = characters.numeral[digit_number];
          data_buffer[i - 1] |= 1 << (seg_pos.dp-1);
        }
        else if(i < format.indention + int_count + frac_count)
        {
          digit_number = (uint8_t)((double)(number_residual / digit_magnitude));
          number_residual -= digit_number * digit_magnitude;
          digit_magnitude /= 10;
					data_buffer[i] = characters.numeral[digit_number];
        }
        else
        {
          data_buffer[i] = 0;
        }
      }
      break;
    case CENTER:
      for(i = 0; i < 10; i++)
      {
        if(i < (display_assembly.digit_count - int_count - frac_count) / 2)
        {
          data_buffer[i] = 0;
        }
        else if(i < (display_assembly.digit_count + int_count + frac_count) / 2 && digit_magnitude == 0.1)
        {
          digit_number = (uint8_t)((double)(number_residual / digit_magnitude));
          number_residual -= digit_number * digit_magnitude;
          digit_magnitude /= 10;
          data_buffer[i] = characters.numeral[digit_number];
          data_buffer[i - 1] |= 1 << (seg_pos.dp-1);
        }
        else if(i < (display_assembly.digit_count + int_count + frac_count) / 2)
        {
          digit_number = (uint8_t)((double)(number_residual / digit_magnitude));
          number_residual -= digit_number * digit_magnitude;
          digit_magnitude /= 10;
					data_buffer[i] = characters.numeral[digit_number];
        }
        else
        {
          data_buffer[i] = 0;
        }
      }
      break;
    case RIGHT:
      for(i = 0; i < 10; i++)
      {
        if(i < display_assembly.digit_count - format.indention - int_count - frac_count)
        {
          data_buffer[i] = 0;
        }
        else if(i < display_assembly.digit_count - format.indention && digit_magnitude == 0.1)
        {
          digit_number = (uint8_t)((double)(number_residual / digit_magnitude));
          number_residual -= digit_number * digit_magnitude;
          digit_magnitude /= 10;
					data_buffer[i] = characters.numeral[digit_number];
          data_buffer[i - 1] |= 1 << (seg_pos.dp-1);
        }
        else if(i < display_assembly.digit_count - format.indention)
        {
          digit_number = (uint8_t)((double)(number_residual / digit_magnitude));
          number_residual -= digit_number * digit_magnitude;
          digit_magnitude /= 10;
					data_buffer[i] = characters.numeral[digit_number];
        }
        else
        {
          data_buffer[i] = 0;
        }
      }
      break;
  }
}

void LIB_7S_SPI_DMA_TIM_show_string(uint8_t *p_characters)
{
	  uint8_t i;

		switch(format.alignment)
			{
				case LEFT:
					for(i = 0; i < 10; i++)
					{
						if(i < format.indention)
						{
							data_buffer[i] = 0;
						}
						else if(i < format.indention + format.leading_zeros)
						{
								data_buffer[i] = *(uint8_t*)&p_characters[i - format.indention];
						}
						else
						{
							data_buffer[i] = 0;
						}
					}
					break;
				case CENTER:
					for(i = 0; i < 10; i++)
					{
						if(i < (display_assembly.digit_count - format.leading_zeros) / 2)
						{
							data_buffer[i] = 0;
						}
						else if(i < (display_assembly.digit_count + format.leading_zeros) / 2)
						{
							data_buffer[i] = *(uint8_t*)&p_characters[i - (display_assembly.digit_count - format.leading_zeros) / 2];
						}
						else
						{
							data_buffer[i] = 0;
						}
					}
					break;
				case RIGHT:
					for(i = 0; i < 10; i++)
					{
						if(i < display_assembly.digit_count - format.indention - format.leading_zeros)
						{
							data_buffer[i] = 0;
						}
						else if(i < display_assembly.digit_count - format.indention)
						{
							data_buffer[i] = *(uint8_t*)&p_characters[i - (display_assembly.digit_count - format.indention - format.leading_zeros)];
						}
						else
						{
							data_buffer[i] = 0;
						}
					}
					break;
			}
}

/**
  * @brief  clear display when using SPI
  */
void LIB_7S_SPI_clear(void)
{
	int i;
	
	for(i = 0; i < display_assembly.digit_count; i++)
	{
		data_buffer[i] = 0;
	}
}

#endif // __USING_SPI

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 

#endif // __USING_7S
