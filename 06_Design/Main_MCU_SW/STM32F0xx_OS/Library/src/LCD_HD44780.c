

#include "GPIO.h"

#define oD7       PE14         /* uC-Portpin f�r Datenleitung D7 (LCD-Pin 14) */
#define oD6       PE13         /* uC-Portpin f�r Datenleitung D6 (LCD-Pin 13) */
#define oD5       PE12        /* uC-Portpin f�r Datenleitung D5 (LCD-Pin 12) */
#define oD4       PE11        /* uC-Portpin f�r Datenleitung D4 (LCD-Pin 11) */
#define oEN       PE9        /* uC-Portpin f�r Enable Leitung (LCD-Pin 6)   */
#define oRW       PE8
#define oRS       PE7         /* uC-Portpin f�r Register Select Leitung (LCD-Pin 4) */


#define T_enable    0.000015
#define T_PON       0.015
#define T_INIT      0.005
#define T_EXE   0.5 // 500�s execution time


#define CMD_CURSOR_AT_START 0x02

#define CMD_8BIT        0x30
#define CMD_4BIT        0x20
#define CMD_FUNCSET     0x2C // 4bit modus zweizeilig gro�e darstellung
#define CMD_OFF         0x08
#define CMD_CLEAR       0x01
#define CMD_ENTRYMODE   0x06 // Laufrichtung rechts
#define CMD_ON          0x0C // alles ein

char _oData;
char _bSend;

void LCD_HD44780(float TZyklus);
void LCD_Send(float TZyklus);

void LCD_Driver_HD44780(float TZyklus){
    LCD_HD44780(TZyklus);
    LCD_Send(TZyklus);
}

void LCD_HD44780(float TZyklus){
    static float t = 0;
    static char iniOK = 0;
    static char *State, *mState;
    static char oData = 0;
    static char oRegisterSelect = 0;
    static char oReadWrite = 0;
    char bSend = _bSend;
    char *sPON         = "Wait for Power On";
    char *sInit8Bit1   = "Set 8Bit Mode First Time";
    char *sInit8Bit2   = "Set 8Bit Mode Second Time";
    char *sInit8Bit3   = "Set 8Bit Mode Third Time";
    char *sInit4Bit    = "Set 4Bit Mode";
    char *sFuncSet     = "Set Display 2 rows 5x8";
    char *sOff         = "Turn Display off";
    char *sClear       = "Clear Display";
    char *sEntryMode   = "Cursor right - no shift";
    char *sOn          = "Turn Display on";
    /* normal operation */
    char *sWait        = "Wait";
    char *sSendHigh    = "Send high byte";
    char *sSendLow     = "Send low byte";

    /* Eing�nge */
    const char iSend = 1;




    do{
        if(!iniOK)                  t = 0;
        else if(State != mState)    t = 0;
        else                        t = t + TZyklus;
        mState = State;

        if(!iniOK)                                                  { State = sPON; }
        else if(State == sPON       && t > T_PON)                   { State = sInit8Bit1;   bSend = 1;  }
        else if(State == sInit8Bit1 && t > T_INIT && bSend == 0)    { State = sInit8Bit2;   bSend = 1;  }
        else if(State == sInit8Bit2 && t > T_INIT && bSend == 0)    { State = sInit8Bit3;   bSend = 1;  }
        else if(State == sInit8Bit3 && t > T_INIT && bSend == 0)    { State = sInit4Bit;    bSend = 1;  }
        else if(State == sInit4Bit  && t > T_EXE  && bSend == 0)    { State = sFuncSet;     bSend = 1;  }
        else if(State == sFuncSet   && t > T_EXE  && bSend == 0)    { State = sOff;         bSend = 1;  }
        else if(State == sOff       && t > T_EXE  && bSend == 0)    { State = sClear;       bSend = 1;  }
        else if(State == sClear     && t > T_EXE  && bSend == 0)    { State = sEntryMode;   bSend = 1;  }
        else if(State == sEntryMode && t > T_EXE  && bSend == 0)    { State = sOn;          bSend = 1;  }
        else if(State == sOn        && t > T_EXE  && bSend == 0)    { State = sWait;        bSend = 1;  }
        /* normal operation */
        else if(State == sWait && iSend && bSend == 0)              { State = sSendHigh;    bSend = 1;  }
        else if(State == sSendHigh && bSend == 0)                   { State = sSendLow;     bSend = 1;  }
        else if(State == sSendLow  && bSend == 0)                   { State = sWait;     }
        //else;
    }while(State != mState);

    /* oReadWrite */
    // immer schreiben
    if(!iniOK)  oReadWrite = 0;
    else        oReadWrite = 0;

    /* oRegisterSelect */
    if(!iniOK)                  oRegisterSelect = 0;
    else if(State == sSendHigh) oRegisterSelect = 1;
    else if(State == sSendLow)  oRegisterSelect = 1;
    else                        oRegisterSelect = 0;

    /* oData */
    if(!iniOK)                      oData = 0;
    else if(State == sInit8Bit1)    oData = CMD_8BIT;
    else if(State == sInit8Bit2)    oData = CMD_8BIT;
    else if(State == sInit8Bit3)    oData = CMD_8BIT;
    else if(State == sInit4Bit)     oData = CMD_4BIT;
    else if(State == sFuncSet)      oData = CMD_FUNCSET;
    else if(State == sOff)          oData = CMD_OFF;
    else if(State == sClear)        oData = CMD_CLEAR;
    else if(State == sEntryMode)    oData = CMD_ENTRYMODE;
    else if(State == sOn)           oData = CMD_ON;
    else if(State == sSendHigh)     oData = 'a'& 0xF0;
    else if(State == sSendLow)      oData = ('a' & 0x0F)<<4;
    //else;


    iniOK = 1;

    /* Ausgang */
    SetDO(oRW, oReadWrite);
    SetDO(oRS, oRegisterSelect);
    _oData = oData;
    _bSend = bSend;
}



void LCD_Send(float TZyklus){
    static float t;
    static char oEnable = 0;
    static char oData = 0;
    char bSend = _bSend;
    static char iniOK = 0;
    static char *State, *mState;
    char *sWait  = "Wait";
    char *sEnable = "Enable";
    char *sSend   = "Send";
    const char iData = _oData;


    do{
        if(!iniOK)                  t = 0;
        else if(State != mState)    t = 0;
        else                        t = t + TZyklus;

        mState = State;

        if(!iniOK)                                          { State = sWait; }
        else if(State == sWait && bSend && t > T_enable)    { State = sEnable; }
        else if(State == sEnable && t > T_enable)           { State = sSend;     bSend = 0;  }
        else if(State == sSend && t >  T_EXE)               { State = sWait; }
        //else;
    }while(mState != State);

    if(!iniOK)                  oEnable = 0;
    else if(State == sEnable)   oEnable = 1;
    else                        oEnable = 0;

    if(!iniOK)                  oData = 0;
    else if(State == sEnable)   oData = iData;
    else if(State == sSend)     oData = iData;
    else                        oData = 0;

    iniOK = 1;

    _bSend = bSend;
    SetDO(oEN,oEnable);
    SetDO(oD7, (oData & 0x80));
    SetDO(oD6, (oData & 0x40));
    SetDO(oD5, (oData & 0x20));
    SetDO(oD4, (oData & 0x10));
}

