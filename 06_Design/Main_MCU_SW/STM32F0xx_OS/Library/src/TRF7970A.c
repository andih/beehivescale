/**
  * @file    ./Library/src/TRF7970A.c
  * @author  Andreas Hirtenlehner
  * @brief   Library for the ti TRF7970A RFID IC
  ******************************************************************************
  * @RevisionHistory
	* 				 2015.04.07, V1.0.0, HA/PL,    -file created
	*          2015.04.08, V1.1.0, HA,       -TI_FindTag_StateMachine, first working code
	*                                        -known issues:     > collision handling not working
	*                                                           > when Tag stays in field it replies always with REQA command
	*                                                           > waiting for SPI with while(!flag)
	*                                        -planned features: > TimeStamp handling (RTC not implemented in current project)
  *                                                           > Debug Messages
	*          2015.04.17, V2.0.0, HA,       -redesigned StateMachine (splitted into LIB_TRF7970A_ISO14443A_statemachine, LIB_TRF7970A_enable_statemachine, LIB_TRF7970A_config_ISO14443A_statemachine, LIB_TRF7970A_find_tag_ISO14443A_statemachine)
	*                                        -bug fixes:        > collision handling is working (was a device bug: RX Wait Time Register was set to 0x1F after writing to ISO Control Register, correct value: 0x07)
  *                                        -planned features: > switching off magnetic field when it's not needed
  *          2015.08.05, V2.1.0, HA,       -bug fixes:        > driver is working with higher period times then 100�s (error in IRQ signal handling)
  *                                        -implemented new SPI API (replaced TI_SPI_WriteRead() with SPI_WriteRead_StateMachine() in SPI.c, reason: no while loop)
  *                                        -disabled collision handling
  *                                        -Rx CRC check enabled
  *                                        -Automatic Gain Control enabled
  *          2015.08.18, V2.1.1, HA,       -reset when Chip Status Register is incorrect (ESHUTDOWN)
  *                                        -reset when IC can't send to Tag (ECONN)
  *          2015.08.27, V2.2.0, HA,       -reorganize state machines for a more standardized structure
  *                                        -made arguments of LIB_TRF7970A_ISO14443A_statemachine() as inputs
  *          2015.09.26, V2.2.1, HA,       -add DMA support for SPI
  *          2016.07.22, V2.2.2, HA,       -remove oDataOK from LIB_TRF7970A_ISO14443A_statemachine and use return value to detect new tags
  *                                        -define RFID_READING_PERIOD_S and RFID_COMMAND
  *          2017.02.28, V2.3.0, HA,       -port to new API in STM32F4xx_OS
  *                                        -add LIB_TRF7970A_config
  *                                        -add LIB_TRF7970A_tag_data_t
  ******************************************************************************
  */
  
#include "LibUsings.h"
#ifdef __USING_TRF7970A

/* Includes ------------------------------------------------------------------*/
#include "TRF7970A.h"

/** @addtogroup MCU_Library
  * @{
  */

/** @defgroup TRF7970A 
  * @brief Library functions for NFC controller
  * @{
  */
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static LIB_TRF7970A_config_t lib_config = { &API_SPI2, &PB15, &PB14, &PB13, &PB12, &PD10, &PB10, &PB11, 0.01 };
LIB_TRF7970A_tag_data_t LIB_TRF7970A_tag_data;

/* Private function prototypes -----------------------------------------------*/
void LIB_TRF7970A_lib_config(API_SPI_type_t* API_SPIx, API_GPIO_type_t* mosi_portpin, API_GPIO_type_t* miso_portpin, API_GPIO_type_t* sck_portpin, API_GPIO_type_t* nss_portpin, API_GPIO_type_t* irq_portpin, API_GPIO_type_t* en_portpin, API_GPIO_type_t* en2_portpin, double reading_period_s);
error_t LIB_TRF7970A_ISO14443A_statemachine(double TPeriod_s);
static error_t LIB_TRF7970A_enable_statemachine(double TPeriod_s);
static error_t LIB_TRF7970A_config_ISO14443A_statemachine(double TPeriod_s);
static error_t LIB_TRF7970A_find_tag_ISO14443A_statemachine(double TPeriod_s, uint8_t REQA_WUPA, uint8_t *pUID, uint8_t *pUIDLength, TM_RTC_Time_t *pTimestamp, uint16_t *pATQA, uint8_t *pSAK, uint8_t *pISO14443_4_compatible, uint8_t *pRSSILevel);
static error_t LIB_TRF7970A_tag_write_read_statemachine(double TPeriod_s, uint8_t *pTxData,uint8_t TxLength, uint8_t *pRxData, uint8_t *pRxLength, uint16_t *CollPos, uint8_t useCRC);
static void LIB_TRF7970A_build_tx_frame(uint8_t *data, uint8_t length, uint8_t useCRC, uint8_t *frame);

/* Private functions ---------------------------------------------------------*/
/** @defgroup TRF7970A_Private_Functions
  * @{
  */

/**
  * @brief  config SPI, portpins, and reading period
	* @param  API_SPIx
	* @param  mosi_portpin
	* @param  miso_portpin
	* @param  sck_portpin
	* @param  nss_portpin
	* @param  irq_portpin
	* @param  en_portpin
	* @param  en2_portpin
	* @param  reading_period_s
  * @retval none
  */
void LIB_TRF7970A_lib_config(API_SPI_type_t* API_SPIx, 
  API_GPIO_type_t* mosi_portpin, 
  API_GPIO_type_t* miso_portpin, 
  API_GPIO_type_t* sck_portpin,
  API_GPIO_type_t* nss_portpin,
  API_GPIO_type_t* irq_portpin,
  API_GPIO_type_t* en_portpin,
  API_GPIO_type_t* en2_portpin,
  double           reading_period_s)
{
  lib_config.API_SPIx          = API_SPIx;
  lib_config.mosi_portpin      = mosi_portpin;
  lib_config.miso_portpin      = miso_portpin;
  lib_config.sck_portpin       = sck_portpin;
  lib_config.nss_portpin       = nss_portpin;
  lib_config.irq_portpin       = irq_portpin;
  lib_config.en_portpin        = en_portpin;
  lib_config.en2_portpin       = en2_portpin;
  lib_config.reading_period_s  = reading_period_s;
}

/**
  * @brief  state machine to configure the TRF7970A and find ISO14443A Tags
	* @param  TPeriod_s: time period
  * @retval error number: 0..Tag found, EBUSY..searching for Tags, EIO..SPI error, EBADMSG..collision occurred, ESHUTDOWN..IC is not active, ECOMM..IC can't send to Tag, EMSGSIZE..error in TX data generation, ENODATA..error in TX data generation, ECANCELD..unknown error
  */
error_t LIB_TRF7970A_ISO14443A_statemachine(double TPeriod_s)
{
  static uint8_t Z         = 0;
  static uint8_t mZ        = 0;
  static uint8_t ENABLE    = 0x01;
  static uint8_t CONFIG    = 0x02;
  static uint8_t FIND_TAG  = 0x03;
  static uint8_t TAG_FOUND = 0x04;
  static uint8_t WAIT      = 0x05;

  static uint8_t iniOK                 = 0;
  static double  t                     = 0;
  static double  t_wait                = 0;
  static error_t error                 = 0;
  static uint8_t ISO14443_4_compatible = 0;

  static TM_RTC_Time_t Timestamp;
  static uint8_t UID[10];
  static uint8_t UIDlength = 0;
  static uint8_t RSSILevel = 0;
  static uint8_t SAK       = 0;
  static uint16_t ATQA     = 0;

  do
  {
    if(!iniOK)       { t = 0;             }
    else if(Z != mZ) { t = 0;             }
    else             { t = t + TPeriod_s; }

    mZ = Z;

    if(!iniOK) { 
      Z = ENABLE; 
      mZ = Z; 
      t_wait = 0; 
      ISO14443_4_compatible = 0; 
      API_SPI_init(lib_config.API_SPIx, 
        lib_config.mosi_portpin, 
        lib_config.miso_portpin, 
        lib_config.sck_portpin, 
        lib_config.nss_portpin, 
        lib_config.SPI_BaudRatePrescaler_x, 
        SPI_MODE_1);  
    }
    else if(Z == ENABLE    && t > 0 && error == 0)     { Z = CONFIG;                                          }
    else if(Z == ENABLE    && t > 0 && error != EBUSY) { iniOK = 0; return error;                             }
    else if(Z == CONFIG    && t > 0 && error == 0)     { Z = FIND_TAG;                                        }
    else if(Z == CONFIG    && t > 0 && error != EBUSY) { iniOK = 0; return error;                             }
    else if(Z == FIND_TAG  && t > 0 && error == 0)     { Z = TAG_FOUND;                                       }
    else if(Z == FIND_TAG  && t > 0 && error == ETIME) { Z = WAIT; t_wait = lib_config.reading_period_s - t;  }
    else if(Z == FIND_TAG  && t > 0 && error != EBUSY) { iniOK = 0; return error;                             }
    else if(Z == TAG_FOUND && t > 0)                   { Z = WAIT; t_wait = lib_config.reading_period_s - t;  }
    else if(Z == WAIT      && t > 0 && t > t_wait)     { Z = FIND_TAG;                                        }
    else if(t > 1.0)                                   { iniOK = 0; return ECANCELED;                         }
    //else;
  }
  while(Z != mZ);

  if(!iniOK)
  {
    LIB_TRF7970A_tag_data.atqa       = 0;
    LIB_TRF7970A_tag_data.rssi_level = 0;
    LIB_TRF7970A_tag_data.sak        = 0;
    LIB_TRF7970A_tag_data.uid_length = 0;
    memset(LIB_TRF7970A_tag_data.uid, 0, sizeof(LIB_TRF7970A_tag_data.uid));
  }
  else if(Z == TAG_FOUND)
  {
    LIB_TRF7970A_tag_data.atqa       = ATQA;
    LIB_TRF7970A_tag_data.rssi_level = RSSILevel;
    LIB_TRF7970A_tag_data.sak        = SAK;
    LIB_TRF7970A_tag_data.timestamp  = Timestamp;
    LIB_TRF7970A_tag_data.uid_length = UIDlength;
    memcpy(LIB_TRF7970A_tag_data.uid, UID, sizeof(LIB_TRF7970A_tag_data.uid));
  }

  if(!iniOK)             { error = EBUSY;                                                                                                                                     }
  else if(Z == ENABLE)   { error = LIB_TRF7970A_enable_statemachine(TPeriod_s);                                                                                                         }
  else if(Z == CONFIG)   { error = LIB_TRF7970A_config_ISO14443A_statemachine(TPeriod_s);                                                                                               }
  else if(Z == FIND_TAG) { error = LIB_TRF7970A_find_tag_ISO14443A_statemachine(TPeriod_s, RFID_COMMAND, UID, &UIDlength, &Timestamp, &ATQA, &SAK, &ISO14443_4_compatible, &RSSILevel);  }
  else                   { error = EBUSY;                                                                                                                                     }

  iniOK = 1;

  if(Z == TAG_FOUND) { return 0;     }
  else               { return EBUSY; }
}

/**
  * @brief  state machine to enable the TRF7970A
	* @param  TPeriod_s: time period
	* @note   disables the IC at the beginning
  * @retval error number: 0..IC enabled, EBUSY..enable IC, ECANCELD..unknown error
  */
error_t LIB_TRF7970A_enable_statemachine(double TPeriod_s)
{
  static uint8_t Z       = 0;
  static uint8_t mZ      = 0;
  static uint8_t DISABLE = 0x01; //Power Down Mode 1 (EN, EN2 = 0)
  static uint8_t DE_INIT = 0x02; //deselect TI (NSS = 1)
  static uint8_t ENABLE1 = 0x03; //Power Down Mode 2, Sleep Mode (EN = 0, EN2 = 1)
  static uint8_t ENABLE2 = 0x04; //stand-by mode (EN = 1, EN2 = X)

  static uint8_t iniOK = 0;
  static double  t     = 0;

  do
  {
    if(!iniOK)       { t = 0;             }
    else if(Z != mZ) { t = 0;             }
    else             { t = t + TPeriod_s; }

    mZ = Z;

    if(!iniOK)                          { Z = DISABLE; mZ = Z;         }
    else if(Z == DISABLE && t >= 0.002) { Z = DE_INIT;                 }
    else if(Z == DE_INIT && t >= 0.005) { Z = ENABLE1;                 }
    else if(Z == ENABLE1 && t >= 0.005) { Z = ENABLE2;                 }
    else if(Z == ENABLE2 && t >= 0.005) { iniOK = 0; return 0;         }
    else if(t > 1.0)                    { iniOK = 0; return ECANCELED; }
    //else;
  }
  while(Z != mZ);

  if(Z == DISABLE)
  { 
    API_setDO(lib_config.en_portpin,  Bit_RESET); 
    API_setDO(lib_config.en2_portpin, Bit_RESET); 
    API_setDO(lib_config.nss_portpin, Bit_RESET); 
  }
  else if(Z == DE_INIT) { API_setDO(lib_config.nss_portpin, Bit_SET); }
  else if(Z == ENABLE1) { API_setDO(lib_config.en2_portpin, Bit_SET); }
  else if(Z == ENABLE2) { API_setDO(lib_config.en_portpin,  Bit_SET); }
  //else;

  iniOK = 1;

  return EBUSY;
}

/**
  * @brief  state machine to configure the TRF7970A
	* @param  TPeriod_s: time period
	* @note   performs a SoftInit at the beginning (all old configurations will be deleted)
  * @retval error number: 0..IC configuration finished, EBUSY..configure IC, EIO..SPI error, ECANCELD..unknown error
  */
error_t LIB_TRF7970A_config_ISO14443A_statemachine(double TPeriod_s)
{
    static uint8_t Z              = 0;
    static uint8_t mZ             = 0;
    static uint8_t SOFT_INIT      = 0x01; //send Software Initialization command
    static uint8_t SOFT_INIT_WAIT = 0x02;
    static uint8_t IDLE           = 0x03;
    static uint8_t IDLE_WAIT      = 0x04;
    static uint8_t CONF1          = 0x05; //config register 0x00, 0x01
    static uint8_t CONF1_WAIT     = 0x06;
    static uint8_t CONF2          = 0x07; //config register 0x08, 0x09, 0x0A
    static uint8_t CONF2_WAIT     = 0x08;
    static uint8_t CONF3          = 0x09; //config register 0x0D

    static uint8_t iniOK = 0;
    static double  t     = 0;
    static error_t error = 0;
    static uint8_t TxData[4];

    do
    {
        if(!iniOK)       { t = 0;             }
        else if(Z != mZ) { t = 0;             }
        else             { t = t + TPeriod_s; }

        mZ = Z;

        if(!iniOK)                                          { Z = SOFT_INIT; mZ = Z;        }
        else if(Z == SOFT_INIT && t > 0 && error == 0)      { Z = SOFT_INIT_WAIT;           }
        else if(Z == SOFT_INIT && t > 0 && error != EBUSY)  { iniOK = 0; return error;      }
        else if(Z == SOFT_INIT_WAIT && t >= 0.001)          { Z = IDLE;                     }
        else if(Z == IDLE && t > 0 && error == 0)           { Z = IDLE_WAIT;                }
        else if(Z == IDLE && t > 0 && error != EBUSY)       { iniOK = 0; return error;      }
        else if(Z == IDLE_WAIT && t >= 0.001)               { Z = CONF1;                    }
        else if(Z == CONF1 && t > 0 && error == 0)          { Z = CONF1_WAIT;               }
        else if(Z == CONF1 && t > 0 && error != EBUSY)      { iniOK = 0; return error;      }
        else if(Z == CONF1_WAIT && t >= 0.005)              { Z = CONF2;                    }
        else if(Z == CONF2 && t > 0 && error == 0)          { Z = CONF2_WAIT;               }
        else if(Z == CONF2 && t > 0 && error != EBUSY)      { iniOK = 0; return error;      }
        else if(Z == CONF2_WAIT && t >= 0.005)              { Z = CONF3;                    }
        else if(Z == CONF3 && t > 0 && error == 0)          { iniOK = 0; return 0;          }
        else if(Z == CONF3 && t > 0 && error != EBUSY)      { iniOK = 0; return error;      }
        else if(t > 1.0)                                    { iniOK = 0; return ECANCELED;  }
        //else;
    }
    while(Z != mZ);

    error = 0;

    if(!iniOK)              { __RESET_TXDATA();                                                                                                                                   }
    else if(Z == SOFT_INIT) { TxData[0] = SendCommand | SoftInit;                                                                                                                 }
    else if(Z == IDLE)      { TxData[0] = SendCommand | Idle;                                                                                                                     }
    else if(Z == CONF1)     { TxData[0] = SetMultiRegister | ChipStateControl_Address; TxData[1] = ChipStateControl; TxData[2] = ISOControl;                                      }
    else if(Z == CONF2)     { TxData[0] = SetMultiRegister | RXWaitTime_Address; TxData[1] = RXWaitTime_ISO14443AB; TxData[2] = ModulatorControl; TxData[3] = RXSpecialSettings;  }
    else if(Z == CONF3)     { TxData[0] = SetSingleRegister | IRQMask_Address; TxData[1] = IRQMask;                                                                               }
    //else;

    if(!iniOK)              { error = EBUSY;                                                                                           }
    else if(Z == SOFT_INIT) { error = API_SPI_DMA_send_then_receive_statemachine(lib_config.API_SPIx, TPeriod_s, TxData, 1, 0, 0, 0);  }
    else if(Z == IDLE)      { error = API_SPI_DMA_send_then_receive_statemachine(lib_config.API_SPIx, TPeriod_s, TxData, 1, 0, 0, 0);  }
    else if(Z == CONF1)     { error = API_SPI_DMA_send_then_receive_statemachine(lib_config.API_SPIx, TPeriod_s, TxData, 3, 0, 0, 0);  }
    else if(Z == CONF2)     { error = API_SPI_DMA_send_then_receive_statemachine(lib_config.API_SPIx, TPeriod_s, TxData, 4, 0, 0, 0);  }
    else if(Z == CONF3)     { error = API_SPI_DMA_send_then_receive_statemachine(lib_config.API_SPIx, TPeriod_s, TxData, 2, 0, 0, 0);  }
    else                    { error = EBUSY;                                                                                           }

    iniOK = 1;

    return EBUSY;
}

/**
  * @brief  state machine for finding ISO 14443A Tags
	* @param  TPeriod_s: time period
	* @param  REQA_WUPA: REQA..send Request command (0x26), WUPA..send Wake-up command (0x52)
	* @param  pUID: received UID
	* @param  pUIDLength: number of bytes from UID
	* @param  pTimestamp: Time Stamp when Tag answers first time (data from RTC)
	* @param  pATQA: Answer to Request, Type A (ATQA and SAK defines card type)
	* @param  pSAK: Select acknowledge (ATQA and SAK defines card type)
	* @param  pISO14443_4_compatible: 0..Tag is not compatible to ISO14443-4, 1..Tag is compatible to ISO14443-4
	* @param  pRSSILevel: RSSI level of Main input
  * @retval error number: 0..Tag found, EBUSY..searching for Tags, ETIME..no Tag found, EBADMSG..collision occurred, ESHUTDOWN..IC is not active, ECOMM..IC can't send to Tag, EIO..SPI error, EMSGSIZE..error in TX data generation, ENODATA..error in TX data generation, ECANCELD..unknown error
  */
error_t LIB_TRF7970A_find_tag_ISO14443A_statemachine(double TPeriod_s, uint8_t REQA_WUPA, uint8_t *pUID, uint8_t *pUIDLength, TM_RTC_Time_t *pTimestamp, uint16_t *pATQA, uint8_t *pSAK, uint8_t *pISO14443_4_compatible, uint8_t *pRSSILevel)
{
  static uint8_t Z                = 0;
  static uint8_t mZ               = 0;
  static uint8_t SEND_REQA_WUPA   = 0x01; //send Request or Wake-up command to Tag
  static uint8_t GET_STATUS_REG   = 0x02;
  static uint8_t CHECK_STATUS_REG = 0x03;
  static uint8_t GET_RSSI_LEVEL   = 0x04;
  static uint8_t SELECT_RX_IN     = 0x05; //select RX input with higher RSSI level
  static uint8_t CHANGE_RX_IN     = 0x06;
  static uint8_t GET_UID          = 0x07; //get part of UID from Tag
  static uint8_t GET_UID_RETRY    = 0x08; //get part of UID from Tag after bit collision
  static uint8_t SELECT_UID       = 0x09; //select all Tags with known UID
  static uint8_t SAK_CHECK        = 0x0A; //check if UID is complete

  static uint8_t  iniOK          = 0;
  static double   t              = 0;
  static error_t  error          = 0;
  static uint16_t collPos        = 0; //collision position
  static uint8_t  RxLength       = 0;
  static uint8_t  SEL            = 0; //Select code
  static uint8_t  NVB            = 0; //Number of Valid Bits
  static uint8_t  SAK            = 0; //Select acknowledge
  static uint16_t ATQA           = 0; //Answer to Request, Type A
  static uint8_t  RSSILevel_MAIN = 0; //Main input RSSI Level: 0..7
  static uint8_t  RSSILevel_AUX  = 0; //Aux input RSSI Level: 0..7
  static uint8_t  active_input   = 0; //0..Main input -> IN1, Aux input -> IN2; pm_on..Main input -> IN2, Aux input -> IN1
  static uint8_t  statusReg      = 0;
  static uint8_t  RxData[10];
  static uint8_t  mRxData[10];
  static uint8_t  TxData[10];

  if(!iniOK)                                      { SAK = 0; ATQA = 0; RSSILevel_MAIN = 0; RSSILevel_AUX = 0; active_input = 0; __RESET_MRXDATA();                          }
  else if(Z == SEND_REQA_WUPA && error == 0)      { ATQA = (RxData[1] << 8) | RxData[0];                                                                                    }
  else if(Z == GET_STATUS_REG && error == 0)      { statusReg = RxData[0];                                                                                                  }
  else if(Z == GET_RSSI_LEVEL && error == 0)      { RSSILevel_MAIN = RxData[0] & 0x07; RSSILevel_AUX = (RxData[0] & 0x38) >> 3;                                             }
  else if(Z == GET_UID && error == 0)             { mRxData[0] = RxData[0]; mRxData[1] = RxData[1]; mRxData[2] = RxData[2]; mRxData[3] = RxData[3]; mRxData[4] = RxData[4]; }
  else if(Z == GET_UID && error == EBADMSG)       { mRxData[0] = RxData[0]; mRxData[1] = RxData[1]; mRxData[2] = RxData[2]; mRxData[3] = RxData[3]; mRxData[4] = RxData[4]; }
  else if(Z == GET_UID_RETRY && error == 0)       { mRxData[0] = RxData[0]; mRxData[1] = RxData[1]; mRxData[2] = RxData[2]; mRxData[3] = RxData[3]; mRxData[4] = RxData[4]; }
  else if(Z == GET_UID_RETRY && error == EBADMSG) { mRxData[0] = RxData[0]; mRxData[1] = RxData[1]; mRxData[2] = RxData[2]; mRxData[3] = RxData[3]; mRxData[4] = RxData[4]; }
  else if(Z == SELECT_UID && error == 0)          { SAK = RxData[0];                                                                                                        }
  //else;

  do
  {
    if(!iniOK)       { t = 0;             }
    else if(Z != mZ) { t = 0;             }
    else             { t = t + TPeriod_s; }

    mZ = Z;

    if(!iniOK)                                                                          { Z = SEND_REQA_WUPA; mZ = Z; collPos = 0; RxLength = 0; SEL = 0; NVB = 0;                                          }
    else if(Z == SEND_REQA_WUPA && t > 0 && error == 0)                                 { API_RTC_get_datetime(pTimestamp, TM_RTC_Format_BIN); Z = GET_STATUS_REG;                                            }
    else if(Z == SEND_REQA_WUPA && t > 0 && error == ETIME)                             { ATQA = 0; Z = GET_STATUS_REG;                                                                                     }
    else if(Z == SEND_REQA_WUPA && t > 0 && error != EBUSY)                             { iniOK = 0; return error;                                                                                          }
    else if(Z == GET_STATUS_REG && t > 0 && error == 0)                                 { Z = CHECK_STATUS_REG;                                                                                             }
    else if(Z == GET_STATUS_REG && t > 0 && error != EBUSY)                             { iniOK = 0; return error;                                                                                          }
    else if(Z == CHECK_STATUS_REG && t > 0 && (statusReg & ~pm_on) == ChipStateControl) { active_input = statusReg & pm_on; Z = GET_RSSI_LEVEL;                                                             }
    else if(Z == CHECK_STATUS_REG && t > 0)                                             { iniOK = 0; return ESHUTDOWN;                                                                                      }
    else if(Z == GET_RSSI_LEVEL && t > 0 && error == 0)                                 { Z = SELECT_RX_IN;                                                                                                 }
    else if(Z == GET_RSSI_LEVEL && t > 0 && error != EBUSY)                             { iniOK = 0; return error;                                                                                          }
    else if(Z == SELECT_RX_IN && t > 0 && RSSILevel_AUX > RSSILevel_MAIN)               { Z = CHANGE_RX_IN; *pRSSILevel = RSSILevel_AUX;                                                                    }
    else if(Z == SELECT_RX_IN && t > 0 && ATQA != 0)                                    { *pRSSILevel = RSSILevel_MAIN; Z = GET_UID; SEL = CASCADE_LEVEL_1; NVB = 0x20;                                     }
    else if(Z == SELECT_RX_IN && t > 0)                                                 { *pRSSILevel = RSSILevel_MAIN; iniOK = 0; return ETIME;                                                            }
    else if(Z == CHANGE_RX_IN && t > 0 && error == 0 && ATQA != 0)                      { Z = GET_UID; SEL = CASCADE_LEVEL_1; NVB = 0x20;                                                                   }
    else if(Z == CHANGE_RX_IN && t > 0 && error == 0)                                   { ATQA = 0; Z = SEND_REQA_WUPA;                                                                                     }
    else if(Z == CHANGE_RX_IN && t > 0 && error != EBUSY)                               { iniOK = 0; return error;                                                                                          }
    else if(Z == GET_UID && t > 0 && error == 0)                                        { Z = SELECT_UID; NVB = 0x70;                                                                                       }
    else if(Z == GET_UID && t > 0 && error == EBADMSG)                                  { Z = GET_UID_RETRY; NVB = collPos;                                                                                 }
    else if(Z == GET_UID && t > 0 && error == ETIME)                                    { iniOK = 0; return ETIME;                                                                                          }
    else if(Z == GET_UID && t > 0 && error != EBUSY)                                    { iniOK = 0; return error;                                                                                          }
    else if(Z == GET_UID_RETRY && t > 0 && error == 0)                                  { Z = SELECT_UID; NVB = 0x70;                                                                                       }
    else if(Z == GET_UID_RETRY && t > 0 && error == EBADMSG)                            { NVB = collPos;                                                                                                    }
    else if(Z == GET_UID_RETRY && t > 0 && error == ETIME)                              { iniOK = 0; return ETIME;                                                                                          }
    else if(Z == GET_UID_RETRY && t > 0 && error != EBUSY)                              { iniOK = 0; return error;                                                                                          }
    else if(Z == SELECT_UID && t > 0 && error == 0)                                     { Z = SAK_CHECK;                                                                                                    }
    else if(Z == SELECT_UID && t > 0 && error == ETIME)                                 { iniOK = 0; return ETIME;                                                                                          }
    else if(Z == SELECT_UID && t > 0 && error != EBUSY)                                 { iniOK = 0; return error;                                                                                          }
    else if(Z == SAK_CHECK && t > 0 && (SAK & 0x04) == 0x04 && SEL == CASCADE_LEVEL_1)  { pUID[0] = mRxData[1]; pUID[1] = mRxData[2]; pUID[2] = mRxData[3]; SEL = CASCADE_LEVEL_2; NVB = 0x20; Z = GET_UID; } //Cascade bit set: UID not complete
    else if(Z == SAK_CHECK && t > 0 && (SAK & 0x04) == 0x04 && SEL == CASCADE_LEVEL_2)  { pUID[3] = mRxData[1]; pUID[4] = mRxData[2]; pUID[5] = mRxData[3]; SEL = CASCADE_LEVEL_3; NVB = 0x20; Z = GET_UID; } //Cascade bit set: UID not complete
    else if(Z == SAK_CHECK && t > 0 && (SAK & 0x04) == 0x00 && SEL == CASCADE_LEVEL_1)
    {
      //UID complete
      pUID[0] = mRxData[0];
      pUID[1] = mRxData[1];
      pUID[2] = mRxData[2];
      pUID[3] = mRxData[3];
      *pUIDLength = 4;
      *pISO14443_4_compatible = (SAK & 0x20) >> 5;
      *pATQA = ATQA;
      *pSAK  = SAK;

      iniOK = 0;
      return 0;
    }
    else if(Z == SAK_CHECK && t > 0 && (SAK & 0x24) == 0x00 && SEL == CASCADE_LEVEL_2)
    {
      //UID complete
      pUID[3] = mRxData[0];
      pUID[4] = mRxData[1];
      pUID[5] = mRxData[2];
      pUID[6] = mRxData[3];
      *pUIDLength = 7;
      *pISO14443_4_compatible = (SAK & 0x20) >> 5;
      *pATQA = ATQA;
      *pSAK  = SAK;

      iniOK = 0;
      return 0;
    }
    else if(Z == SAK_CHECK && t > 0 && (SAK & 0x24) == 0x00 && SEL == CASCADE_LEVEL_3)
    {
      //UID complete
      pUID[6] = mRxData[0];
      pUID[7] = mRxData[1];
      pUID[8] = mRxData[2];
      pUID[9] = mRxData[3];
      *pUIDLength = 10;
      *pISO14443_4_compatible = (SAK & 0x20) >> 5;
      *pATQA = ATQA;
      *pSAK  = SAK;

      iniOK = 0;
      return 0;
    }
    else if(Z == SAK_CHECK && t > 0)                                                    { iniOK = 0; return ECANCELED;                                                                                      }
    else if(t > 1.0)                                                                    { iniOK = 0; return ECANCELED;                                                                                      }
    //else;
  }
  while(Z != mZ);

  if(!iniOK)                    { __RESET_TXDATA();                                                                                                                                         }
  else if(Z == SEND_REQA_WUPA)  { TxData[0] = REQA_WUPA;                                                                                                                                    }
  else if(Z == GET_STATUS_REG)  { TxData[0] = GetSingleRegister | ChipStateControl_Address;                                                                                                 }
  else if(Z == GET_RSSI_LEVEL)  { TxData[0] = GetSingleRegister | RSSILevels_Address;                                                                                                       }
  else if(Z == CHANGE_RX_IN)    { TxData[0] = SetSingleRegister | ChipStateControl_Address; TxData[1] = (ChipStateControl & ~pm_on) | (~active_input & pm_on);                              }
  else if(Z == GET_UID)         { TxData[0] = SEL; TxData[1] = NVB;                                                                                                                         }
  else if(Z == GET_UID_RETRY)   { TxData[0] = SEL; TxData[1] = NVB; TxData[2] = mRxData[0]; TxData[3] = mRxData[1]; TxData[4] = mRxData[2]; TxData[5] = mRxData[3]; TxData[6] = mRxData[4]; }
  else if(Z == SELECT_UID)      { TxData[0] = SEL; TxData[1] = NVB; TxData[2] = mRxData[0]; TxData[3] = mRxData[1]; TxData[4] = mRxData[2]; TxData[5] = mRxData[3]; TxData[6] = mRxData[4]; }
  //else

  if(!iniOK)                    { error = EBUSY; __RESET_RXDATA();                                                                              }
  else if(Z == SEND_REQA_WUPA)  { error = LIB_TRF7970A_tag_write_read_statemachine(TPeriod_s, TxData, 1, RxData, &RxLength, &collPos, 0);                  }
  else if(Z == GET_STATUS_REG)  { error = API_SPI_DMA_send_then_receive_statemachine(lib_config.API_SPIx, TPeriod_s, TxData, 1, RxData, 1, 0);  }
  else if(Z == GET_RSSI_LEVEL)  { error = API_SPI_DMA_send_then_receive_statemachine(lib_config.API_SPIx, TPeriod_s, TxData, 1, RxData, 1, 0);  }
  else if(Z == CHANGE_RX_IN)    { error = API_SPI_DMA_send_then_receive_statemachine(lib_config.API_SPIx, TPeriod_s, TxData, 2, 0, 0, 0);       }
  else if(Z == GET_UID)         { error = LIB_TRF7970A_tag_write_read_statemachine(TPeriod_s, TxData, 2, RxData, &RxLength, &collPos, 0);                  }
  else if(Z == GET_UID_RETRY)   { error = LIB_TRF7970A_tag_write_read_statemachine(TPeriod_s, TxData, 4, RxData, &RxLength, &collPos, 0);                  }
  else if(Z == SELECT_UID)      { error = LIB_TRF7970A_tag_write_read_statemachine(TPeriod_s, TxData, 7, RxData, &RxLength, &collPos, 1);                  }
  else                          { error = EBUSY;                                                                                                }

  iniOK = 1;

  return EBUSY;
}

/**
  * @brief  state machine to send and receive Data from Tag
	* @param  TPeriod_s: time period
	* @param  pTxData: Data to send
	* @param  TxLength: number of bytes to send
	* @param  pRxData: received Data
	* @param  pRxLength: number of bytes to receive
	* @param  pCollPos: collision position
	* @param  useCRC: 0..don't send CRC, 1..send CRC
  * @retval error number: 0..data received, EBUSY..sending or receiving data, ETIME..no answer from Tag, EBADMSG..collision occurred, ECOMM..IC can't send to Tag, EIO..SPI error, EMSGSIZE..TX size too big, ENODATA..no data to send, ECANCELD..unknown error
  */
error_t LIB_TRF7970A_tag_write_read_statemachine(double TPeriod_s, uint8_t *pTxData, uint8_t TxLength, uint8_t *pRxData, uint8_t *pRxLength, uint16_t *pCollPos, uint8_t useCRC)
{
    static uint8_t Z                 = 0;
    static uint8_t mZ                = 0;
    static uint8_t SET_RX_CRC        = 0x01;
    static uint8_t SEND              = 0x02;
    static uint8_t READ_IRQ          = 0x03;
    static uint8_t CHECK_IRQ         = 0x04;
    static uint8_t READ_FIFO_STATUS  = 0x05;
    static uint8_t CHECK_FIFO_STATUS = 0x06;
    static uint8_t READ_FIFO         = 0x07;
    static uint8_t RESET             = 0x08;

    static uint8_t  iniOK           = 0;
    static double   t               = 0;
    static uint8_t  FIFO_count_byte = 0;
    static uint8_t  IRQ_reg         = 0;
    static uint8_t  mIRQ_reg        = 0;
    static uint16_t raceCount       = 0;
    static double   raceTime        = 0;
    static uint8_t  Zold            = 0;
    static error_t  error           = 0;
    static uint8_t  sendInProgress  = 0;
    static uint8_t  RxData[10];
    static uint8_t  TxData[16];

    if(!iniOK)                                    { FIFO_count_byte = 0; IRQ_reg = 0; mIRQ_reg = 0; }
    else if(Z == READ_IRQ && error == 0)          { mIRQ_reg = IRQ_reg; IRQ_reg = RxData[0];        }
    else if(Z == READ_FIFO_STATUS && error == 0)  { FIFO_count_byte = RxData[0];                    }
    //else;

    do
    {
        if(!iniOK)       { t = 0;             }
        else if(Z != mZ) { t = 0;             }
        else             { t = t + TPeriod_s; }

        if(!iniOK)         { raceCount = 0; }
        else if(Z == Zold) { raceCount++;   }
        else if(Z == mZ);
        else               { raceCount = 0; }

        if(!iniOK)         { raceTime = 0;                    }
        else if(Z == Zold) { raceTime = raceTime + TPeriod_s; }
        else if(Z == mZ)   { raceTime = raceTime + TPeriod_s; }
        else               { raceTime = 0;                    }

        if(!iniOK)       { Zold = Z;  }
        else if(Z != mZ) { Zold = mZ; }
        //else;

        mZ = Z;

        if(!iniOK)                                                                                    { Z = SET_RX_CRC; mZ = Z;  sendInProgress  = 0; }
        else if(Z == SET_RX_CRC && t > 0 && error == 0)                                               { Z = SEND;                                     }
        else if(Z == SET_RX_CRC && t > 0 && error != EBUSY)                                           { iniOK = 0; return error;                      }
        else if(Z == SEND && TxLength > 10)                                                           { iniOK = 0; return EMSGSIZE;                   }
        else if(Z == SEND && TxLength <= 0)                                                           { iniOK = 0; return ENODATA;                    }
        else if(Z == SEND && t > 0 && error == 0)                                                     { Z = READ_IRQ;                                 }
        else if(Z == SEND && t > 0 && error != EBUSY)                                                 { iniOK = 0; return error;                      }
        else if(Z == READ_IRQ && t > 0 && error == 0)                                                 { Z = CHECK_IRQ;                                }
        else if(Z == READ_IRQ && t > 0 && error != EBUSY)                                             { iniOK = 0; return error;                      }
        else if(Z == CHECK_IRQ && t > 0 && (IRQMask & En_irq_err1) && (IRQ_reg & Irq_err1) && useCRC) { iniOK = 0; return ECANCELED;                  }
        else if(Z == CHECK_IRQ && t > 0 && (IRQMask & En_irq_err2) && (IRQ_reg & Irq_err2))           { iniOK = 0; return ECANCELED;                  }
        else if(Z == CHECK_IRQ && t > 0 && (IRQMask & En_irq_err3) && (IRQ_reg & Irq_err3))           { iniOK = 0; return ECANCELED;                  }
        else if(Z == CHECK_IRQ && t > 0 && (IRQ_reg & Irq_col))                                       { iniOK = 0; return ECANCELED;                  }
        else if(Z == CHECK_IRQ && t > 0 && (IRQMask & En_irq_noresp) && (IRQ_reg & Irq_noresp))       { iniOK = 0; return ETIME;                      }
        else if(Z == CHECK_IRQ && t > 0 && !(IRQ_reg & Irq_srx) && (mIRQ_reg & Irq_srx))              { Z = READ_FIFO_STATUS;                         }
        else if(Z == CHECK_IRQ && t > 0 && (IRQ_reg & Irq_tx) && !sendInProgress)                     { sendInProgress = 1;                           }
        else if(Z == CHECK_IRQ && t > 0 && raceTime > 0.005 && raceCount > 3 && sendInProgress)       { iniOK = 0; return ETIME;                      }
        else if(Z == CHECK_IRQ && t > 0 && raceTime > 0.005 && raceCount > 3)                         { iniOK = 0; return ECOMM;                      }
        else if(Z == CHECK_IRQ && t > 0.0002)                                                         { Z = READ_IRQ;                                 }
        else if(Z == READ_FIFO_STATUS && t > 0 && error == 0)                                         { Z = CHECK_FIFO_STATUS;                        }
        else if(Z == READ_FIFO_STATUS && t > 0 && error != EBUSY)                                     { iniOK = 0; return error;                      }
        else if(Z == CHECK_FIFO_STATUS && t > 0 && FIFO_count_byte > 0)                               { Z = READ_FIFO;                                }
        else if(Z == CHECK_FIFO_STATUS && t > 0)                                                      { iniOK = 0; return ECANCELED;                  }
        else if(Z == READ_FIFO && t > 0 && error == 0)                                                { Z = RESET; *pRxLength = FIFO_count_byte;      }
        else if(Z == READ_FIFO && t > 0 && error != EBUSY)                                            { iniOK = 0; return error;                      }
        else if(Z == RESET && t > 0 && error == 0)                                                    { iniOK = 0; return 0;                          }
        else if(Z == RESET && t > 0 && error != EBUSY)                                                { iniOK = 0; return error;                      }
        else if(t > 1.0)                                                                              { iniOK = 0; return ECANCELED;                  }
        //else;
    }
    while(Z != mZ);

    if(!iniOK)                      { __RESET_TXDATA();                                                                                                       }
    else if(Z == SET_RX_CRC)        { TxData[0] = SetSingleRegister | ISOControl_Address; TxData[1] = (ISOControl & ~(rx_crc_n)) | (((!useCRC) & 0x01) << 7); }
    else if(Z == SEND)              { LIB_TRF7970A_build_tx_frame(pTxData, TxLength, useCRC, TxData);                                                                        }
    else if(Z == READ_IRQ)          { TxData[0] = GetMultiRegister  | IRQStatus_Address;                                                                      }
    else if(Z == READ_FIFO_STATUS)  { TxData[0] = GetSingleRegister | FIFOStatus_Address;                                                                     }
    else if(Z == READ_FIFO)         { TxData[0] = GetMultiRegister  | FIFO_Address;                                                                           }
    else if(Z == RESET)             { TxData[0] = SendCommand       | Reset;                                                                                  }
    //else;

    if(!iniOK)                     { error = EBUSY; __RESET_RXDATA();                                                                                                       }
    else if(Z == SET_RX_CRC)       { error = API_SPI_DMA_send_then_receive_statemachine(lib_config.API_SPIx, TPeriod_s, TxData, 2,            0,       0,               0); }
    else if(Z == SEND)             { error = API_SPI_DMA_send_then_receive_statemachine(lib_config.API_SPIx, TPeriod_s, TxData, 5 + TxLength, 0,       0,               0); }
    else if(Z == READ_IRQ)         { error = API_SPI_DMA_send_then_receive_statemachine(lib_config.API_SPIx, TPeriod_s, TxData, 1,            RxData,  2,               0); }
    else if(Z == READ_FIFO_STATUS) { error = API_SPI_DMA_send_then_receive_statemachine(lib_config.API_SPIx, TPeriod_s, TxData, 1,            RxData,  1,               0); }
    else if(Z == READ_FIFO)        { error = API_SPI_DMA_send_then_receive_statemachine(lib_config.API_SPIx, TPeriod_s, TxData, 1,            pRxData, FIFO_count_byte, 0); }
    else if(Z == RESET)            { error = API_SPI_DMA_send_then_receive_statemachine(lib_config.API_SPIx, TPeriod_s, TxData, 1,            0,       0,               0); }
    else                           { error = EBUSY;                                                                                                                         }

    iniOK = 1;

    return EBUSY;
}

void LIB_TRF7970A_build_tx_frame(uint8_t *data, uint8_t length, uint8_t useCRC, uint8_t *frame)
{
    uint8_t i = 0;

    frame[0] = SendCommand | Reset;
    frame[2] = SetMultiRegister | TXLenghtByte1_Address;

    if(useCRC && length == 1)
    {
        frame[1] = SendCommand | TransmitCRC;
        frame[3] = 0x00;
        frame[4] = 0x0F;
    }
    else if(useCRC && length > 1)
    {
        frame[1] = SendCommand | TransmitCRC;
        frame[3] = (uint8_t)(length >> 8);
        frame[4] = (uint8_t)(length << 4);
    }
    else if(!useCRC && length == 1)
    {
        frame[1] = SendCommand | TransmitNoCRC;
        frame[3] = 0x00;
        frame[4] = 0x0F;
    }
    else if(!useCRC && length > 1)
    {
        frame[1] = SendCommand | TransmitNoCRC;
        frame[3] = (uint8_t)(length >> 8);
        frame[4] = (uint8_t)(length << 4);
    }

    for(i = 0; i < length; i++)
    {
        frame[i+5] = *(data+i);
    }
}

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 

#endif // __USING_TRF7970A
