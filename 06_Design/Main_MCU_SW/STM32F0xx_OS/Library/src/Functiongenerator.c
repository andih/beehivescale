/**
  ******************************************************************************
  * @file    ./STM32Fx_OS/Library/src/Functiongenerator.c 
  * @author  EG, HA
  * @version V1.0.0
  * @date    08-November-2013
  * @brief   Functiongenerator function block
  ******************************************************************************
  * @RevisionHistory
	* 				 2013.11.08, V1.0.0, HA, -Datei Erstellung 
  *                                  -Funktionen Rectangle(), Sine(), Triangle(), Noise() hinzugefgt
  ******************************************************************************
  */
  
/* Includes ------------------------------------------------------------------*/
#include "Functiongenerator.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void Rectangle(DAC_Type DAC_x, double TPeriod, double Upp, double DCOffset, double DutyCycle, uint32_t StepsPerPeriod);
void Sine(DAC_Type DAC_x, double TPeriod, double Upp, double DCOffset, double Symmetry, uint32_t StepsPerPeriod);
void Triangle(DAC_Type DAC_x, double TPeriod, double Upp, double DCOffset, double Symmetry, uint32_t StepsPerPeriod);
void Noise(DAC_Type DAC_x);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Set a rectangle-signal on the selected analog output.
  * @param  DAC_x: where x can be (1,2) to select the DAC
  * @param  TPeriode: Periodtime of th eoutput signal
  * @param  Upp: peak-peak voltage of the output signal, can be (0..Vref_V)
  * @param  DCOffset: can be (0..Vref_V)
  * @param  DutyCycle: can be (0..1.0)
  * @param  StepsPerPeriod: number of voltege-steps in one period
  * @retval none
  */
void Rectangle(DAC_Type DAC_x, double TPeriod, double Upp, double DCOffset, double DutyCycle, uint32_t StepsPerPeriod){
  static uint16_t Rechteck[Rectangle_ArrayLength];
  static uint32_t i           = 0;
  double    LowPegel_digit    = (4095.0 / 3.3) * DCOffset - (4095.0 / 3.3 / 2.0) * Upp;
  double    HighPegel_digit   = (4095.0 / 3.3) * DCOffset + (4095.0 / 3.3 / 2.0) * Upp;
  uint32_t  Symmetry_Schritte = StepsPerPeriod * DutyCycle;
  
  if(HighPegel_digit > 4095)   HighPegel_digit = 4095;
  else if(HighPegel_digit < 0) HighPegel_digit = 0;
  
  if(LowPegel_digit > 4095)   LowPegel_digit = 4095;
  else if(LowPegel_digit < 0) LowPegel_digit = 0;

  for(i = 0; i <= StepsPerPeriod; i++){
    if(i <= Symmetry_Schritte) Rechteck[i] = (uint16_t)LowPegel_digit;   
    else                       Rechteck[i] = (uint16_t)HighPegel_digit;
  }
  
  SetUaDMA(DAC_x, (uint32_t*)&Rechteck[0], TPeriod, StepsPerPeriod);
}

/**
  * @brief  Set a sine-signal on the selected analog output.
  * @param  DAC_x: where x can be (1,2) to select the DAC
  * @param  TPeriode: Periodtime of th eoutput signal
  * @param  Upp: peak-peak voltage of the output signal, can be (0..Vref_V)
  * @param  DCOffset: can be (0..Vref_V)
  * @param  Symmetry: can be (0..1.0)
  * @param  StepsPerPeriod: number of voltege-steps in one period
  * @retval none
  */
void Sine(DAC_Type DAC_x, double TPeriod, double Upp, double DCOffset, double Symmetry, uint32_t StepsPerPeriod){
  static uint16_t Sinus[Sine_ArrayLength];
  static uint32_t i              = 0;
  static double   x              = 0;
  static int      Uout_digit     = 0;
  const double    ZaehlerSchritt = pow(2.0 * PI, 1.0 / (Symmetry + 0.5)) / StepsPerPeriod;
  const double    DCOffset_digit = (4095.0 / 3.3) * DCOffset;
  const double    Up_digit       = (4095.0 / 3.3 / 2.0) * Upp;

  for(i = 0; i <= StepsPerPeriod; i++){
    x = x + ZaehlerSchritt;
   
    Uout_digit = DCOffset_digit + sin(pow(x,(Symmetry + 0.5))) * Up_digit;

    if(Uout_digit > 4095)   Uout_digit = 4095;
    else if(Uout_digit < 0) Uout_digit = 0;

    Sinus[i] = Uout_digit;
  }  
  
  SetUaDMA(DAC_x, (uint32_t*)&Sinus, TPeriod, StepsPerPeriod);
}

/**
  * @brief  Set a triangle-signal on the selected analog output.
  * @param  DAC_x: where x can be (1,2) to select the DAC
  * @param  TPeriode: Periodtime of th eoutput signal
  * @param  Upp: peak-peak voltage of the output signal, can be (0..Vref_V)
  * @param  DCOffset: can be (0..Vref_V)
  * @param  Symmetry: can be (0..1.0)
  * @param  StepsPerPeriod: number of voltege-steps in one period
  * @retval none
  */
void Triangle(DAC_Type DAC_x, double TPeriod, double Upp, double DCOffset, double Symmetry, uint32_t StepsPerPeriod){
  static uint16_t Dreieck[Triangle_ArrayLength];
  static uint32_t i                    = 0;
  static int      Uout_digit           = 0;
  const double    DCOffset_digit       = (4095.0 / 3.3) * DCOffset;
  const double    Upp_digit            = (4095.0 / 3.3) * Upp;
  const uint32_t  Symmetry_Schritte    = StepsPerPeriod * Symmetry;
  const double    SchrittPositiv_digit = Upp_digit / Symmetry_Schritte;
  const double    SchrittNegativ_digit = Upp_digit / (StepsPerPeriod - Symmetry_Schritte);

  Uout_digit = DCOffset_digit - Upp_digit / 2.0;

  for(i = 0; i <= StepsPerPeriod; i++){
    if(i <= Symmetry_Schritte) Uout_digit += SchrittPositiv_digit;   
    else                       Uout_digit -= SchrittNegativ_digit;

    if(Uout_digit > 4095)   Uout_digit = 4095;
    else if(Uout_digit < 0) Uout_digit = 0;

    Dreieck[i] = Uout_digit;
  }
  
  SetUaDMA(DAC_x, (uint32_t*)&Dreieck, TPeriod, StepsPerPeriod);
}


/**
  * @brief  Set a noise-signal on the selected analog output.
  * @param  DAC_x: where x can be (1,2) to select the DAC
  * @retval none
  */
void Noise(DAC_Type DAC_x){
  DAC_InitTypeDef DAC_InitStructure;
  
  /* DAC channel Configuration */
  DAC_InitStructure.DAC_Trigger = DAC_x.TriggerSource;
  DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_Noise;
  DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bits10_0;
  DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
  DAC_Init(DAC_x.DAC_Channel_x, &DAC_InitStructure);

  /* Enable DAC Channel */
  DAC_Cmd(DAC_x.DAC_Channel_x, ENABLE);

  /* Set DAC channel DHR12RD register */
	if(DAC_x.DAC_Channel_x == DAC_1.DAC_Channel_x)      DAC_SetChannel1Data(DAC_Align_12b_R, 0x7FF0);
	else if(DAC_x.DAC_Channel_x == DAC_2.DAC_Channel_x) DAC_SetChannel2Data(DAC_Align_12b_R, 0x7FF0);
  //else;	
}
