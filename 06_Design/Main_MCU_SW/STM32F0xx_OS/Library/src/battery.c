/**
 * @file    ./Library/src/battery.c
 * @author  Andreas Hirtenlehner
 * @brief   monitoring battery charge
 */

#include "LibUsings.h"
#ifdef __USING_BATTERY

/* Includes ------------------------------------------------------------------*/
#include "battery.h"

/** @addtogroup MCU_Library
  * @{
  */

/** @defgroup battery 
  * @brief Library functions monitor battery charge
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
void LIB_battery_add_measurement(LIB_battery_t* battery);

/* Private functions ---------------------------------------------------------*/

/** @defgroup battery_Private_Functions
  * @{
  */

/**
  * @brief  get parameters of an input signal on a portpin
  * @param  hw_signal: input signal of type LIB_hw_signal_t
  * @param  period_time_s: sampling interval
	* @note   debounce time: 5ms
  */
void LIB_battery_add_measurement(LIB_battery_t* battery)
{
	datetime_t datetime;
	API_RTC_get_datetime(&datetime, TM_RTC_Format_BCD);
	
	const double du_dt = (battery->u_bat_v - battery->m_u_bat_v) / (datetime.unix - battery->m_datetime_unix);
	
	if(du_dt >= LIB_BATTERY_FULL_DU_DT && battery->u_bat_v >= battery->u_mean_v)		  { battery->state = FULL;        }
	else if(du_dt <= -LIB_BATTERY_LOW_DU_DT && battery->u_bat_v <= battery->u_mean_v)	{ battery->state = LOW;         }
	else if(du_dt >= LIB_BATTERY_STATE_CHANGE_DU_DT_MIN)                              { battery->state = CHARGING;    }
	else if(du_dt <= -LIB_BATTERY_STATE_CHANGE_DU_DT_MIN)                             { battery->state = DISCHARGING; }
	
	if(battery->m_state == FULL && battery->state == DISCHARGING)                                      { battery->u_mean_attempt_valid = 1; }
	else if(battery->m_state == DISCHARGING && battery->state == LOW && battery->u_mean_attempt_valid) { battery->u_mean_attempt_valid = 1; }
	else if(battery->m_state != battery->state)                                                        { battery->u_mean_attempt_valid = 0; }
	else if(battery->state == LOW)                                                                     { battery->u_mean_attempt_valid = 0; }
	
	if(battery->m_state != battery->state) { battery->state_change_timestamp = datetime.unix; }
	
	if(battery->state == LOW && battery->u_mean_attempt_valid) { battery->u_mean_v = battery->u_accu_vs / (double)datetime.unix; }
	
	if(battery->state != battery->m_state) { battery->u_accu_vs = 0; }
	else                                   { battery->u_accu_vs = battery->u_bat_v * (datetime.unix - battery->m_datetime_unix); }
	
	if(battery->state == FULL && battery->m_state == CHARGING) { battery->u_full_v = battery->u_bat_v; }
	
	if(battery->state == LOW && battery->m_state == DISCHARGING) { battery->u_low_v = battery->u_bat_v; }
	
	battery->m_state = battery->state;
	battery->m_datetime_unix = datetime.unix;
	battery->m_u_bat_v = battery->u_bat_v;
}


/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 

#endif // __USING_LIB_HW_SIGNAL_READ
