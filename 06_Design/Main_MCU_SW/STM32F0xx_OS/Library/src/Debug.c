/**
 * @file    ./Library/src/Debug.c
 * @author  Gerald Ebmer
 * @brief   Debug Utility
 */

#include "LibUsings.h"
#ifdef __USING_DEBUG

/* Includes ------------------------------------------------------------------*/
#include "Debug.h"

/** @addtogroup MCU_Library
  * @{
  */

/** @defgroup Debug 
  * @brief Debug Library functions
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define HARDFAULT_DEBUG_LEVEL (HARDFAULT_DEBUG | SYSTEM_DBG_LEVEL_SEVERE)

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void LIB_DEBUG_hardfault_handler_wrapper(void);
void LIB_DEBUG_hardfault_handler(unsigned int * hardfault_args, unsigned lr_value);
void LIB_DEBUG_textcolor(int attr, int fg, int bg);
void LIB_DEBUG_pulse(API_GPIO_type_t* portpin, uint8_t cnt);
void LIB_DEBUG_tic(unsigned long *ticks);
unsigned long LIB_DEBUG_toc(unsigned long *ticks);

/* Private functions ---------------------------------------------------------*/
/** @defgroup Debug_Private_Functions
  * @{
  */
	
/**
  * @brief  Hard Fault handler wrapper in assembly
  * @param  none
  * @note   It extracts the location of stack frame and passes it to handler
  *         in C as a pointer. We also extract the LR value as second
  *         parameter.
  * @retval none
  */
void LIB_DEBUG_hardfault_handler_wrapper(void)
{
#ifdef __gnuc__
  asm volatile(
    "MOVS r0, #4;"
    "MOV r1, LR;"
    "TST r0, r1;"
    "BEQ stacking_used_MSP;"
    "MRS R0, PSP ;"//    first parameter - stacking was using PSP
    "B get_LR_and_branch;"
    "stacking_used_MSP:"
    "MRS R0, MSP ;"    //first parameter - stacking was using MSP
    "get_LR_and_branch:"
    "MOV R1, LR;"    //second parameter is LR current value
    "LDR R2,=(LIB_DEBUG_hardfault_handler);"
    "BX R2"
  );
#else // Keil
/*  int value0;
  int value1;
  int value2;
  
  __asm volatile(
    "MOVS value0, #4;"
    "MOV value1, __return_address();"
    "TST value0, value1;"
    "BEQ stacking_used_MSP;"
    "MRS value0, PSP ;"//    first parameter - stacking was using PSP
    "B get_LR_and_branch;"
    "stacking_used_MSP:"
    "MRS value0, MSP ;"    //first parameter - stacking was using MSP
    "get_LR_and_branch:"
    "MOV value1, __return_address();"    //second parameter is LR current value
    "LDR value2, =LIB_DEBUG_hardfault_handler;"
    "BX value2"
  );*/
#endif // __gnuc__
  
}

/**
  * @brief  Hard Fault handler in C
  * @param  hardfault_args
  * @param  lr_value
  * @note   with stack frame location and LR value
  *         extracted from the assembly wrapper as input parameters
  * @retval none
  */
void LIB_DEBUG_hardfault_handler(unsigned int * hardfault_args, unsigned lr_value)
{
    unsigned int stacked_r0;
    unsigned int stacked_r1;
    unsigned int stacked_r2;
    unsigned int stacked_r3;
    unsigned int stacked_r12;
    unsigned int stacked_lr;
    unsigned int stacked_pc;
    unsigned int stacked_psr;
    stacked_r0 = ((unsigned long) hardfault_args[0]);
    stacked_r1 = ((unsigned long) hardfault_args[1]);
    stacked_r2 = ((unsigned long) hardfault_args[2]);
    stacked_r3 = ((unsigned long) hardfault_args[3]);
    stacked_r12 = ((unsigned long) hardfault_args[4]);
    stacked_lr = ((unsigned long) hardfault_args[5]);
    stacked_pc = ((unsigned long) hardfault_args[6]);
    stacked_psr = ((unsigned long) hardfault_args[7]);
    SYSTEM_DEBUGF_TEXT(HARDFAULT_DEBUG_LEVEL, "[Hard fault handler]\n");
    SYSTEM_DEBUGF(HARDFAULT_DEBUG_LEVEL, "R0 = %x\n", stacked_r0);
    SYSTEM_DEBUGF(HARDFAULT_DEBUG_LEVEL, "R1 = %x\n", stacked_r1);
    SYSTEM_DEBUGF(HARDFAULT_DEBUG_LEVEL, "R2 = %x\n", stacked_r2);
    SYSTEM_DEBUGF(HARDFAULT_DEBUG_LEVEL, "R3 = %x\n", stacked_r3);
    SYSTEM_DEBUGF(HARDFAULT_DEBUG_LEVEL, "R12 = %x\n", stacked_r12);
    SYSTEM_DEBUGF(HARDFAULT_DEBUG_LEVEL, "Stacked LR = %x\n", stacked_lr);
    SYSTEM_DEBUGF(HARDFAULT_DEBUG_LEVEL, "Stacked PC = %x\n", stacked_pc);
    SYSTEM_DEBUGF(HARDFAULT_DEBUG_LEVEL, "Stacked PSR = %x\n", stacked_psr);
    SYSTEM_DEBUGF(HARDFAULT_DEBUG_LEVEL, "Current LR = %x\n", lr_value);
    while(1); // endless loop
}

void LIB_DEBUG_textcolor(int attr, int fg, int bg)
{	char command[13];

	/* Command is the control command to the terminal */
	sprintf(command, "%c[%d;%d;%dm", 0x1B, attr, fg + 30, bg + 40);
	printf("%s", command);
}

/**
  * @brief  Get processor temperature
  * @param  none
  * @retval temperature in �C
  */
void LIB_DEBUG_pulse(API_GPIO_type_t* portpin, uint8_t cnt)
{
    int i = cnt;
    while(i)
    {
        API_setDO(portpin, Bit_SET);
        API_setDO(portpin, Bit_RESET);
        i--;
    }
}

/**
  * @brief  evaluates current value of systick timer
  * @param  pointer to uint32 variable to store systicks
  * @retval address of data segment
  */
void LIB_DEBUG_tic(unsigned long *ticks)
{
  *ticks = SysTick->VAL;
}


/**
  * @brief  evaluates current systick timer value and returns systick span between tic() and toc()
  * @param  pointer to uint32 variable with stored systicks count
  * @retval system clock cycle span between function call tic() and toc()
  */
unsigned long LIB_DEBUG_toc(unsigned long *ticks)
{
  unsigned long tocks;
  tocks = SysTick->VAL;
  *ticks = (*ticks-tocks);
  return *ticks;
}

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 

#endif // __USING_DEBUG
