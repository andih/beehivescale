/**
 * @file    ./Library/src/hw_signal_read.c
 * @author  Andreas Hirtenlehner
 * @brief   reading a hardware digital input
 */

#include "LibUsings.h"
#ifdef __USING_HW_SIGNAL_READ

/* Includes ------------------------------------------------------------------*/
#include "hw_signal_read.h"

/** @addtogroup MCU_Library
  * @{
  */

/** @defgroup hw_signal_read 
  * @brief Library functions to read input signals on portpins
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void LIB_hw_signal_read(LIB_hw_signal_t* hw_signal, double period_time_s);
void LIB_hw_signal_reset(LIB_hw_signal_t* hw_signal);
uint8_t LIB_hw_signal_debounce(LIB_hw_signal_t* hw_signal, double period_time_s);
uint8_t LIB_hw_signal_pos_edge(LIB_hw_signal_t* hw_signal, double period_time_s);
uint8_t LIB_hw_signal_neg_edge(LIB_hw_signal_t* hw_signal, double period_time_s);
uint8_t LIB_hw_signal_edge(LIB_hw_signal_t* hw_signal, double period_time_s);

/* Private functions ---------------------------------------------------------*/
/** @defgroup hw_signal_read_Private_Functions
  * @{
  */
	
void LIB_hw_signal_reset(LIB_hw_signal_t* hw_signal)
{
	hw_signal->t_on_s = 0; 
  hw_signal->t_off_s = 0;	
	hw_signal->o_debounced = 0;
	hw_signal->o_pos_edge = 0;
	hw_signal->o_neg_edge = 0;
	hw_signal->o_edge = 0;
	hw_signal->m_o_debounced = hw_signal->o_debounced;
}
	
/**
  * @brief  get parameters of an input signal on a portpin
  * @param  hw_signal: input signal of type LIB_hw_signal_t
  * @param  period_time_s: sampling interval
	* @note   debounce time: 5ms
  */
void LIB_hw_signal_read(LIB_hw_signal_t *hw_signal, double period_time_s)
{
		static int iniOK = 0;
	
    hw_signal->i_signal = API_getDI(hw_signal->portpin);

		if(!iniOK) 												 { hw_signal->t_on_s = 0; 						  }
    else if(hw_signal->t_on_s >= 1e8)  { /* limit */                          }
    else if(hw_signal->i_signal)  		 { hw_signal->t_on_s += period_time_s;  }
    else                          		 { hw_signal->t_on_s = 0.0;             }

		if(!iniOK) 												 { hw_signal->t_off_s = 0; 							}
    else if(hw_signal->t_off_s >= 1e8) { /* limit */                          }
    else if(!hw_signal->i_signal) 		 { hw_signal->t_off_s += period_time_s; }
    else                          		 { hw_signal->t_off_s = 0.0;            }

		if(!iniOK) 																									 { hw_signal->o_debounced = 0; }
    else if(hw_signal->i_signal && hw_signal->t_on_s >= 0.005)   { hw_signal->o_debounced = 1; }
    else if(!hw_signal->i_signal && hw_signal->t_off_s >= 0.005) { hw_signal->o_debounced = 0; }

		if(!iniOK)																					     	   { hw_signal->o_pos_edge = 0; }
    else if(hw_signal->o_debounced && !hw_signal->m_o_debounced) { hw_signal->o_pos_edge = 1; }
    else                                                         { hw_signal->o_pos_edge = 0; }

		if(!iniOK)																					     	   { hw_signal->o_pos_edge = 0; }
    else if(!hw_signal->o_debounced && hw_signal->m_o_debounced) { hw_signal->o_neg_edge = 1; }
    else                                                         { hw_signal->o_neg_edge = 0; }

  	if(!iniOK)						     	   { hw_signal->o_edge = 0; }
    if(hw_signal->o_pos_edge)      { hw_signal->o_edge = 1; }
    else if(hw_signal->o_neg_edge) { hw_signal->o_edge = 1; }
    else                           { hw_signal->o_edge = 0; }

    hw_signal->m_o_debounced = hw_signal->o_debounced;
		
		iniOK = 1;
}

/**
  * @brief  debounce input signal on a portpin
  * @param  hw_signal: input signal of type LIB_hw_signal_t
  * @param  period_time_s: sampling interval
  * @retval debounced input signal
	* @note   debounce time: 5ms
  */
uint8_t LIB_hw_signal_debounce(LIB_hw_signal_t* hw_signal, double period_time_s)
{
    LIB_hw_signal_read(hw_signal, period_time_s);

    return hw_signal->o_debounced;
}

/**
  * @brief  detect positive edge of the input signal
  * @param  hw_signal: input signal of type LIB_hw_signal_t
  * @param  period_time_s: sampling interval
  * @retval 1: positive edge detected, 0: else
	* @note   debounce time: 5ms
  */
uint8_t LIB_hw_signal_pos_edge(LIB_hw_signal_t *hw_signal, double period_time_s)
{
    LIB_hw_signal_read(hw_signal, period_time_s);

    return hw_signal->o_pos_edge;
}

/**
  * @brief  detect negative edge of the input signal
  * @param  hw_signal: input signal of type LIB_hw_signal_t
  * @param  period_time_s: sampling interval
  * @retval 1: negative edge detected, 0: else
	* @note   debounce time: 5ms
  */
uint8_t LIB_hw_signal_neg_edge(LIB_hw_signal_t *hw_signal, double period_time_s)
{
    LIB_hw_signal_read(hw_signal, period_time_s);

    return hw_signal->o_neg_edge;
}

/**
  * @brief  detect negative or positive edge of the input signal
  * @param  hw_signal: input signal of type LIB_hw_signal_t
  * @param  period_time_s: sampling interval
  * @retval 1: edge detected, 0: else
	* @note   debounce time: 5ms
  */
uint8_t LIB_hw_signal_edge(LIB_hw_signal_t *hw_signal, double period_time_s)
{
    LIB_hw_signal_read(hw_signal, period_time_s);

    return hw_signal->o_edge;
}

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 

#endif // __USING_LIB_HW_SIGNAL_READ
