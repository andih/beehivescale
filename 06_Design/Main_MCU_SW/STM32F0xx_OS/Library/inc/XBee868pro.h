
#include "main.h"


#define AT_COMMAND                        0x08
#define AT_COMMAND_QUEUE_PARAMETER_VALUE  0x09
#define TRANSMIT_REQUEST                  0x10
#define EXPLICIT_ADDRESSING_COMMAND_FRAME 0x11
#define REMOTE_COMMAND_REQUEST            0x17
#define AT_COMMAND_RESPONSE               0x88
#define MODEM_STATUS                      0x8A
#define TRANSMIT_STATUS                   0x8B
#define RECEIVE_PACKET                    0x90
#define EXPLICIT_RX_INDICATOR             0x91
#define NODE_IDENTIFICATION_INDICATOR     0x95
#define REMOTE_COMMAND_RESPONSE           0x97

#define START_DELIMITER                   0x7E

#define USART_XBEE           USART_6
#define USART_XBEE_TxPin	 	 PC7
#define USART_XBEE_RxPin     PC6
#define USART_XBEE_BAUDRATE	 9600


#define XBEE_TX_HEADER_LEN 17

typedef struct XBee_Tx_Frame_s{
  u8_t startdelimiter;
  u16_t length;
  u8_t type;
  u8_t id;
  u8_t destaddr[8];
  u16_t reserved;
  u8_t broadcastradius;
  u8_t transmitoptions;
  char *rfdata;
  u8_t checksum;
} XBee_Tx_Frame_t;


