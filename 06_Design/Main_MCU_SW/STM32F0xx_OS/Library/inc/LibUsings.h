/**
 * @file    ./Library/inc/LibUsings.h
 * @author  Andreas Hirtenlehner
 * @brief   Header file to define which Libraries are used
 */

#ifndef __LIBUSINGS_H
#define __LIBUSINGS_H

/* Includes ------------------------------------------------------------------*/

/** @addtogroup MCU_Library
  * @{
  */

/** @addtogroup Library_Usings
  * @{
  */
	
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/** @defgroup LibUsings_Exported_Constants
  * @{
  */ 
	
#define __USING_DEBUG
#define __USING_HW_SIGNAL_READ
#define __USING_TRF7970A
#define __USING_7S
#define __USING_FILTER
#define __USING_BATTERY

/**
  * @}
  */ 

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/**
  * @}
  */ 
	
/**
  * @}
  */ 

#endif // __LIBUSINGS_H
