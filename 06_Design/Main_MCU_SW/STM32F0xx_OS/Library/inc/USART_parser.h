/**
  ******************************************************************************
  * @file    ./STM32Fx_OS/Library/inc/Parser.h 
  * @author  EG, HA
  * @version V1.0.0
  * @date    18-November-2013
  * @brief   Header for Parser.c module
  ******************************************************************************
  * @RevisionHistory
  * 		 2013.11.18, V1.0.0, HA, -Datei Erstellung
  *                              -CompareStringBuffer_length, ValueBuffer_length, delimiter_char und closing_char definiert
  *                              -Funktionen USARTx_parser(), USART_CalcValue() und USART_WriteValue() definiert
  ******************************************************************************
  */
  
#ifndef __PARSER_H
#define __PARSER_H

/* Includes ------------------------------------------------------------------*/
#include "API.h"
#include "string.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define NumberOfValues         2
#define maxCompareStringLength 30
#define maxValueLength         30

#define delimiter_char '='
#define closing_char   '\n'

extern char      CompareString[NumberOfValues][maxCompareStringLength];
extern uint32_t *USARTParserVal[NumberOfValues];

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void USART1_parser(uint8_t ActualByte);
extern void USART2_parser(uint8_t ActualByte);
extern void USART3_parser(uint8_t ActualByte);
extern void USART4_parser(uint8_t ActualByte);
extern void USART5_parser(uint8_t ActualByte);
extern void USART6_parser(uint8_t ActualByte);
extern double USART_CalcValue(uint8_t *USART_RxValueBuffer, uint8_t KommaPosition);
extern void USART_WriteValue(char *USART_RxCompareStringBuffer, double Wert);

#endif
