/**
  * @file    ./Library/inc/battery.h
  * @author  Andreas Hirtenlehner
  * @brief   Header for battery.c module
  */
  
#ifndef __BATTERY_H
#define __BATTERY_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "RTC.h"

/** @addtogroup MCU_Library
  * @{
  */

/** @addtogroup battery
  * @{
  */

/* Exported types ------------------------------------------------------------*/
typedef enum LIB_battery_type_e
{
    NIMH = 1
} LIB_battery_type_t;

typedef enum LIB_battery_state_e
{
	UNDEFINED = 0,
	FULL,
	LOW,
	CHARGING,
	DISCHARGING,
} LIB_battery_state_t;

typedef struct LIB_battery_s
{
		double u_bat_v;
		double m_u_bat_v;
		double i_bat_a;
    double u_low_v;
		double u_full_v;
		double u_mean_v;
		double u_accu_vs;
	  double u_mean_attempt_valid;
		double charge_p;
		double r_internal_ohm;
		LIB_battery_state_t m_state;
	  uint32_t m_datetime_unix;
		uint32_t state_change_timestamp;
		LIB_battery_state_t state;
} LIB_battery_t;



/* Exported constants --------------------------------------------------------*/
#define LIB_BATTERY_STATE_CHANGE_DU_DT_MIN ((double) 1e-2)
#define LIB_BATTERY_FULL_DU_DT ((double) 0.1 / 180.0)
#define LIB_BATTERY_LOW_DU_DT ((double) 0.1 / 320.0)
#define LIB_BATTERY_SATTLE_TIME_S ((uint16_t) 2.0)
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/**
  * @}
  */ 
	
/**
  * @}
  */ 

#endif // __BATTERY_H
