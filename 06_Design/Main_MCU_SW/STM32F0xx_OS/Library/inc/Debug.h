/**
  * @file    ./Library/inc/Debug.h
  * @author  Gerald Ebmer
  * @brief   Header for Debug.c module
  */

#ifndef __DEBUG_H
#define __DEBUG_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include <stdbool.h>
#include <stdio.h>
#include "DebugOpt.h"
#include "GPIO.h"

/** @addtogroup MCU_Library
  * @{
  */

/** @addtogroup Debug
  * @{
  */

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/
/** @defgroup ADC_Exported_Constants
  * @{
  */ 
	
/** lower two bits indicate debug level
 * - 0 all
 * - 1 warning
 * - 2 serious
 * - 3 severe
 */
#define SYSTEM_DBG_LEVEL_ALL     0x00
#define SYSTEM_DBG_LEVEL_OFF     SYSTEM_DBG_LEVEL_ALL /* compatibility define only */
#define SYSTEM_DBG_LEVEL_WARNING 0x01 /* bad checksums, dropped packets, ... */
#define SYSTEM_DBG_LEVEL_SERIOUS 0x02 /* memory allocation failures, ... */
#define SYSTEM_DBG_LEVEL_SEVERE  0x03
#define SYSTEM_DBG_MASK_LEVEL    0x03

/** flag for SYSTEM_DEBUGF to enable that debug message */
#define SYSTEM_DBG_ON            0x80U
/** flag for SYSTEM_DEBUGF to disable that debug message */
#define SYSTEM_DBG_OFF           0x00U

/** flag for SYSTEM_DEBUGF indicating a tracing message (to follow program flow) */
#define SYSTEM_DBG_TRACE         0x40U
/** flag for SYSTEM_DEBUGF indicating a state debug message (to follow module states) */
#define SYSTEM_DBG_STATE         0x20U
/** flag for SYSTEM_DEBUGF indicating newly added code, not thoroughly tested yet */
#define SYSTEM_DBG_FRESH         0x10U
/** flag for SYSTEM_DEBUGF to halt after printing this debug message */
#define SYSTEM_DBG_HALT          0x08U

/****************************** Help functions ********************************/
/** set text color in terminal **/
//#include <stdio.h>

#define ATTR_RESET		  0
#define ATTR_BRIGHT 		1
#define ATTR_DIM		    2
#define ATTR_UNDERLINE  3
#define ATTR_BLINK		  4
#define ATTR_REVERSE		7
#define ATTR_HIDDEN		  8

#define BLACK 		0
#define RED		    1
#define GREEN		  2
#define YELLOW		3
#define BLUE		  4
#define MAGENTA		5
#define CYAN		  6
#define	GREY		  7
#define WHITE     8

/**
  * @}
  */ 

/* Exported macro ------------------------------------------------------------*/
#ifndef SYSTEM_NOASSERT
#define SYSTEM_ASSERT(message, assertion) do { if(!(assertion)) \
  SYSTEM_PLATFORM_ASSERT(message); } while(0)
#else  /* SYSTEM_NOASSERT */
#define SYSTEM_ASSERT(message, assertion)
#endif /* SYSTEM_NOASSERT */

/** if "expression" isn't true, then print "message" and execute "handler" expression */
#ifndef SYSTEM_ERROR
#define SYSTEM_ERROR(message, expression, handler) do { if (!(expression)) { \
  SYSTEM_PLATFORM_ASSERT(message); handler;}} while(0)
#endif /* SYSTEM_ERROR */

#ifdef __USING_DEBUG
/** print debug message only if debug message type is enabled...
 *  AND is of correct type AND is at least SYSTEM_DBG_LEVEL
 */

/*** HINT ***/
/* Place always a third argument in your function call.
** e.g. SYSTEM_DEBUGF(SYSCALL_DEBUG, "Too many files are open.\n",0);
** otherwise compiler gives you an error.
*/

#define SYSTEM_DEBUGF(debug, fmt, ...) \
do { \
   if ( ((debug) & SYSTEM_DBG_ON) && \
        ((debug) & SYSTEM_DBG_TYPES_ON) && \
        ((int16_t)((debug) & SYSTEM_DBG_MASK_LEVEL) >= SYSTEM_DBG_MIN_LEVEL)) { \
        fprintf(stderr, "\r%s:%d:%s():\t" fmt "\n\r", __FILE__, \
                    __LINE__, __func__, __VA_ARGS__); \
        if ((debug) & SYSTEM_DBG_HALT) { \
          while(1); \
        } \
    } \
 } while(0)

#define SYSTEM_DEBUGF_TEXT(debug, fmt) \
do { \
   if ( ((debug) & SYSTEM_DBG_ON) && \
        ((debug) & SYSTEM_DBG_TYPES_ON) && \
        ((int16_t)((debug) & SYSTEM_DBG_MASK_LEVEL) >= SYSTEM_DBG_MIN_LEVEL)) { \
        fprintf(stderr, "\r%s:%d:%s():\t" fmt "\n\r", __FILE__, \
                    __LINE__, __func__); \
        if ((debug) & SYSTEM_DBG_HALT) { \
          while(1); \
        } \
    } \
 } while(0)
#else  /* __USING_DEBUG */
#define SYSTEM_DEBUGF(debug, fmt, ...)
#endif /* __USING_DEBUG */

/* Exported functions ------------------------------------------------------- */
extern void LIB_DEBUG_hardfault_handler_wrapper(void);
extern void LIB_DEBUG_hardfault_handler(unsigned int * hardfault_args, unsigned lr_value);
extern void LIB_DEBUG_textcolor(int attr, int fg, int bg);
extern void LIB_DEBUG_pulse(API_GPIO_type_t* portpin, uint8_t cnt);
extern void LIB_DEBUG_tic(unsigned long *ticks);
extern unsigned long LIB_DEBUG_toc(unsigned long *ticks);
 
/**
  * @}
  */ 
	
/**
  * @}
  */ 

#endif // __DEBUG_H
