/**
  ******************************************************************************
  * @file    ./STM32Fx_OS/Library/inc/Functiongenerator.h 
  * @author  EG, HA
  * @version V1.0.0
  * @date    08-November-2013
  * @brief   Header for Functiongenerator.c module
  ******************************************************************************
  * @RevisionHistory
  * 		 2013.11.08, V1.0.0, HA, -Datei Erstellung
  *                              -Funktionen Rectangle(), Sine(), Triangle() und Noise() definiert
  *                              -Rectangle_ArrayLength, Sine_ArrayLength und Triangle_ArrayLength definiert
  ******************************************************************************
  */
  
#ifndef __FUNCTIONGENERATOR_H
#define __FUNCTIONGENERATOR_H

/* Includes ------------------------------------------------------------------*/
#include "API.h"
#include "DAC.h"
#include "math.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define PI 3.14159265359

#define Rectangle_ArrayLength 1000
#define Sine_ArrayLength      1000
#define Triangle_ArrayLength  1000

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void Rectangle(DAC_Type DAC_x, double TPeriod, double Upp, double DCOffset, double DutyCycle, uint32_t StepsPerPeriod);
extern void Sine(DAC_Type DAC_x, double TPeriod, double Upp, double DCOffset, double Symmetry, uint32_t StepsPerPeriod);
extern void Triangle(DAC_Type DAC_x, double TPeriod, double Upp, double DCOffset, double Symmetry, uint32_t StepsPerPeriod);
extern void Noise(DAC_Type DAC_x);

#endif
