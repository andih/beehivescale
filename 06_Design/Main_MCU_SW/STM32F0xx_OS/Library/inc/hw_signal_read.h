/**
  * @file    ./Library/inc/hw_signal_read.h
  * @author  Andreas Hirtenlehner
  * @brief   Header for hw_signal_read.c module
  */
  
#ifndef __HW_SIGNAL_READ_H
#define __HW_SIGNAL_READ_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"

/** @addtogroup MCU_Library
  * @{
  */

/** @addtogroup hw_signal_read
  * @{
  */

/* Exported types ------------------------------------------------------------*/
typedef struct LIB_hw_signal_s
{
    API_GPIO_type_t* portpin;
    uint8_t i_signal;
    uint8_t o_debounced;
    uint8_t m_o_debounced;
    uint8_t o_pos_edge;
    uint8_t o_neg_edge;
    uint8_t o_edge;
    double t_on_s;
    double t_off_s;
} LIB_hw_signal_t;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void LIB_hw_signal_read(LIB_hw_signal_t* hw_signal, double period_time_s);
extern void LIB_hw_signal_reset(LIB_hw_signal_t* hw_signal);
extern uint8_t LIB_hw_signal_debounce(LIB_hw_signal_t* hw_signal, double period_time_s);
extern uint8_t LIB_hw_signal_pos_edge(LIB_hw_signal_t* hw_signal, double period_time_s);
extern uint8_t LIB_hw_signal_neg_edge(LIB_hw_signal_t* hw_signal, double period_time_s);
extern uint8_t LIB_hw_signal_edge(LIB_hw_signal_t* hw_signal, double period_time_s);

/**
  * @}
  */ 
	
/**
  * @}
  */ 

#endif // __HW_SIGNAL_READ_H
