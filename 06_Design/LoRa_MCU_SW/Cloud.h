#ifndef CLOUD_H
#define CLOUD_H

#include "mbed.h"
#include "CayenneLPP.h"
#include "Beehive.h"
#include "SX1276_LoRaRadio.h"
#include "lorawan/LoRaRadio.h"
#include "lorawan/LoRaWANInterface.h"
#include "lorawan/system/lorawan_data_structures.h"
#include "events/EventQueue.h"
#include "trace_helper.h"

#define SX1272   0xFF
#define SX1276   0xEE

#ifdef TRACE_GROUP
    #undef TRACE_GROUP
#endif

#define TRACE_GROUP "CLOUD"

/*
 * Sets up an application dependent transmission timer in ms. Used only when Duty Cycling is off for testing
 */
#define TX_TIMER                        10000

/**
 * Maximum number of events for the event queue.
 * 10 is the safe number for the stack events, however, if application
 * also uses the queue for whatever purposes, this number should be increased.
 */
#define MAX_NUMBER_OF_EVENTS            10

/**
 * Maximum number of retries for CONFIRMED messages before giving up
 */
#define CONFIRMED_MSG_RETRY_COUNTER     3

#define RX_PORT_TIME                    1

using namespace events;
class Cloud {

public:
    Cloud();

    lorawan_status_t init();
    lorawan_status_t connect();
    lorawan_status_t disconnect();
    void upload(Beehive* beehive);
    bool isConnected();
    bool isConnecting();

private:
    bool _is_connected;
    bool _is_connecting;
    /**
     * Application specific callbacks
     */
    lorawan_app_callbacks_t _callbacks;

    /**
     * LoRa receive buffer
     * Max payload size can be LORAMAC_PHY_MAXPAYLOAD.
     */
    uint8_t _rx_buffer[30];

    SX1276_LoRaRadio* _radio;

    /**
    * This event queue is the global event queue for both the
    * application and stack. To conserve memory, the stack is designed to run
    * in the same thread as the application and the application is responsible for
    * providing an event queue to the stack that will be used for ISR deferment as
    * well as application information event queuing.
    */
    EventQueue* _ev_queue;

    /**
    * Constructing Mbed LoRaWANInterface and passing it the radio object from lora_radio_helper.
    */
    LoRaWANInterface* _lorawan;
    CayenneLPP* _payload;

    /**
     * Event handler.
     *
     * This will be passed to the LoRaWAN stack to queue events for the
     * application which in turn drive the application.
     */
    void lora_event_handler(lorawan_event_t event);
    void receive();
    void send();
    uint8_t data[2];
};

#endif
