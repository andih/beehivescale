#include "rtc_tamper.h"

void (*p_rtc_tamp2_handler)(void);

void rtc_set_tamp2_handler(void (*p_handler)(void))
{
    p_rtc_tamp2_handler = p_handler;
}

void rtc_tamper_gpio_init(PinName pin)
{
    GPIO_InitTypeDef gpioinitstruct;
	
    /* GPIO Ports Clock Enable */
    if(pin == PA_0)       { __HAL_RCC_GPIOA_CLK_ENABLE(); }
    else if(pin == PC_13) { __HAL_RCC_GPIOC_CLK_ENABLE(); }
    
    /* Configure Button pin as input */
    gpioinitstruct.Pull = GPIO_NOPULL;
    gpioinitstruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
	gpioinitstruct.Mode = GPIO_MODE_INPUT;

    if(pin == PA_0)       
    { 
        gpioinitstruct.Pin = GPIO_PIN_0; 
        HAL_GPIO_Init(GPIOA, &gpioinitstruct);
    }
    else if(pin == PC_13) 
    { 
        gpioinitstruct.Pin = GPIO_PIN_13; 
        HAL_GPIO_Init(GPIOC, &gpioinitstruct);
    }
}

void HAL_RTC_MspInit(RTC_HandleTypeDef* hrtc)
{
    /* Peripheral clock enable */
    __HAL_RCC_RTC_ENABLE();

     /* RTC interrupt Init */
    HAL_NVIC_SetPriority(RTC_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(RTC_IRQn);

    RTC_TamperTypeDef RtcTamperHandle;
    RtcTamperHandle.Tamper = RTC_TAMPER_2;
    RtcTamperHandle.Interrupt = RTC_TAMPER2_INTERRUPT;
    RtcTamperHandle.Trigger = RTC_TAMPERTRIGGER_RISINGEDGE;
    RtcTamperHandle.Filter = RTC_TAMPERFILTER_DISABLE;
    RtcTamperHandle.SamplingFrequency = RTC_TAMPERSAMPLINGFREQ_RTCCLK_DIV256;
    RtcTamperHandle.PrechargeDuration = RTC_TAMPERPRECHARGEDURATION_1RTCCLK;
    RtcTamperHandle.TamperPullUp = RTC_TAMPER_PULLUP_DISABLE;
    RtcTamperHandle.TimeStampOnTamperDetection = RTC_TIMESTAMPONTAMPERDETECTION_DISABLE;
    RtcTamperHandle.NoErase = RTC_TAMPER_ERASE_BACKUP_DISABLE;
    RtcTamperHandle.MaskFlag = RTC_TAMPERMASK_FLAG_DISABLE;
    
    HAL_RTCEx_SetTamper_IT(hrtc, &RtcTamperHandle);
}

void HAL_RTCEx_Tamper2EventCallback(RTC_HandleTypeDef *hrtc)
{
    if(p_rtc_tamp2_handler) { p_rtc_tamp2_handler(); }
}