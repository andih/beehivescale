#ifndef UART_API_H
#define UART_API_H

#include "stm32l0xx_hal.h"

#ifdef __cplusplus
extern "C" {
#endif

void uart_dma_init(DMA_HandleTypeDef **hdma_tx, DMA_HandleTypeDef **hdma_rx, UART_HandleTypeDef **huart);
void uart_set_rx_handler(void (*p_handler)(int event));
int uart_dma_rx(uint8_t* p_data, uint16_t size);
int uart_dma_tx(uint8_t* p_data, uint16_t size);

#ifdef __cplusplus
}
#endif

#endif