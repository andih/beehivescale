#ifndef RTC_TAMPER
#define RTC_TAMPER

#include "stm32l0xx_hal.h"
#include "rtc_api_hal.h"
#include "mbed_critical.h"
#include "mbed_mktime.h"
#include "mbed_error.h"

#ifdef __cplusplus
extern "C" {
#endif

void rtc_tamper_gpio_init(PinName pin);
void rtc_set_tamp2_handler(void (*p_handler)(void));

#ifdef __cplusplus
}
#endif

#endif