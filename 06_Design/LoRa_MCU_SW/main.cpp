#include "mbed.h"

#include "Cloud.h"
#include "Beehive.h"
#include "USBPhyHw.h"
#include "USBSerial.h"
#include "HTS221Sensor.h"
#include "rtc_api_hal.h"
#include "rtc_tamper.h"

// Application helpers
#include "trace_helper.h"
#include "rtc_api_helper.h"

#define USB_REQUEST ((uint8_t) 0x01)

#ifdef TRACE_GROUP
    #undef TRACE_GROUP
#endif

#define TRACE_GROUP "MAIN"

typedef enum connection_mode_e
{
    CLOUD = 1,
    USB_SERIAL,
} connection_mode_t;

void beehive_callback_isr(Beehive::beehive_event_t event);
void beehive_callback(Beehive::beehive_event_t event);
void usb_callback_isr();
void usb_callback();
void usb_print(Beehive* beehive);
uint8_t test_env_sensor();
void usb_dp_fall();
void usb_dm_fall();
void usb_handler();
void start_usb_detection();
void cloud_connect();
void rtc_tamp2_handler();

Thread* lorawanThread;
Thread* usbThread;
Thread* eventThread;
EventQueue* eventQueue;

InterruptIn* usb_detect_dp;
InterruptIn* usb_detect_dm;
DigitalOut* lora_tcxo;
USBSerial* usb_serial;
RawSerial* serial;
HTS221Sensor* env_sensor;

Cloud* cloud;
Beehive* beehive;

connection_mode_t connection_mode;

/**
 * Entry point for application
 */
int main(void)
{
#if defined (MBED_CONF_MBED_TRACE_ENABLE) && (MBED_CONF_MBED_TRACE_ENABLE == 1)
    setup_trace();
#endif

    tr_info("Initializing ...");
    print_memory_info();

    /* set system time */
    tr_debug("Set system time to 01.01.2019 ...");
    set_time(1546300800);
    time_t mcu_time = time(NULL);
    tr_info("%s", ctime(&mcu_time));
    
    /* init Event Queue */
    tr_debug("Init Event Queue ...");
    eventThread = new Thread(osPriorityNormal);
    eventQueue = new EventQueue(12 * EVENTS_EVENT_SIZE);

    eventThread->start(callback(eventQueue, &EventQueue::dispatch_forever));

    /* init Envionment Sensor HTS221 */
    tr_debug("Init Envionment Sensor HTS221 ...");
    env_sensor = new HTS221Sensor(new DevI2C(I2C_SDA, I2C_SCL), HTS221_I2C_ADDRESS, HTS221_DRDY);

    /* init Main MCU Serial */
    tr_debug("Init Main MCU Serial ...");
    serial = new RawSerial(UART_TX, UART_RX, MBED_CONF_PLATFORM_DEFAULT_SERIAL_BAUD_RATE);
    
    /* init Beehive */
    tr_debug("Init Beehive ...");
    beehive = new Beehive(serial, env_sensor, eventQueue);
    beehive->attach(eventQueue->event(beehive_callback)); 

    /* read USB data lines and connect if host is present */
    tr_debug("Start USB detection ...");
    start_usb_detection();
    
    if(connection_mode != USB_SERIAL)
    {
        /* Init LoRaWAN Cloud connection */
        tr_debug("Init LoRaWAN Cloud connection ...");
        cloud = new Cloud();
        cloud->init();
    }

    /* init RTC Tamper input */
    tr_debug("Init RTC Tamper input ...");
    rtc_set_tamp2_handler(rtc_tamp2_handler);
    rtc_tamper_gpio_init(WAKEUP);

    tr_info("Initialization done.");
    print_memory_info();

#ifndef NDEBUG
    /* test envionment sensor */
    tr_debug("Test envionment sensor ...");
    test_env_sensor(); 
#endif

    if(connection_mode == CLOUD) eventQueue->call(cloud_connect); 

    /*int i = 0;
    while(1)
    {
        time_t _time = time(NULL);
        tr_cmdline("heartbeat: %s", ctime(&_time));
        if(connection_mode == USB_SERIAL) usb_serial->printf("heartbeat: %i\r\n", i);
        if(connection_mode == CLOUD && cloud->isConnected()) cloud->upload(beehive);
        wait(30);
        
        i++;
    }*/

    eventQueue->dispatch_forever();

    return 0;
}

void beehive_callback(Beehive::beehive_event_t event)
{
    static bool processing = false;

    switch(event)
    {
        case Beehive::DATA_RECEIVED:
            tr_debug("Processing received dataset: %s ...", beehive->getDatetime());

            if(connection_mode == USB_SERIAL && usb_serial->connected()) { usb_print(beehive);     beehive->sensor_control(Beehive::ACK, 0); }
            else if(connection_mode == CLOUD && cloud->isConnected())    { cloud->upload(beehive); beehive->sensor_control(Beehive::ACK, 0); }
            else {
                tr_warning("USB and CLOUD connection closed. Abort request.");
            }

            break;

        case Beehive::TX_COMPLETE:
            break;

        default:
            tr_warning("Undefined Beehive event: %i", event);
            break;
    }
}

void usb_callback_isr()
{
    eventQueue->call(usb_callback);
}

void usb_callback()
{
    tr_info("Received Command on USB Port.");
    while(usb_serial->readable())
    {
        char command = usb_serial->getc();
        
        switch(command)
        {
            case USB_REQUEST:
                tr_debug("\tREQUEST DATA");
                break;
            
            default:
                tr_warning("Received undefined command on USB Port: %i", command);
                break;
        }
    }
}

void usb_print(Beehive* beehive)
{
    tr_info("Print data set to USB ...");
    tr_debug("\tDatetime: %s", beehive->getDatetime());
    tr_debug("\tWeight: %.2f", beehive->getWeight());
    tr_debug("\tAmbient Temperature: %0.2fgradC", beehive->getAmbTemp());
    tr_debug("\tAmbient Humidity: %0.2f%%", beehive->getAmbHumidity());
    tr_debug("\tBattery Charge: %i%%", beehive->getBatteryCharge());
    tr_debug("\tExternal Supply Voltage: %0.2fV", beehive->getUExtSupply());
    if(beehive->swarming())      { tr_warning("\tBEEHIVE IS SWARMING!");  }
    if(beehive->batteryLow())    { tr_warning("\tLOW BATTERY!");          }
    if(beehive->extSupplyUVLO()) { tr_warning("\tEXTERNAL SUPPLY UVLO!"); }
    if(beehive->charging())      { tr_debug("\tBATTERY CHARGING!");       }

    if(usb_serial->connected())
    {
        usb_serial->printf("%u, %0.2f, %0.2f, %0.2f, %i, %0.2f, %i, %i, %i, %i\r\n",   
            beehive->getDatetimeUNIX(),
            beehive->getWeight(),
            beehive->getAmbTemp(),
            beehive->getAmbHumidity(),
            beehive->getBatteryCharge(),
            beehive->getUExtSupply(),
            beehive->swarming(),
            beehive->batteryLow(),
            beehive->extSupplyUVLO(),
            beehive->charging());
    }
}

uint8_t test_env_sensor()
{
    float amb_temp;
    float amb_hum;

    int retval = env_sensor->get_temperature(&amb_temp);
    if(retval != 0)
    {
        tr_error("HTS221 error while reading temperature: code %i", retval);
        return 1;
    }
    else
    {
        tr_info("Ambient Temperature: %.2f gradC.", amb_temp);
    }
    
    retval = env_sensor->get_humidity(&amb_hum);
    if(retval != 0)
    {
        tr_error("HTS221 error while reading humidity: code %i", retval);
        return 2;
    }
    else
    {
        tr_info("Ambient Humidity:    %.2f%%.", amb_hum);
    }

    tr_debug("Envionment Sensor OK.");

    return 0;
}

void start_usb_detection()
{
    usb_detect_dp = new InterruptIn(USB_DP, PullUp);
    usb_detect_dm = new InterruptIn(USB_DM, PullUp);

    wait(0.5); 

    if(usb_detect_dp->read() && usb_detect_dm->read()) 
    {
        tr_debug("No USB host detected.");
        free(usb_detect_dm);
        free(usb_detect_dp);
        connection_mode = CLOUD;
    }
    else
    {
        tr_debug("Connect to USB Serial ...");
        free(usb_detect_dm);
        free(usb_detect_dp);
        connection_mode = USB_SERIAL;
        usb_serial = new USBSerial(false);
        usb_serial->connect();    
        usb_serial->attach(usb_callback_isr);
    }
}

void cloud_connect()
{
    cloud->connect();
}

void rtc_tamp2_handler()
{
   beehive->wakeup_handler();
}

// EOF
