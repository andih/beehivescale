#include "Cloud.h"

Cloud::Cloud()
{
    _is_connected = false;
    _is_connecting = false;
    _radio = new SX1276_LoRaRadio(LORA_SPI_MOSI,
                                  LORA_SPI_MISO,
                                  LORA_SPI_SCLK,
                                  LORA_CS,
                                  LORA_RESET,
                                  LORA_DIO0,
                                  LORA_DIO1,
                                  LORA_DIO2,
                                  LORA_DIO3,
                                  LORA_DIO4,
                                  LORA_DIO5,
                                  LORA_RF_SWITCH_CTL1,
                                  LORA_RF_SWITCH_CTL2,
                                  LORA_ANT_TX,
                                  LORA_ANT_RX,
                                  LORA_ANT_SWITCH,
                                  LORA_ANT_BOOST,
                                  LORA_TCXO_EN);

    _ev_queue = new EventQueue(MAX_NUMBER_OF_EVENTS * EVENTS_EVENT_SIZE);
    _lorawan = new LoRaWANInterface(*_radio);
    _payload = new CayenneLPP(200);
}

lorawan_status_t Cloud::init()
{
    // stores the status of a call to LoRaWAN protocol
    lorawan_status_t retcode;

    // Initialize LoRaWAN stack
    retcode = _lorawan->initialize(_ev_queue);
    if (retcode != LORAWAN_STATUS_OK) {
        tr_error("LoRa initialization failed!");
        return retcode;
    }

    tr_debug("Mbed LoRaWANStack initialized.");

    // prepare application callbacks
    _callbacks.events = callback(this, &Cloud::lora_event_handler);
    _lorawan->add_app_callbacks(&_callbacks);

    // Set number of retries in case of CONFIRMED messages
    retcode = _lorawan->set_confirmed_msg_retries(CONFIRMED_MSG_RETRY_COUNTER);
    if (retcode != LORAWAN_STATUS_OK) {
        tr_error("set_confirmed_msg_retries failed!");
        return retcode;
    }

    tr_debug("CONFIRMED message retries: %d", CONFIRMED_MSG_RETRY_COUNTER);

    // Enable adaptive data rate
#if defined (MBED_CONF_LORA_ADR_ON) && \
            (MBED_CONF_LORA_ADR_ON == 1)
    retcode = _lorawan->enable_adaptive_datarate();
    if (retcode != LORAWAN_STATUS_OK) {
        tr_error("enable_adaptive_datarate failed!");
        return retcode;
    }

    tr_debug("Adaptive data  rate (ADR) - Enabled");
#else 
    _lorawan->remove_channel_plan();
    _lorawan->disable_adaptive_datarate();
    _lorawan->set_datarate(MBED_CONF_APP_LORA_DATARATE);
    tr_debug("Set fixed datarate: %i", MBED_CONF_APP_LORA_DATARATE);
#endif

    return LORAWAN_STATUS_OK;
}

lorawan_status_t Cloud::connect()
{
    // stores the status of a call to LoRaWAN protocol
    lorawan_status_t retcode;

    _is_connecting = true;

    tr_debug("Trying to connect to LoRaWAN Gateway...");
    retcode = _lorawan->connect();
    if (retcode != LORAWAN_STATUS_OK && retcode != LORAWAN_STATUS_CONNECT_IN_PROGRESS) {
        tr_error("Connection error, code = %d", retcode);
        return retcode;
    }

    // make your event queue dispatching events forever
    _ev_queue->dispatch_forever();

    return LORAWAN_STATUS_OK;
}

lorawan_status_t Cloud::disconnect()
{
    return _lorawan->disconnect();
}

bool Cloud::isConnected()
{
    return _is_connected;
}

bool Cloud::isConnecting()
{
    return _is_connecting;
}

/**
 * Upload Data to the Network Server
 */
void Cloud::upload(Beehive* beehive)
{
    tr_info("Upload Data Set.");
    tr_debug("\tDatetime: %s", beehive->getDatetime());
    tr_debug("\tWeight: %.2f", beehive->getWeight());
    tr_debug("\tAmbient Temperature: %0.2fgradC", beehive->getAmbTemp());
    tr_debug("\tAmbient Humidity: %0.2f%%", beehive->getAmbHumidity());
    tr_debug("\tBattery Charge: %i%%", beehive->getBatteryCharge());
    tr_debug("\tExternal Supply Voltage: %0.2fV", beehive->getUExtSupply());
    if(beehive->swarming())      { tr_warning("\tBEEHIVE IS SWARMING!");  }
    if(beehive->batteryLow())    { tr_warning("\tLOW BATTERY!");          }
    if(beehive->extSupplyUVLO()) { tr_warning("\tEXTERNAL SUPPLY UVLO!"); }
    if(beehive->charging())      { tr_debug("\tBATTERY CHARGING!");       }

    _payload->reset();
    _payload->addTemperature(0, beehive->getAmbTemp());
    _payload->addRelativeHumidity(1, beehive->getAmbHumidity());
    _payload->addAnalogInput(2, beehive->getWeight());
    _payload->addAnalogInput(3, beehive->getBatteryCharge());
    _payload->addAnalogInput(4, beehive->getUExtSupply());
    _payload->addDigitalInput(5, beehive->swarming());
    _payload->addDigitalInput(6, beehive->batteryLow());
    _payload->addDigitalInput(7, beehive->extSupplyUVLO());
    _payload->addDigitalInput(8, beehive->charging());

    send();
}

void Cloud::send()
{
    int16_t retcode = _lorawan->send(MBED_CONF_LORA_APP_PORT, _payload->getBuffer(), _payload->getSize(), MSG_UNCONFIRMED_FLAG);
    
    if (retcode < 0) {
        retcode == LORAWAN_STATUS_WOULD_BLOCK ? tr_debug("send - WOULD BLOCK") : tr_error("send() - Error code %d", retcode);

        if (retcode == LORAWAN_STATUS_WOULD_BLOCK) {
            if (MBED_CONF_LORA_DUTY_CYCLE_ON) {
                _ev_queue->call_in(3000, callback(this, &Cloud::send));
            }
        }
        return;
    }

    tr_debug("%d bytes scheduled for transmission", retcode);
}

/**
 * Receive a message from the Network Server
 */
void Cloud::receive()
{
    uint8_t port;
    int flags;
    int16_t retcode = _lorawan->receive(_rx_buffer, sizeof(_rx_buffer), port, flags);

    if (retcode < 0) {
        tr_error("receive() - Error code %d", retcode);
        return;
    }

    tr_debug("RX Data on port %u (%d bytes): ", port, retcode);
    for (uint8_t i = 0; i < retcode; i++) {
        tr_debug("%02x ", _rx_buffer[i]);
    }

    if(port == RX_PORT_TIME) 
    {
        time_t time_utc = (_rx_buffer[3] << 24) ||
                          (_rx_buffer[2] << 16) ||
                          (_rx_buffer[1] << 8) ||
                          (_rx_buffer[0] << 0);
        set_time(time_utc);
    }

    memset(_rx_buffer, 0, sizeof(_rx_buffer));
}

/**
 * Event handler
 */
void Cloud::lora_event_handler(lorawan_event_t event)
{
    switch (event) {
        case CONNECTED:
            _is_connecting = false;
            _is_connected = true;
            tr_debug("Connection - Successful");
            break;
        case DISCONNECTED:
            _is_connected = false;
            _ev_queue->break_dispatch();
            tr_debug("Disconnected Successfully");
            break;
        case TX_DONE:
            tr_debug("Message Sent to Network Server");
            break;
        case TX_TIMEOUT:
            tr_error("TX Timeout.");
        case TX_ERROR:
            tr_error("TX Error.");
        case TX_CRYPTO_ERROR:
            tr_error("Crypto Error.");
        case TX_SCHEDULING_ERROR:
            tr_error("Transmission Error.");
            // try again
            if(!_is_connected) 
            {
                _lorawan->disconnect();
                if(init() == LORAWAN_STATUS_OK) { connect(); }
            }
            else if (MBED_CONF_LORA_DUTY_CYCLE_ON) 
            {
                send();
            }
            break;
        case RX_DONE:
            tr_debug("Received message from Network Server");
            receive();
            break;
        case RX_TIMEOUT:
            tr_error("RX Timeout.");
        case RX_ERROR:
            tr_error("RX Error.");
            break;
        case JOIN_FAILURE:
            tr_error("OTAA Failed - Check Keys");
            break;
        case UPLINK_REQUIRED:
            tr_debug("Uplink required by NS");
            if (MBED_CONF_LORA_DUTY_CYCLE_ON) {
                send();
            }
            break;
        default:
            tr_warning("Unknown LoRaWAN Event.");
            MBED_ASSERT("Unknown Event");
    }
}
