/* mbed Microcontroller Library
 * Copyright (c) 2006-2013 ARM Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef BEEHIVE_H
#define BEEHIVE_H

#include "platform/platform.h"
#include "HTS221Sensor.h"
#include "trace_helper.h"
#include <string.h> 
#include "uart_api.h"

#ifdef TRACE_GROUP
    #undef TRACE_GROUP
#endif

#define TRACE_GROUP "BEEH"

#define BATTERY_LOW_BITPOS     ((uint8_t) 0)
#define CHARGING_BITPOS        ((uint8_t) 1)
#define EXT_SUPPLY_UVLO_BITPOS ((uint8_t) 2)
#define SWARMING_BITPOS        ((uint8_t) 3)

#define WEIGHT_INT_SCALE          ((uint16_t)(0x10000 / 256))
#define BATTERY_INT_SCALE         ((uint16_t)(0x10000 / 128))
#define U_EXT_SUPPLY_INT_SCALE    ((uint16_t)(0x10000 / 64))
#define AMB_TEMP_INT_SCALE        ((uint16_t)(0x10000 / 128))
#define AMB_HUM_INT_SCALE         ((uint16_t)(0x10000 / 128))

class Beehive {

public:

    /** Create an AnalogIn, connected to the specified pin
     *
     * @param pin AnalogIn pin to connect to
     */
    Beehive(RawSerial* serialport, HTS221Sensor* env_sensor, EventQueue* event_queue);

    typedef struct sensor_data_s
    {
        uint32_t datetime;
        uint16_t weight;
        uint16_t amb_temperature;
        uint16_t amb_humidity;
        uint16_t battery_charge;
        uint16_t u_ext_supply;
        uint16_t status;

        sensor_data_s()
        {
            datetime = 0;
            weight = 0;
            amb_temperature = 0;
            amb_humidity = 0;
            battery_charge = 0;
            u_ext_supply = 0;
            status = 0;
        }

        sensor_data_s(uint8_t init_data[16])
        {
            datetime        = *((uint32_t*)&init_data[0]);
            weight          = *((uint16_t*)&init_data[4]);
            amb_temperature = *((uint16_t*)&init_data[6]);
            amb_humidity    = *((uint16_t*)&init_data[8]);
            battery_charge  = *((uint16_t*)&init_data[10]);
            u_ext_supply    = *((uint16_t*)&init_data[12]);
            status          = *((uint16_t*)&init_data[14]);
        }
    } sensor_data_t;

    typedef struct sensor_control_s
    {
        uint8_t type;
        uint32_t data;
    } sensor_control_t;

    typedef enum control_type_e
    {
        ACK = 1,
        ACQUIRE_DATA,
        REQUEST_DATA,
        REQUEST_DATA_SINCE,
        REQUEST_DATA_BEFORE,
        SET_TEMPERATURE,
        SET_TIME
    } control_type_e;

    typedef enum beehive_event_e
    {
        DATA_RECEIVED = 0,
        TX_COMPLETE

    } beehive_event_t;

    char* getDatetime();
    uint32_t getDatetimeUNIX();
    double getWeight();
    double getAmbTemp();
    double getAmbHumidity();
    uint8_t getBatteryCharge();
    double getUExtSupply();
    bool swarming();
    bool batteryLow();
    bool charging();
    bool extSupplyUVLO();

    void attach(mbed::Callback<void(beehive_event_t)> callback);
    void acquireData();
    void requestData();
    void requestData(time_t datetime);
    void requestDataSince(time_t datetime);
    void requestDataBefore(time_t datetime);
    void wakeup_handler();
    void sensor_control(Beehive::control_type_e type, uint32_t data);

private:
    sensor_data_t _sensor_data;
    InterruptIn* _wakeup;
    RawSerial* _serialport;
    HTS221Sensor* _env_sensor;
    EventQueue* _eventQueue;
    sensor_control_t _sensor_control;
    mbed::Callback<void(beehive_event_t)> _beehive_callback;
    uint8_t rx_buffer[sizeof(sensor_data_t)];

    void serialport_callback(int event);
    void wakeup_callback();
    void serialport_rx_handler(int event);    
    void sensorDataReceived();

    /** DMA implementation due to Isuue #6 
      * {
      */
    void serialport_tx_handler(int event);
    void serialport_tx_handler_helper();
     /** 
       * }
       */
};

#endif
