## Setup
### Disable STM USB driver since STM32L0xx is not supported
`usb/device/targets/TARGET_STM/` must be ignored in mbed-os (e.g. using `.mbedignore` file)

### Enable Flashing for custom target BEEHIVE_SCALE via ST-Link V3 (mbedls version 1.7.7)
add `ven_stm` to `MBED_STORAGE_DEVICE_VENDOR_STRINGS` in `C:\Python37\Lib\site-packages\mbed_os_tools\detect\windows.py` (line 37)

### MBED-OS changes
#### Upgrade RTC API to support Tamper input
File: `mbed-os/targets/TARGET_STM/rtc_api.c`
Add following in `RTC_IRQHandler()` (Line 290):
```cpp
if(RtcHandle.Instance->ISR & (RTC_IT_TAMP2 >> 5)) {
    HAL_RTCEx_TamperTimeStampIRQHandler(&RtcHandle);

    #ifdef __HAL_RTC_TAMPER_CLEAR_FLAG
        __HAL_RTC_TAMPER_CLEAR_FLAG(&RtcHandle, RTC_IT_TAMP2);
    #endif
}
```

---
**NOTES**

### Generate .mbedls-mock file
run `mbedls -m "004B:BEEHIVE_SCALE"` and `mbedls -m "0833:DISCO_L072CZ_LRWAN1_USB"` in the userApp directory

`004B` are the upper two bytes of the ST-LINK SN. It can be determined using ST-LINK Utility.

---