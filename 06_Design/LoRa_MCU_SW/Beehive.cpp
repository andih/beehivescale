#include "Beehive.h"

/** DMA implementation due to Issue #6 
 * {
 */
static DMA_HandleTypeDef *hdma_tx;
static DMA_HandleTypeDef *hdma_rx;
static UART_HandleTypeDef *huart;
static mbed::Callback<void(int)> rx_handler;
static mbed::Callback<void(int)> tx_handler;
/** 
 * }
 */

Beehive::Beehive(RawSerial* serialport, HTS221Sensor* env_sensor, EventQueue* event_queue)
{
    _eventQueue = event_queue;

    /** DMA implementation due to Issue #6 
      * {
      */
    //_serialport = serialport;
    //_serialport->set_dma_usage_tx(DMA_USAGE_ALWAYS);
    //_serialport->set_dma_usage_rx(DMA_USAGE_ALWAYS);

    rx_handler = callback(this, &Beehive::serialport_rx_handler);
    tx_handler = callback(this, &Beehive::serialport_tx_handler);
    uart_dma_init(&hdma_tx, &hdma_rx, &huart);
    /** 
      * }
      */

    /* enable pull-ups on I2C lines due to issue #1 */
    pin_mode(I2C_SCL, OpenDrainPullUp);
    pin_mode(I2C_SDA, OpenDrainPullUp);
    
    _env_sensor = env_sensor;
    int retval = _env_sensor->init(NULL);
    if(retval == 0) { _env_sensor->enable();                                        }
    else            { tr_error("Error while initializing HTS221. Code %i", retval); }
    
}

char* Beehive::getDatetime()
{
    return ctime((time_t*)&_sensor_data.datetime);
}

uint32_t Beehive::getDatetimeUNIX()
{
    return _sensor_data.datetime;
}

double Beehive::getWeight()
{
    return (double)_sensor_data.weight / WEIGHT_INT_SCALE;
}

double Beehive::getAmbTemp()
{
    return (double)_sensor_data.amb_temperature / AMB_TEMP_INT_SCALE;
}

double Beehive::getAmbHumidity()
{
    return (double)_sensor_data.amb_humidity / AMB_HUM_INT_SCALE;
}

uint8_t Beehive::getBatteryCharge()
{
    return (double)_sensor_data.battery_charge / BATTERY_INT_SCALE;
}

double Beehive::getUExtSupply()
{
    return (double)_sensor_data.u_ext_supply / U_EXT_SUPPLY_INT_SCALE;
}

bool Beehive::swarming()
{
    return _sensor_data.status & (1 << SWARMING_BITPOS);
}

bool Beehive::batteryLow()
{
    return _sensor_data.status & (1 << BATTERY_LOW_BITPOS);
}

bool Beehive::charging()
{
    return _sensor_data.status & (1 << CHARGING_BITPOS);
}

bool Beehive::extSupplyUVLO()
{
    return _sensor_data.status & (1 << EXT_SUPPLY_UVLO_BITPOS);
}

void Beehive::attach(mbed::Callback<void(beehive_event_t)> beehive_callback)
{
    _beehive_callback = beehive_callback;
}

void Beehive::acquireData()
{
    float amb_temperature;
    float amb_humidity;

    tr_debug("Trigger Sensor Data acquisition...");
    
    _env_sensor->get_temperature(&amb_temperature);
    _env_sensor->get_humidity(&amb_humidity);

    tr_debug("Ambient Temperature: %.2f°C", amb_temperature);
    tr_debug("Ambient Humidity:    %.2f%%", amb_humidity);

    uint16_t amb_temperature_int = amb_temperature * AMB_TEMP_INT_SCALE;
    uint16_t amb_humidity_int = amb_humidity * AMB_HUM_INT_SCALE;

    tr_debug("Request current sonsor data from Main MCU...");
    sensor_control(ACQUIRE_DATA, (amb_temperature_int << 16) | amb_humidity_int);
}

void Beehive::requestData()
{
    tr_debug("Request all Sensor Data from Main MCU...");
    sensor_control(REQUEST_DATA_SINCE, 0);
}

void Beehive::requestData(time_t datetime)
{
    tr_debug("Request Sensor Data from Main MCU from %s...", ctime(&datetime));
    sensor_control(REQUEST_DATA, (uint32_t)datetime);
}

void Beehive::requestDataSince(time_t datetime)
{
    tr_debug("Request Sensor Data from Main MCU since %s...", ctime(&datetime));
    sensor_control(REQUEST_DATA_SINCE, (uint32_t)datetime);
}

void Beehive::requestDataBefore(time_t datetime)
{
    tr_debug("Request Sensor Data from Main MCU before %s", ctime(&datetime));
    _sensor_control.type = REQUEST_DATA;
    _sensor_control.data = (uint32_t)datetime;
    _serialport->write((uint8_t*)&_sensor_control, sizeof(_sensor_control), callback(this, &Beehive::serialport_callback), SERIAL_EVENT_TX_ALL);
}

void Beehive::sensorDataReceived()
{
    tr_debug("Sensor Data received from Main MCU.");

    // disable RX to allow deep-sleep
    //_serialport->abort_read();

    _sensor_data = (Beehive::sensor_data_t)rx_buffer; 

    if(_beehive_callback) { _beehive_callback(DATA_RECEIVED); }
}

void Beehive::sensor_control(Beehive::control_type_e type, uint32_t data)
{
    _sensor_control.type = type;
    _sensor_control.data = data;

     /** DMA implementation due to Issue #6 
      * {
      */
    //_serialport->write((uint8_t*)&_sensor_control, sizeof(_sensor_control), _eventQueue->event(callback(this, &Beehive::serialport_callback)), SERIAL_EVENT_TX_ALL);
    int retval = uart_dma_tx((uint8_t*)&_sensor_control, sizeof(_sensor_control));
    if(retval < 0)
    {
        tr_warning("TX ongoing. (%i)", retval);
    }
    /** 
      * }
      */
}

void Beehive::serialport_callback(int event)
{
    switch(event)
    {
        case SERIAL_EVENT_TX_COMPLETE:
            tr_debug("Serialport TX complete.");
            if(_beehive_callback) { _beehive_callback(TX_COMPLETE); }
            break;

        default:
            tr_warning("Unknown Serialport Event: %i", event);
            break;
    }
}

void Beehive::wakeup_handler()
{
    _eventQueue->call(callback(this, &Beehive::wakeup_callback));
}

void Beehive::wakeup_callback()
{
    static uint8_t tx_buffer[2 * sizeof(_sensor_control)];
    float amb_temperature;
    float amb_humidity;

    tr_debug("Wakeup request from Main MCU received.");

    _env_sensor->get_temperature(&amb_temperature);
    _env_sensor->get_humidity(&amb_humidity);

    uint16_t amb_temperature_int = amb_temperature * 0xFFFF / 128.0;
    uint16_t amb_humidity_int = amb_humidity * 0xFFFF / 128.0;

    _sensor_control.type = SET_TEMPERATURE;
    _sensor_control.data = (amb_temperature_int << 16) | amb_humidity_int;
    memcpy(&tx_buffer[0], &_sensor_control, sizeof(_sensor_control));

    _sensor_control.type = SET_TIME;
    _sensor_control.data = time(NULL);
    memcpy(&tx_buffer[sizeof(_sensor_control)], &_sensor_control, sizeof(_sensor_control));

    /** DMA implementation due to Issue #6 
      * {
      */
    /*int retval = _serialport->read(rx_buffer, sizeof(Beehive::sensor_data_t), callback(this, &Beehive::serialport_rx_handler), SERIAL_EVENT_RX_COMPLETE);
    if(retval < 0)
    {
        tr_warning("RX ongoing. (%i)", retval);
    }

    retval = _serialport->write(tx_buffer, 2 * sizeof(_sensor_control), _eventQueue->event(callback(this, &Beehive::serialport_callback)), SERIAL_EVENT_TX_ALL);
    if(retval < 0)
    {
        tr_warning("TX ongoing. (%i)", retval);
    }*/

    if(huart->RxState == HAL_UART_STATE_BUSY_RX)                    { tr_warning("RX ongoing."); }
    else if(uart_dma_rx(rx_buffer, sizeof(sensor_data_t)) >= 0)     {}
    else                                                            { tr_error("RX error.");     }

    if(huart->gState == HAL_UART_STATE_BUSY_TX)                         { tr_warning("TX ongoing."); }
    else if(uart_dma_tx(tx_buffer, 2 * sizeof(_sensor_control)) >= 0)   {}
    else                                                                { tr_error("TX error.");     }
    /** 
      * }
      */
}

/** DMA implementation due to Issue #6 
 * {
 */

void Beehive::serialport_rx_handler(int event)
{
    _eventQueue->call(callback(this, &Beehive::sensorDataReceived));
}

void Beehive::serialport_tx_handler(int event)
{
    _eventQueue->call(callback(this, &Beehive::serialport_tx_handler_helper));
}

void Beehive::serialport_tx_handler_helper()
{
    serialport_callback(SERIAL_EVENT_TX_COMPLETE);
}

#ifdef __cplusplus
extern "C" {
#endif

void DMA1_Channel2_3_IRQHandler(void)
{
    //tr_warning("TX ongoing.");
    /* Transfer Complete Interrupt management ***********************************/
  if(__HAL_DMA_GET_FLAG(hdma_tx, __HAL_DMA_GET_TC_FLAG_INDEX(hdma_tx)) != RESET)
  {
    if(__HAL_DMA_GET_IT_SOURCE(hdma_tx, DMA_IT_TC) != RESET)
    {
      if((hdma_tx->Instance->CCR & DMA_CCR_CIRC) == 0U)
      {
        /* Disable the transfer complete interrupt */
        __HAL_DMA_DISABLE_IT(hdma_tx, DMA_IT_TC);
      }

      /* Clear the transfer complete flag */
      __HAL_DMA_CLEAR_FLAG(hdma_tx, __HAL_DMA_GET_TC_FLAG_INDEX(hdma_tx));

      /* Update error code */
      hdma_tx->ErrorCode |= HAL_DMA_ERROR_NONE;

      /* Change the DMA state */
      hdma_tx->State = HAL_DMA_STATE_READY;    
    
      /* Process Unlocked */
      __HAL_UNLOCK(hdma_tx);

      huart->gState = HAL_UART_STATE_READY;

      tx_handler(0);
    }
  }
  if(__HAL_DMA_GET_FLAG(hdma_rx, __HAL_DMA_GET_TC_FLAG_INDEX(hdma_rx)) != RESET)
  {
    if(__HAL_DMA_GET_IT_SOURCE(hdma_rx, DMA_IT_TC) != RESET)
    {
      if((hdma_rx->Instance->CCR & DMA_CCR_CIRC) == 0U)
      {
        /* Disable the transfer complete interrupt */
        __HAL_DMA_DISABLE_IT(hdma_rx, DMA_IT_TC);
      }

      /* Clear the transfer complete flag */
      __HAL_DMA_CLEAR_FLAG(hdma_rx, __HAL_DMA_GET_TC_FLAG_INDEX(hdma_rx));
    
      /* Update error code */
      hdma_rx->ErrorCode |= HAL_DMA_ERROR_NONE;

      /* Change the DMA state */
      hdma_rx->State = HAL_DMA_STATE_READY;    
    
      /* Process Unlocked */
      __HAL_UNLOCK(hdma_rx);

      huart->RxState = HAL_UART_STATE_READY;

      rx_handler(0);
    }
  }

  __HAL_DMA_CLEAR_FLAG(hdma_tx, 0xFFFFFFFF);
}

#ifdef __cplusplus
}
#endif

/** 
 * }
 */